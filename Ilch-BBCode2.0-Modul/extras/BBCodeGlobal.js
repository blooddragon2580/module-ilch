function Klapptext(str) {
	var KlappText = document.getElementById('layer_'+str);
	var KlappBild = document.getElementById('image_'+str);
	var medientuner_minus= "include/images/icons/minus.gif", medientuner_plus="include/images/icons/plus.gif";
	
	if (KlappText.style.display == 'none') {
		KlappText.style.display = 'block';
		KlappBild.src = medientuner_minus;
	} else {
		KlappText.style.display = 'none';
		KlappBild.src = medientuner_plus;
	}
}