BBCode 2.0 f�r IlchClan 1.1O (Stand 01.05.2010):
"""""""""""""""""""""""
Beschreibung:
-------------
Das Modul ersetzt den integrierten BBCode, mit mehr Funktionen.

Entwickelt
----------
� von "Funjoy"
� Anpassungen und Bufixes von "Mairu"
  unter Beihilfe von "boehserdavid"
� auf Basis von IlchClan 1.1 K (funktioniert nicht mit �lteren Versionen)

F�r Updater von alten Versionen vor 1.1I
----------------------------------------
 Folgende Dateien werden nicht mehr vom Modul ge�ndert und sollten wenn m�glich
 mit den Standarddateien ersetzt werden, soweit noch nicht durch das Update auf I geschehen
 - include/admin/admin.php
 - include/boxes/adminmenu.php
 - include/includes/loader.php (hier ggf. die beiden Zeilen l�schen die wegen dem Modul eingetragen worden)
 
Installation:
-------------
� Im Normalfall k�nnen alle Dateien �berschrieben werden, es k�nnte aber sein,
  dass sich dann einige Module nicht mit dem BBCode vertragen, dies betrifft die Dateien
  - include/admin/templates/news.htm
  - include/templates/gbook.htm
  - include/templates/forum/newpost.htm
  - include/templates/forum/newtopic.htm
  - include/templates/forum/postedit.htm
  - include/templates/forum/pm/new.htm
  Informationen dazu kannst du in der install.html finden
� nach dem Upload der Dateien bbcode_install.php aufrufen (http://www.deineseite.de/bbcode_install.php)
  und nach erfolgreichen Ausf�hren dann l�schen
� Optionen im Adminmen� einrichten und fertig

Fakten zum Modul:
-----------------
� Bilderverkleinerung funktionierte nicht auf jedem Webspace, deswegen habe ich eine JS-Version geschrieben
  mit der es eigentlich �berall klappen sollte und die auch den Webspace weniger belastet, wenn man die originalen
  Dateien von Funjoy verwenden m�chte, diese sind im Order extra hinterlegt
  (mittlerweise dann doch recht veraltet)

�nderungen :
------------------------------
vom 17.19.2011
� Farbauswahlpositionierung ver�ndert, so dass es f�r kein Design Positionierungsprobleme geben sollte

vom 04.09.2010
� Links innerhalb von [size] Tags sollten nun funktionieren

vom 01.05.2010
� Umlaute in Links werden wieder richtig dargestellt

vom 19.04.2010 (1.1O)
� # in Links werden wieder richtig interpretiert

vom 21.03.2010
� Shortwords angepasst, damit Flash auch mit Parameterangabe nicht gek�rzt wird und dadurch nicht funktioniert

vom 14.11.2009
� kleine Sachen ge�ndert, Badwordfilter f�r Umlaute, Noticefehler entfernt

vom 28.10.2009
� [url=http://...][color=...]... funktioniert wieder
� Standardtexte bei url und Schriftgr��e wieder hinzugef�gt

vom 11.10.2009 (f�r 1.1N)
� Smileys werden anders erkannt, damit sie nicht andere Elemente zerst�ren, daf�r muss von einem Smiley nun ein Leerzeichen kommen
� Flash nun mit Angabe von H�he und Breite
� Handhabung der Buttons etwas angepasst
� PHP Code wird auch ohne <?(php) und ?> formatiert

vom 01.03.2009:
� Fehler bei kurzer maximaler Wortl�nge mit Countdown und Liste behoben

vom 21.07.2008:
� Fehler mit der Anzeige des Flash-Buttons behoben
� Fehler in Installationsdatei behoben

vom 12.07.2008:
� Fehler mit zu breiten Codecontainern behoben
� Fehler mit langen Dateinamen f�r Codecontainern behoben
� Fehler mit Emailadressen behoben
� rudiment�re Flashunterst�tzung (standardm��ig deaktiviert)
� bbcode_install.php und bbcode_update.php zusammengefasst, also einfach nur noch bbcode_install.php aufrufen
	
vom 11.01.2008:
� Fehler wenn man alle Codes deaktiviert hatte und dann ein Quote oder Farbe einf�gen wollte (interface.js)

vom 09.01.2008:
� Fehler im Klapptext behoben, es wurden keine Umlaute um Titel unterst�tzt (class/bbcode.php)

vom 20.12.2007 (f�r I):
� Anpassung an Version I des Clanscriptes und damit gro�e Vereinfachung und der Installation und
  Reduzierung von Inkompatibilit�ten mit anderen Modulen
� kleine Fehler beseitig

vom 09.08.2007:
� Bilder die verkleinert dargestellt werden, sind nun mit dem Original verlinkt (es sei denn sie sind mit [url] anderweitig verlinkt worden)
� Man kann bei [img] und [shot] nun einen float-Wert mit angeben, also [img=left], [img=right] oder [img=none] (�quivalent bei shot)
  damit wird das Bild vom Text umflossen

vom 08.07.2007:
� Einf�gen einer Farbauswahlbox
� Bei Code kann nun ein Dateiname sowie Startzeile angegeben werden und es funktioniert auch im IE7
� Nochmaliger �berarbeitung der Bildverkleinerung, ich hoffe nun gehts auch im IE7 immer
� Ge�nderte Installationsanleitung !!

------------------------------
�nderungen von F zu H:
� "Schutz" vor langen W�rtern �berarbeitet
� kleine (interne) �nderungen am Linksystem - Fehlerbehebung
� Installationsroutine �berarbeitet

Bekannte Einschr�nkungen / Fehler:
----------------------------------


Haftungsausschluss:
-------------------
Ich �bernehme keine Haftung f�r Sch�den, die durch dieses Skript entstehen.
Benutzung ausschlie�lich AUF EIGENE GEFAHR.

Fehler bitte im ilch Forum posten, am besten in einem bestehenden BBCode 2 Thread
