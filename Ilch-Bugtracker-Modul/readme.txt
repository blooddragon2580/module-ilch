********************************
** Bugtracker f�r IlchClan 1.1 **
********************************


Beschreibung:
-------------
Der Bugtracker ist gut f�r Projekte bei dem die User mit Testen d�rfen. 
Diese k�nnen Fehler dar�ber berichten.

Entwickelt
----------
� von DarkBrain
� auf Basis von IlchClan 1.1 h

******************************************************************************
**Installation:							                                    **
**-------------							                                    **
**�  alle Dateien im Ordner upload, in ihrer Ordnerstrucktur hochladen      **
**�  Und die install.php ausf�hren unter www.deineHP.de/install.php         ** 
******************************************************************************


Bekannte Einschr�nkungen / Fehler:
----------------------------------
Keine

Haftungsausschluss:
-------------------
Ich �bernehme keine Haftung f�r Sch�den, die durch dieses Skript entstehen.
Benutzung ausschlie�lich AUF EIGENE GEFAHR. Also vorher ein BackUp machen!

Support: http://www.gods-of-nation.eu