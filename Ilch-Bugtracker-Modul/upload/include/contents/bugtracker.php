<?php

### Copyright by DarkBrain

defined ('main') or die ( 'no direct access' );

$title = $allgAr['title'].' :: Bugtracker';
$hmenu = 'Bugtracker';
$design = new design ( $title , $hmenu );
$design->header();
$stat_ar = array("0"=>"Neu","1"=>"Bearbeitung","2"=>"Erledigt",);

$title = "";
$text = "";
$link = "";
$ok = false;

## sql
if(isset($_POST['submit']) AND !empty($_POST['submit']))
{
	$kate = escape($_POST['kate'], 'string');
	$charname = escape($_POST['charname'], 'string');
	$title = escape($_POST['title'], 'string');
	$string = addslashes($_POST['text']);
	$text = escape($string, 'string');
	$link = escape($_POST['link'], 'string');
	$status = 0;
 	$date = date("Y-m-d H:i:s",time());
	
	if(!empty($text) || !empty($title))
	{
		db_query("INSERT INTO prefix_bugtracker (kate,status,charname,title,text,date,link) VALUES ('".$kate."','".$status."','".$charname."','".$title."','".$text."','".$date."','".$link."')");
		
		$to = "streetracer1@hotmail.de,rexiandi@web.de,mosxphonix@t-online.de"; 
		$subject = "Gods of Nation neuer Bug"; 
		$body = "Ein neuer Bug wurde in den Bugtracker eingetragen."; 
		mail($to, $subject, $body);
		
		echo 'Eintrag hinzugef&uuml;gt<br><br>';
		$ok = true;
	}
	else
	{
		echo "Es wurden nicht alle ben&ouml;tigten Felder ausgef&uuml;llt.<br><br>";
	}
}

if($ok == true)
{
	$title = "";
	$text = "";
	$link = "";
}

echo '	<form method="post" action="" style="width: 99%; padding: 5px 0; border-bottom:2px solid rgb(70,50,24); border-top:2px solid rgb(70,50,24); margin-bottom: 15px;">
			<div id="left_menu_head" style="padding-left: 0px; margin-bottom: 15px">Fehler melden</div>
			<div style="width: 100%; padding: 5px;">
				<label for="kate">Kategorie*:</label><br>
				<select name="kate" id="kate">
					<option value="Server">Server</option>
					<option value="Homepage">Homepage</option>
					<option value="Andere">Andere</option>
				</select>
			</div>
			<div style="margin-top: 10px; padding: 5px;">
				<label for="charname">Character Name*:</label><br>
				<input style="width: 100px; float: none;" type="text" name="charname" id="charname" maxlength="170" value="'.$charname.'"/>
			</div>
			<div style="margin-top: 10px; padding: 5px;">
				<label for="title">Titel*:</label><br>
				<input style="width: 500px; float: none;" type="text" name="title" id="title" maxlength="170" value="'.$title.'"/>
			</div>
			<div style="margin-top: 10px; padding: 5px;">
				<label for="text">Beschreibung*:</label><br>
				<textarea style="width: 100%; height: 100px; float: none;" name="text" id="text" maxlength="500">'.$text.'</textarea>
			</div>
			<div style="margin-top: 10px; padding: 5px;">
				<label for="link">WoW-Head Link:</label><br>
				<input style="width: 500px; float: none;" type="text" name="link" id="link" maxlength="200" value="'.$link.'"/>
			</div>
			<div style="margin-top: 10px; padding: 5px;">
				<div style="display: block;">Die mit einem * markierten Felder m&uuml;ssen ausgef&uuml;llt werden.</div>
			</div>
			<div style="margin-top: 10px; padding: 5px;">
				<input type="submit" name="submit" id="submit" value="Hinzuf&uuml;gen" />
			</div>
		</form>';


## Kates
$b = db_num_rows(db_query("SELECT DISTINCT kate FROM prefix_bugtracker"));

for($i=0;$i<$b;$i++){
 $a = @db_result(db_query("SELECT DISTINCT kate FROM prefix_bugtracker ORDER by kate ASC"),$i);
 echo '<table width="100%" border="0" cellpadding="5" cellspacing="1" class="border">
        <tr class="Chead">
         <td colspan="3" align="left"><font size="3"><b><u>'.$a.'</u></b></font></td>
        </tr>';
 $erg = db_query("SELECT * FROM prefix_bugtracker WHERE kate = '$a' ORDER BY status ASC");
 while ($row = db_fetch_assoc($erg)) {
  if($row['link'] == 'http://' OR $row['link'] == ''){
   $link = "";
  }else{
   $link = 'Link';
  }
  echo '<tr class="Cmite">
 	       <td align="left" valign="top" nowrap>&nbsp;<img src="include/images/icons/'.$stat_ar[$row['status']].'.png" alt="'.$stat_ar[$row['status']].'" title="'.$stat_ar[$row['status']].'">&nbsp;</td>
        </tr>';
  echo '<tr class="Cmite">
 	       <td align="left" width="100%"><font size="2,5"><b><u>'.$row['title'].'</u></b></font></td>
        </tr>';
  echo '<tr class="Cmite">
 	       <td align="left" width="100%"><font size="2" color="#FF0000">Beschreibung: </font><br><font size="2">'.stripslashes($row['text']).'</font><br></td>
        </tr>';
  echo '<tr class="Cmite">
 	       <td align="left" width="100%"><font size="2" color="#FF0000">GM Kommentar: </font><br><font size="2">'.stripslashes($row['adminkommentar']).'</font><br></td>
        </tr>';
  echo '<tr class="Cmite">
 	       <td align="left" valign="top" nowrap><font size="2" color="#FF0000">WoWHead Link:</font><br><a href="'.$row['link'].'" target="_blank"><font size="2">'.$link.'</font></a><br><br>&nbsp;<hr style="width: 850px; margin: 0px;" ><br></td>
        </tr><p>';
		
  }
 echo '</table><br>';
}
$design->footer();
?>