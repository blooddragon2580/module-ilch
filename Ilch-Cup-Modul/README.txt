#####################################################

Ver. 2.2
Copyright by mySEK.de Autor flanders
Support auf www.mySEK.de

#####################################################
modifiziert & angepasst an Ilch 1.1P by Lord|Schirmer
#####################################################


Das Cupscript ist ein einfach Turnier-Script mit dem es m�glich ist
Teams hinzuzuf�gen und diese jeweils eine Runde weiterkommen zu lassen.

Changelog v2.2:

- Bearbeitung der einzelnen Teams
- Ergebniseingabe
- Verbesserte �bersicht bei anlegen von Teams

Changelog v2.1:

- Verbesserte Funktion des Turnierbaums.
- Abfragen verbessert.

Changelog v2.0:

- Komplettes Design ist nun im Adminbereich einstellbar.
- Design kann auf "Standard" zur�ckgesetz werden.
- Verbesserte Funktion des Turnierbaums.
- Spiel um Platz 3.
- Angabe von Preisen/Gewinnen.
- Gruppenanzeige zuschaltbar.
- Teilnehmer k�nnen mit Ilch-Teams verkn�pft werden.
- Dummies zum Test k�nnen erzeugt und gel�scht werden.

Extra:
- Im Ordner Boxes befindet sich sich die "cup_nextmatches_v.php"
- Eine kleine Scroll-up Box welche die N�chsten Spiele anzeigt.


Installation:
- Alle Dateien hochladen und die "index.php?installation" aufrufen.
- Es werden keine Dateien �berschrieben.

Fehler:
- Keine Bekannt

ToDo:
- Angabe der Ergebnisse.