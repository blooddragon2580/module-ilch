<?php
#   Copyright by mySEK.de; flanders
#	Support für das Modul gibt es auf www.mySEK.de

array_walk ( $_POST,    'eingabebereinigen' );
array_walk ( $_GET,     'eingabebereinigen' );
array_walk ( $_REQUEST, 'eingabebereinigen' );

function eingabebereinigen(&$value, $key) {
    // $value = strip_tags($value, '<p><br /><b><strong>');
	$value = strip_tags($value); 
	// HTML-Tags maskieren
	$value = htmlspecialchars($value);
	// Leerzeichen am Anfang und Ende beseitigen
	$value = trim($value);
}

defined ('main') or die ('no direct access');
defined ('admin') or die ('only admin access');

$design = new design ( 'Admins Area', 'Admins Area', 2 );
$design->header();

include('include/includes/func/cup_abfragen.php');

$filename = 'install_cup.php';

if (file_exists($filename)) {
	echo '<div align="center"><h2><blink>install_cup.php bitte löschen!</blink></h2>';
	echo '<br>' . $footer . '</div>';
	exit;
} else {
	?>
    <table width="100%">
        <tr>
            <td>
                <a href="?cup"><img src="include/images/icons/admin/news.png"></a> | 
                <a href="?cup&page=teams"><img src="include/images/icons/admin/teams.png"></a> | 
                <a href="?cup&page=config"><img src="include/images/icons/admin/cup_admin.png"></a> <br>
            </td>
        </tr>
        <tr>
            <td>
			    <?php
                $page = (isset($_GET['page'])?$_GET['page']:'');
				switch($page) {
					
					######################  Teams  ####################
					case 'teams':
					if(isset($_REQUEST['add'])) {
						$abf = db_query('SELECT * FROM `prefix_cup_teams`');
						$id = $_POST['id'];
						$teamid = $_POST['teamid'];
						$clantag = $_POST['clantag'];
						$name = $_POST['name'];
						$gruppe = $_POST['gruppe'];
						$anordnung = $_POST['anordnung'];
						$hp = $_POST['hp'];
						db_query("INSERT INTO `prefix_cup_teams` VALUES ('" . $id . "','" . $teamid . "','" . $clantag . "','" . $name . "','" . $gruppe . "','" . $anordnung . "','" . $hp . "','','','','','','','','','','','')");
						echo 'Team wurde hinzugefügt';
					}
					if(isset($_REQUEST['setedit'])) {
						$abf = db_query('SELECT * FROM `prefix_cup_teams`');
						$id = $_POST['id'];
						$teamid = $_POST['teamid'];
						$clantag = $_POST['clantag'];
						$name = $_POST['name'];
						$gruppe = $_POST['gruppe'];
						$anordnung = $_POST['anordnung'];
						$hp = $_POST['hp'];
						db_query("UPDATE `prefix_cup_teams` SET name='" . $name . "', clantag='" . $clantag . "', teamid='" . $teamid . "', hp='" . $hp . "', gruppe='" . $gruppe . "', anordnung='" . $anordnung . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Eintrag wird geändert...</h3></div>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup&page=teams">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='del') {
						$id = $_GET['id'];
						db_query("DELETE FROM `prefix_cup_teams` WHERE id='" . $id . "'");
					}
					if(isset($_REQUEST['dummy'])) {
						/* db_query("INSERT INTO prefix_cup_config SET id='1', gruppe='ja', tablebg1='#e1e1e1', tablebg2='#ebebeb', headbg='#ffffff', headfont='#000000', fieldbg='#ffffff', fieldbor='#cccccc', font1='#000000', font2='#2BB4FE', preis1='Preis1', preis2='Preis2', preis3='Preis3' ON DUPLICATE KEY UPDATE gruppe='ja', tablebg1='#e1e1e1', tablebg2='#ebebeb', headbg='#ffffff', headfont='#000000', fieldbg='#ffffff', fieldbor='#cccccc', font1='#000000', font2='#2BB4FE', preis1='Preis1', preis2='Preis2', preis3='Preis3'"); */
						db_query("INSERT INTO `prefix_cup_teams` (`id`, `teamid`, `clantag`, `name`, `gruppe`, `anordnung`, `hp`, `viertel`, `halb`, `finale`, `p1`, `p2`, `p3`) VALUES
						    (1, 0, 'mySEK', 'mySEK', 1, 1, 'http://mysek.de', 0, 0, 0, 0, 0, 0),
							(2, 0, 'df', 'Die Front', 1, 2, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(3, 0, '-HT-', 'Harrington Team', 1, 3, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(4, 0, '=WT=', 'wilbury team 0', 1, 4, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(5, 0, 'KB', 'Kaos Bande', 2, 5, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(6, 0, '#LT#', 'LazyTeam', 2, 6, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(7, 0, 'DH', 'dahirinis', 2, 7, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(8, 0, 'RC', 'Roxbury Clan', 2, 8, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(9, 0, 'TA', 'Team Austria', 3, 9, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(10, 0, 'RF', 'Roflmao', 3, 10, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(11, 0, 'FC', 'flashchecker', 3, 11, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(12, 0, 'DW', 'Dawutz', 3, 12, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(13, 0, 'TW', 'Team Wax', 4, 13, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(14, 0, '=???=', 'Pretenders', 4, 14, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(15, 0, '=HH=', 'HullaHups', 4, 15, 'http://unserehp.de', 0, 0, 0, 0, 0, 0),
							(16, 0, 'CK', 'Cr4Ck0rs', 4, 16, 'http://unserehp.de', 0, 0, 0, 0, 0, 0);");
						echo '<h3>Dummies eingesetzt</h3>';
					}
					if(isset($_REQUEST['dummydel'])) {
						db_query('TRUNCATE TABLE `prefix_cup_teams`');
						echo '<h3>Dummies gelöscht</h3>';
					}			
					// Halb
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='halb') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = mysql_fetch_assoc($abf);
						$halb = $erg['halb'];
						$halb = 1;
						db_query("UPDATE `prefix_cup_teams` SET halb='" . $halb . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='loseh') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET halb='0' WHERE id='" . $id . "'");
					}
					// Finale
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='finale') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = mysql_fetch_assoc($abf);
						$finale = $erg['finale'];
						$finale = 1;
						db_query("UPDATE `prefix_cup_teams` SET finale='" . $finale . "' WHERE id='" . $id . "'");
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losef') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET finale='0' WHERE id='" . $id . "'");
					}
					// Platz1
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='p1') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = mysql_fetch_assoc($abf);
						$p1 = $erg['p1'];
						$p1 = 1;
						db_query("UPDATE `prefix_cup_teams` SET p1='" . $p1 . "' WHERE id='" . $id . "'");
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losep1') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET p1='0' WHERE id='" . $id . "'");
					}
					// Platz3
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='p3') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = mysql_fetch_assoc($abf);
						$p3 = $erg['p3'];
						$p3 = 1;
						db_query("UPDATE `prefix_cup_teams` SET p3='" . $p3 . "' WHERE id='" . $id . "'");
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losep3') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET p3='0' WHERE id='" . $id . "'");
					}
					?>
					<script type="text/javascript" src="include/includes/js/cCore.js"></script>
                    <form action="?cup&page=teams" method="post">
                        <table>
                            <tr class="Cdark">
                                <td colspan="3" align="center"><b>Teams<b></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Team Name:</td>
                                <td colspan="4" align="right"><input name="name" type="text" size="20" /></td>
                            </tr>
                            <tr>
                                <td>ClanTag:</td>
                                <td colspan="4" align="right"><input name="clantag" type="text" size="10" maxlength="5" /></td>
                            </tr>
                            <tr>
                                <td>Team ID:</td>
                                <td colspan="4" align="right"><a href="javascript:void(0)" onmouseover="return coolTip('Du hast die Möglichkeit Die ID eines Teams anzugeben, welches Du in der Ilch Administration angelegt hast.<br><br> Lässt Du das Feld leer, wird die Homepage URL als Link für das Team angegeben.',CAPTION,'Ilch TeamID:',CAPCOLOR,'#000000',BGCOLOR,'#729FCF',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/question.png' width="12px" height="12px" border="0px"></a>&nbsp;<input name="teamid" type="text" size="2" maxlength="2" /></td>
                            </tr>
                            <tr>
                                <td>Homepage</td>
                                <td align="right"><input name="hp" type="text" size="20" /></td>
                            </tr>
                            <tr>
                                <td>Gruppe / Anordnung:</td>
                                <td align="right"> <a href="javascript:void(0)" onmouseover="return coolTip('<b>Gruppe 1:</b><br>Mögliche Anordnung 1-4<br><br><b>Gruppe 2:</b><br>Mögliche Anordnung 5-8<br><br><b>Gruppe 3:</b><br>Mögliche Anordnung 9-12<br><br><b>Gruppe 4:</b><br>Mögliche Anordnung 13-16<br><br> Nutze als Beispiel die Dummies !',CAPTION,'Ilch TeamID:',CAPCOLOR,'#000000',BGCOLOR,'#729FCF',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/question.png' width="12px" height="12px" border="0px"></a>&nbsp;<input name="gruppe" type="text" size="1" maxlength="1" /> &nbsp;/&nbsp; <input name="anordnung" type="text" size="2" maxlength="2" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><input name="add" type="submit" value="Team hinzufügen" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;<br><br></td>
                            </tr>
                            <tr>
                                <td colspan="4" ><input name="dummy" type="submit" value="Dummies eintragen" /> | <input name="dummydel" type="submit" onClick="return confirm('Wirklich alle löschen ?');" value="Alle löschen" /></td>
                            </tr>
                        </table>
                    </form>
					<?php
                    echo '<hr>';
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='edit') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * from `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = mysql_fetch_assoc($abf);
						echo "<form action='?cup&page=teams' method='post'>";
						echo "<input type='hidden' value='$erg[id]' name='id' />";
						echo "<table width=100% bgcolor=#ebebeb><tr bgcolor=#729FCF><td><b>Name</b></td><td><b>ClanTag</b></td><td><b>Gruppe/Anordnung</b></td><td><b>Ilch_TeamID</b></td><td><b>Homepage</b></td><td><b>Edit</b></td></tr>";
						echo "<tr><td bgcolor=#e1e1e1>";
						echo "<input name='name' type='text' size='20' value='$erg[name]'>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<input name='clantag' type='text' size='10' value='$erg[clantag]'>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<input name='gruppe' type='text' size='2' value='$erg[gruppe]'> / <input name='anordnung' type='text' size='2' value='$erg[anordnung]'>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<input name='teamid' type='text' size='2' value='$erg[teamid]'>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<input name='hp' type='text' size='20' value='$erg[hp]'>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<input name='setedit' type='submit' value='Edit' />";
						echo "</td></tr>";
						echo "</table></form>";
						echo "<br><hr>";
					}
					$abf = db_query("SELECT * FROM `prefix_cup_teams` ORDER BY anordnung ASC");
					echo "<table width=100% bgcolor=#ebebeb><tr bgcolor=#729FCF><td><b>Name</b></td><td><b>ClanTag</b></td><td><b>Gruppe/Anordnung</b></td><td><b>Ilch_TeamID</b></td><td><b>Homepage</b></td><td><b>Löschen</b></td><td><b>Edit</td></b></tr>";
					while($erg=mysql_fetch_array($abf)) {
						echo "<tr><td bgcolor=#e1e1e1>";
						echo $erg['name'];
						echo "</td><td bgcolor=#e1e1e1>";
						echo $erg['clantag'];
						echo "</td><td bgcolor=#e1e1e1>";
						echo "$erg[gruppe] / $erg[anordnung]";
						echo "</td><td bgcolor=#e1e1e1>";
						echo $erg['teamid'];
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<a href=\"$erg[hp]\" target=\"_blank\">$erg[hp]</a>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<a href='?cup&page=teams&do=del&id=".$erg['id']."' onClick=\"return confirm('Wirklich löschen ?');\"><img src='/include/images/icons/del.gif'></a>";
						echo "</td><td bgcolor=#e1e1e1>";
						echo "<A HREF='?cup&page=teams&do=edit&id=".$erg['id']."'><img src='/include/images/icons/edit.gif'></A>";
					}
					echo "</table>";
					break;
					
					##################  Config  ####################
					case 'config': 
					if(isset($_REQUEST['add'])) {
						$abf = db_query("SELECT * FROM `prefix_cup_config`");
						$id = $_POST['id'];
						$gruppe = $_POST['gruppe'];
						$tablebg1 = $_POST['tablebg1'];
						$tablebg2 = $_POST['tablebg2'];
						$headbg = $_POST['headbg'];
						$headfont = $_POST['headfont'];
						$fieldbg = $_POST['fieldbg'];
						$fieldbor = $_POST['fieldbor'];
						$font1 = $_POST['font1'];
						$font2 = $_POST['font2'];
						$preis1 = $_POST['preis1'];
						$preis2 = $_POST['preis2'];
						$preis3 = $_POST['preis3'];
						/* db_query("INSERT INTO prefix_cup_config VALUES ('".$id."','".$gruppe."','".$tablebg1."','".$tablebg2."','".$headbg."','".$headfont."','".$fieldbg."','".$fieldbor."','".$font1."','".$font2."')"); */
						db_query("INSERT INTO `prefix_cup_config` SET id='1', gruppe='" . $gruppe . "', tablebg1='" . $tablebg1 . "', tablebg2='" . $tablebg2 . "', headbg='" . $headbg . "', headfont='" . $headfont . "', fieldbg='" . $fieldbg . "', fieldbor='" . $fieldbor . "', font1='" . $font1 . "', font2='" . $font2 . "', preis1='" . $preis1 . "', preis2='" . $preis2 . "', preis3='" . $preis3 . "' ON DUPLICATE KEY UPDATE gruppe='" . $gruppe . "', tablebg1='" . $tablebg1 . "', tablebg2='" . $tablebg2 . "', headbg='" . $headbg . "', headfont='" . $headfont . "', fieldbg='" . $fieldbg . "', fieldbor='" . $fieldbor . "', font1='" . $font1 . "', font2='" . $font2 . "', preis1='" . $preis1 . "', preis2='" . $preis2 . "', preis3='" . $preis3 . "'");
						echo '<h3>Config bearbeitet</h3>';
					}
					if(isset($_REQUEST['addori'])) {
						db_query("INSERT INTO prefix_cup_config SET id='1', gruppe='ja', tablebg1='#e1e1e1', tablebg2='#ebebeb', headbg='#ffffff', headfont='#000000', fieldbg='#ffffff', fieldbor='#cccccc', font1='#000000', font2='#2BB4FE', preis1='Preis1', preis2='Preis2', preis3='Preis3' ON DUPLICATE KEY UPDATE gruppe='ja', tablebg1='#e1e1e1', tablebg2='#ebebeb', headbg='#ffffff', headfont='#000000', fieldbg='#ffffff', fieldbor='#cccccc', font1='#000000', font2='#2BB4FE', preis1='Preis1', preis2='Preis2', preis3='Preis3'");
						echo '<h3>Original Config wieder hergestellt</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup&page=config">';
					}
					?>
                    <form action="?cup&page=config" method="post">
                        <table>
                            <tr class="Cdark">
                                <td colspan="3" align="center"><b>Cup Config<b></td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Gruppenanzeige:</td>
                                <td align="left">
                                    <select name="gruppe">
                                        <option selected><?php echo $gruppe; ?></option>
                                        <option value="ja">Ja</option>
                                        <option value="nein">Nein</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Tabellenhintergrund 1:</td>
                                <td><input name="tablebg1" type="text" size="10" maxlength="7" value="<?php echo $tablebg1; ?>" /></td>
                                <td bgcolor="<?php echo $tablebg1; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Tabellenhintergrund 2:</td>
                                <td><input name="tablebg2" type="text" size="10" maxlength="7" value="<?php echo $tablebg2; ?>" /></td>
                                <td bgcolor="<?php echo $tablebg2; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Überschrift Hintergrundfarbe</td>
                                <td><input name="headbg" type="text" size="10" maxlength="7" value="<?php echo $headbg; ?>"/></td>
                                <td bgcolor="<?php echo $headbg; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Überschrift Schriftfarbe</td>
                                <td><input name="headfont" type="text" size="10" maxlength="7" value="<?php echo $headfont; ?>" /></td>
                                <td bgcolor="<?php echo $headfont; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Teilnehmerfeld Hintergrundfarbe</td>
                                <td><input name="fieldbg" type="text" size="10" maxlength="7" value="<?php echo $fieldbg; ?>" /></td>
                                <td bgcolor="<?php echo $fieldbg; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Teilnehmerfeld Rahmenfarbe</td>
                                <td><input name="fieldbor" type="text" size="10" maxlength="7" value="<?php echo $fieldbor; ?>" /></td>
                                <td bgcolor="<?php echo $fieldbor; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Schriftfarbe 1</td>
                                <td><input name="font1" type="text" size="10" maxlength="7" value="<?php echo $font1; ?>" /></td>
                                <td bgcolor="<?php echo $font1; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Schriftfarbe 2</td>
                                <td><input name="font2" type="text" size="10" maxlength="7" value="<?php echo $font2; ?>" /></td>
                                <td bgcolor="<?php echo $font2; ?>">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Preis 1. Platz</td>
                                <td colspan="2"><input name="preis1" type="text" size="20" value="<?php echo $preis1; ?>" /></td>
                            </tr>
                            <tr>
                                <td>Preis 2. Platz</td>
                                <td colspan="2"><input name="preis2" type="text" size="20" value="<?php echo $preis2; ?>" /></td>
                            </tr>
                            <tr>
                                <td>Preis 3. Platz</td>
                                <td colspan="2"><input name="preis3" type="text" size="20" value="<?php echo $preis3; ?>" /></td>
                            </tr>
                            <tr>
                                <td colspan="3"><br><hr><br></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input name="add" type="submit" value="Speichern" /></td>
                                <td><input name="addori" type="submit" onClick="return confirm('Wirklich die Standardeinstellungen laden ?');" value="Standard wiederherstellen" /></td>
                            </tr>
                        </table>
                    </form>
					<?php
                    break;
                    
                    #################  Turnierbaum  #################	
                    default:
					// Viertel
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='viertel') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = db_fetch_assoc($abf);
						$viertel = $erg['viertel'];
						$viertel = 1;
						db_query("UPDATE `prefix_cup_teams` SET viertel='" . $viertel . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losev') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET viertel='0' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					// Halb
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='halb') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = db_fetch_assoc($abf);
						$halb = $erg['halb'];
						$halb = 1;
						db_query("UPDATE `prefix_cup_teams` SET halb='" . $halb . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='loseh') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET halb='0' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					// Finale
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='finale') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = db_fetch_assoc($abf);
						$finale = $erg['finale'];
						$finale = 1;
						db_query("UPDATE `prefix_cup_teams` SET finale='" . $finale . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losef') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET finale='0' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					// Platz1
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='p1') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = db_fetch_assoc($abf);
						$p1 = $erg['p1'];
						$p1 = 1;
						db_query("UPDATE `prefix_cup_teams` SET p1='" . $p1 . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losep1') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET p1='0' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					// Platz3
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='p3') {
						$id = $_GET['id'];
						$abf = db_query("SELECT * FROM `prefix_cup_teams` WHERE id='" . $id . "'");
						$erg = db_fetch_assoc($abf);
						$p3 = $erg['p3'];
						$p3 = 1;
						db_query("UPDATE `prefix_cup_teams` SET p3='" . $p3 . "' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['do']) && $_REQUEST['do']=='losep3') {
						$id = $_GET['id'];
						db_query("UPDATE `prefix_cup_teams` SET p3='0' WHERE id='" . $id . "'");
						echo '<div align="center"><h3>Bitte warten...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					
					########### Ergebniseingabe  Gruppe ##############
					if(isset($_REQUEST['eg1'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=1");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=2");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg2'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=3");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=4");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg3'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=5");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=6");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg4'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=7");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=8");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg5'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=9");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=10");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg6'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=11");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=12");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg7'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=13");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=14");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eg8'])) {
						$scoreg1 = $_POST['scoreg1'];
						$scoreg2 = $_POST['scoreg2'];
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg1 . "' WHERE anordnung=15");
						db_query("UPDATE `prefix_cup_teams` SET eg='" . $scoreg2 . "' WHERE anordnung=16");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					
					########### Ergebniseingabe  Viertelfinale ##############
					if(isset($_REQUEST['ev1'])) {
						$scorev1 = $_POST['scorev1'];
						$scorev2 = $_POST['scorev2'];
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev1 . "' WHERE viertel=1 && (anordnung=1 || anordnung=2)");
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev2 . "' WHERE viertel=1 && (anordnung=3 || anordnung=4)");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['ev2'])) {
						$scorev1 = $_POST['scorev1'];
						$scorev2 = $_POST['scorev2'];
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev1 . "' WHERE viertel=1 && (anordnung=5 || anordnung=6)");
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev2 . "' WHERE viertel=1 && (anordnung=7 || anordnung=8)");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['ev3'])) {
						$scorev1 = $_POST['scorev1'];
						$scorev2 = $_POST['scorev2'];
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev1 . "' WHERE viertel=1 && (anordnung=9 || anordnung=10)");
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev2 . "' WHERE viertel=1 && (anordnung=11 || anordnung=12)");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['ev4'])) {
						$scorev1 = $_POST['scorev1'];
						$scorev2 = $_POST['scorev2'];
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev1 . "' WHERE viertel=1 && (anordnung=13 || anordnung=14)");
						db_query("UPDATE `prefix_cup_teams` SET ev='" . $scorev2 . "' WHERE viertel=1 && (anordnung=15 || anordnung=16)");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					
					########### Ergebniseingabe  Halbfinale ##############
					if(isset($_REQUEST['eh1'])) {
						$scoreh1 = $_POST['scoreh1'];
						$scoreh2 = $_POST['scoreh2'];
						db_query("UPDATE `prefix_cup_teams` SET eh='" . $scoreh1 . "' WHERE halb=1 && gruppe=1");
						db_query("UPDATE `prefix_cup_teams` SET eh='" . $scoreh2 . "' WHERE halb=1 && gruppe=2");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					if(isset($_REQUEST['eh2'])) {
						$scoreh1 = $_POST['scoreh1'];
						$scoreh2 = $_POST['scoreh2'];
						db_query("UPDATE `prefix_cup_teams` SET eh='" . $scoreh1 . "' WHERE halb=1 && gruppe=3");
						db_query("UPDATE `prefix_cup_teams` SET eh='" . $scoreh2 . "' WHERE halb=1 && gruppe=4");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					
					########### Ergebniseingabe   Finale ##############
					if(isset($_REQUEST['ef1'])) {
						$scoref1 = $_POST['scoref1'];
						$scoref2 = $_POST['scoref2'];
						db_query("UPDATE `prefix_cup_teams` SET ef='" . $scoref1 . "' WHERE finale=1 && (gruppe=1 || gruppe=2)");
						db_query("UPDATE `prefix_cup_teams` SET ef='" . $scoref2 . "' WHERE finale=1 && (gruppe=3 || gruppe=4)");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					
					########### Ergebniseingabe  Platz 3  ##############
					if(isset($_REQUEST['ep3'])) {
						$scorep31 = $_POST['scorep31'];
						$scorep32 = $_POST['scorep32'];
						db_query("UPDATE `prefix_cup_teams` SET ep3='" . $scorep31 . "' WHERE finale=0 && (gruppe=1 || gruppe=2)");
						db_query("UPDATE `prefix_cup_teams` SET ep3='" . $scorep32 . "' WHERE finale=0 && (gruppe=3 || gruppe=4)");
						echo '<div align="center"><h3>Ergebnis wird eingetragen...</h3>';
						echo '<meta http-equiv="refresh" content="1; URL=?cup">';
					}
					?>
                    <table width="100%"  border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr valign="middle" align="center" class="Cdark">
                            <td><b>Vorunden</b></td>
                            <td><b>Viertelfinale</b></td>
                            <td><b>Halbfinale</b></td>
                            <td><b>Finale</b></td>
                            <td><b>Gewinner</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">Gruppe 1:</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody" valign="middle">
							<?php
							if (isset($clan_vor_1_name)) {
								echo $clan1_name;
								echo '';
							}
							elseif (isset($clan1_name)) {
								echo $clan1_name;
								echo "  <a href='?cup&do=viertel&id=".$clan1_id."'><img src='include/images/icons/admin/win.png'></a>";
							} else {
								echo '';
							}
							?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg1">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan1_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan2_eg; ?>" />
                                    <button type="submit" name="eg1" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php
							if (isset($clan_halb_1_name)) {
								echo $clan_vor_1_name;
								echo "";
							} elseif (isset($clan_vor_1_name)) {
								echo $clan_vor_1_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_1_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_1_id."'><img src='include/images/icons/admin/lose.png'></a>";
							} else {
								echo "";
							}
							?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">	
							<?php
                            if (isset($clan_vor_1_name)) {
								echo $clan2_name;
								echo "";
                            } elseif (isset($clan2_name)) {
								echo $clan2_name;
								echo "  <a href='?cup&do=viertel&id=".$clan2_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="ev1">
                                    <input name="scorev1" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_1_ev; ?>" /> zu 
                                    <input name="scorev2" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_2_ev; ?>" />
                                    <button type="submit" name="ev1" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php
                            if (isset($clan_fin_1_name)) {
								echo $clan_halb_1_name;
								echo "";
                            } elseif (isset($clan_halb_1_name)) {
								echo $clan_halb_1_name;
								echo "  <a href='?cup&do=finale&id=".$clan_halb_1_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=loseh&id=".$clan_halb_1_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
							<?php
                            if (isset($clan_vor_2_name)) {
								echo $clan3_name;
								echo "";
                            } elseif (isset($clan3_name)) {
								echo $clan3_name;
								echo "  <a href='?cup&do=viertel&id=".$clan3_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg2">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan3_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan4_eg; ?>" />
                                    <button type="submit" name="eg2" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php
                            if (isset($clan_halb_1_name)) {
								echo $clan_vor_2_name;
								echo "";
							} elseif (isset($clan_vor_2_name)) {
								echo $clan_vor_2_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_2_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_2_id."'><img src='include/images/icons/admin/lose.png'></a>";
							} else {
								echo "";
							}
							?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
							<?php
                            if (isset($clan_vor_2_name)) {
								echo $clan4_name;
								echo "";
							} elseif (isset($clan4_name)) {
								echo $clan4_name;
								echo "  <a href='?cup&do=viertel&id=".$clan4_id."'><img src='include/images/icons/admin/win.png'></a>";
							} else {
								echo "";
							}
							?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Gruppe 2:</td>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="eh1">
                                    <input name="scoreh1" type="text" size="1" maxlength="2" value="<?php echo $clan_halb_1_eh; ?>" /> zu 
                                    <input name="scoreh2" type="text" size="1" maxlength="2" value="<?php echo $clan_halb_2_eh; ?>" />
                                    <button type="submit" name="eh1" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php
							if (isset($clan_winner_name)) {
								echo $clan_fin_1_name;
								echo "";
							} elseif (isset($clan_fin_1_name)) {
								echo $clan_fin_1_name;
								echo "  <a href='?cup&do=p1&id=".$clan_fin_1_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losef&id=".$clan_fin_1_id."'><img src='include/images/icons/admin/lose.png'></a>";
							} else {
								echo "";
							}
							?>
                            </td>
                        </tr>   
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
							<?php
                            if (isset($clan_vor_3_name)) {
								echo $clan5_name;
								echo "";
                            } elseif (isset($clan5_name)) {
								echo $clan5_name;
								echo "  <a href='?cup&do=viertel&id=".$clan5_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg3">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan5_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan6_eg; ?>" />
                                    <button type="submit" name="eg3" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php
                            if (isset($clan_halb_2_name)) {
								echo $clan_vor_3_name;
								echo "";
                            } elseif (isset($clan_vor_3_name)) {
								echo $clan_vor_3_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_3_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_3_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
							<?php
                            if (isset($clan_vor_3_name)) {
								echo $clan6_name;
								echo "";
                            } elseif (isset($clan6_name)) {
								echo $clan6_name;
								echo "  <a href='?cup&do=viertel&id=".$clan6_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="ev2">
                                    <input name="scorev1" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_3_ev; ?>" /> zu 
                                    <input name="scorev2" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_4_ev; ?>" />
                                    <button type="submit" name="ev2" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">	
							<?php
                            if (isset($clan_fin_1_name)) {
								echo $clan_halb_2_name;
								echo "";
                            } elseif (isset($clan_halb_2_name)) {
								echo $clan_halb_2_name;
								echo "  <a href='?cup&do=finale&id=".$clan_halb_2_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=loseh&id=".$clan_halb_2_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
                            echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td> 
                        </tr>
                        <tr>
                            <td class="Bbody">	
							<?php
                            if (isset($clan_vor_4_name)) {
								echo $clan7_name;
								echo "";
                            } elseif (isset($clan7_name)) {
								echo $clan7_name;
								echo "  <a href='?cup&do=viertel&id=".$clan7_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg4">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan7_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan8_eg; ?>" />
                                    <button type="submit" name="eg4" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_halb_2_name)) {
								echo $clan_vor_4_name;
								echo "";
                            } elseif (isset($clan_vor_4_name)) {
								echo $clan_vor_4_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_4_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_4_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">	
							<?php
                            if (isset($clan_vor_4_name)) {
								echo $clan8_name;
								echo "";
                            } elseif (isset($clan8_name)) {
								echo $clan8_name;
								echo "  <a href='?cup&do=viertel&id=".$clan8_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Gruppe 3:</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="ef1">
                                    <input name="scoref1" type="text" size="1" maxlength="2" value="<?php echo $clan_fin_1_ef; ?>" /> zu 
                                    <input name="scoref2" type="text" size="1" maxlength="2" value="<?php echo $clan_fin_2_ef; ?>" />
                                    <button type="submit" name="ef1" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody"> 
							<?php 
                            if (isset($clan_winner_name)) {
								echo $clan_winner_name; 
								echo "  <a href='?cup&do=losep1&id=".$clan_winner_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_5_name)) {
								echo $clan9_name;
								echo "";
                            } elseif (isset($clan9_name)) {
								echo $clan9_name;
								echo "  <a href='?cup&do=viertel&id=".$clan9_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg5">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan9_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan10_eg; ?>" />
                                    <button type="submit" name="eg5" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_halb_3_name)) {
								echo $clan_vor_5_name;
								echo "";
                            } elseif (isset($clan_vor_5_name)) {
								echo $clan_vor_5_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_5_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_5_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_5_name)) {
								echo $clan10_name;
								echo "";
							} elseif (isset($clan10_name)) {
								echo $clan10_name;
								echo "  <a href='?cup&do=viertel&id=".$clan10_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="ev3">
                                    <input name="scorev1" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_5_ev; ?>" /> zu 
                                    <input name="scorev2" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_6_ev; ?>" />
                                    <button type="submit" name="ev3" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_fin_2_name)) {
								echo $clan_halb_3_name;
								echo "";
                            } elseif (isset($clan_halb_3_name)) {
								echo $clan_halb_3_name;
								echo "  <a href='?cup&do=finale&id=".$clan_halb_3_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=loseh&id=".$clan_halb_3_id."'><img src='include/images/icons/admin/lose.png'></a>";
							} else {
								echo "";
							}
							?>
							</td>
							<td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
							<?php
                            if (isset($clan_vor_6_name)) {
								echo $clan11_name;
								echo "";
                            } elseif (isset($clan11_name)) {
								echo $clan11_name;
								echo "  <a href='?cup&do=viertel&id=".$clan11_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg6">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan11_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan12_eg; ?>" />
                                    <button type="submit" name="eg6" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_halb_3_name)) {
								echo $clan_vor_6_name;
								echo "";
                            } elseif (isset($clan_vor_6_name)){
								echo $clan_vor_6_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_6_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_6_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
							?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_6_name)) {
								echo $clan12_name;
								echo "";
                            } elseif (isset($clan12_name)) {
								echo $clan12_name;
								echo "  <a href='?cup&do=viertel&id=".$clan12_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Gruppe 4:</td>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="eh2">
                                    <input name="scoreh1" type="text" size="1" maxlength="2" value="<?php echo $clan_halb_3_eh; ?>" /> zu 
                                    <input name="scoreh2" type="text" size="1" maxlength="2" value="<?php echo $clan_halb_4_eh; ?>" />
                                    <button type="submit" name="eh2" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_winner_name)) {
								echo $clan_fin_2_name;
								echo "";
                            } elseif (isset($clan_fin_2_name)) {
								echo $clan_fin_2_name;
								echo "  <a href='?cup&do=p1&id=".$clan_fin_2_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losef&id=".$clan_fin_2_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_7_name)) {
								echo $clan13_name;
								echo "";
                            } elseif (isset($clan13_name)) {
								echo $clan13_name;
								echo "  <a href='?cup&do=viertel&id=".$clan13_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg7">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan13_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan14_eg; ?>" />
                                    <button type="submit" name="eg7" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_halb_4_name)) {
								echo $clan_vor_7_name;
								echo "";
                            } elseif (isset($clan_vor_7_name)) {
								echo $clan_vor_7_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_7_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_7_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_7_name)) {
								echo $clan14_name;
								echo "";
                            } elseif (isset($clan14_name)) {
								echo $clan14_name;
								echo "  <a href='?cup&do=viertel&id=".$clan14_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="ev4">
                                    <input name="scorev1" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_7_ev; ?>" /> zu 
                                    <input name="scorev2" type="text" size="1" maxlength="2" value="<?php echo $clan_vor_8_ev; ?>" />
                                    <button type="submit" name="ev4" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php
                            if (isset($clan_fin_2_name)) {
								echo $clan_halb_4_name;
								echo "";
                            } elseif (isset($clan_halb_4_name)) {
								echo $clan_halb_4_name;
								echo "  <a href='?cup&do=finale&id=".$clan_halb_4_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=loseh&id=".$clan_halb_4_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td> 
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_8_name)) {
								echo $clan15_name;
								echo "";
                            } elseif (isset($clan15_name)) {
								echo $clan15_name;
								echo "  <a href='?cup&do=viertel&id=".$clan15_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <form action="?cup" method="post" name="eg8">
                                    <input name="scoreg1" type="text" size="1" maxlength="2" value="<?php echo $clan15_eg; ?>" /> zu 
                                    <input name="scoreg2" type="text" size="1" maxlength="2" value="<?php echo $clan16_eg; ?>" />
                                    <button type="submit" name="eg8" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_halb_4_name)) {
								echo $clan_vor_8_name;
								echo "";
                            } elseif (isset($clan_vor_8_name)) {
								echo $clan_vor_8_name;
								echo "  <a href='?cup&do=halb&id=".$clan_vor_8_id."'><img src='include/images/icons/admin/win.png'></a> | <a href='?cup&do=losev&id=".$clan_vor_8_id."'><img src='include/images/icons/admin/lose.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="Bbody">
                            <?php
                            if (isset($clan_vor_8_name)) {
								echo $clan16_name;
								echo "";
                            } elseif (isset($clan16_name)) {
								echo $clan16_name;
								echo "  <a href='?cup&do=viertel&id=".$clan16_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
                            ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="Cdark" align="center" colspan="2">Spiel um Platz 3</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="Bbody">
                            <?php 
                            if (isset($clan_platz3_winner_name)) {
								echo $clan_platz3_1_name; 
								echo "";
                            } elseif (isset($clan_fin_1_name)) {
								echo $clan_platz3_1_name; 
								echo "  <a href='?cup&do=p3&id=".$clan_platz3_1_id."'><img src='include/images/icons/admin/win.png'></a>";
                            } else {
								echo "";
                            }
							?>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="center">
                                <form action="?cup" method="post" name="ep3">
                                    <input name="scorep31" type="text" size="1" maxlength="2" value="<?php echo $clan_platz3_1_ep3; ?>" /> zu 
                                    <input name="scorep32" type="text" size="1" maxlength="2" value="<?php echo $clan_platz3_2_ep3; ?>" />
                                    <button type="submit" name="ep3" value="Submit"><img src="include/images/icons/admin/win.png" width="8" height="8" /></button>
                                </form>
                            </td>
                            <td class="Bbody">
							<?php 
							if (isset($clan_platz3_winner_name)) {
								echo $clan_platz3_winner_name; 
								echo "  <a href='?cup&do=losep3&id=".$clan_platz3_winner_id."'><img src='include/images/icons/admin/lose.png'></a>";
							} else {
								echo "";
							}
							?>
							</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
							<td class="Bbody">
							<?php 
							if (isset($clan_platz3_winner_name)) {
								echo $clan_platz3_2_name; 
								echo "";
							} elseif (isset($clan_fin_2_name)) {
								echo $clan_platz3_2_name; 
								echo "  <a href='?cup&do=p3&id=".$clan_platz3_2_id."'><img src='include/images/icons/admin/win.png'></a>";
							} else {
								echo "";
							}
							?>
							</td>
							<td>&nbsp;</td>
                        </tr>
                        <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
                        </tr>
                    </table>
					<?php
                    }
				}
				?>
                <div align="center"><br><br><?php echo $footer; ?><br></div>
<?php $design->footer(); ?>