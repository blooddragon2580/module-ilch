<?php 
#   Copyright by mySEK.de; flanders
#	Support für das Modul gibt es auf www.mySEK.de

defined('main') or die ('no direct access');
include('include/includes/func/cup_abfragen.php');

?> 

<marquee scrollamount="3" direction="up" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 3, 0);">
<table width="100%" cellpadding="2" cellspacing="2" bgcolor="#e1e1e1" align="center">
    <tr valign="middle" align="center">
        <td colspan="3" style="color:#000;border:solid 1px #ccc;background:#fff;"><b>Nextmatches</b></td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Gruppe 1</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan1_id)) {
                echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan1_clantag . '</a>';
            } else {
                echo 'Platz frei';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
		    <?php
            if (isset($clan2_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan2_clantag . '</a>';
			} else {
				echo 'Platz frei';
			}
			?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
		    <?php
			if (isset($clan3_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan3_clantag . '</a>';
			} else {
				echo 'Platz frei';
			}
			?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
            <?php
            if (isset($clan4_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan4_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Gruppe 2</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan5_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan5_clantag . '</a>';
            } else {
				echo 'Platz frei';
			}
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan6_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan6_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan7_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan7_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan8_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan8_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Gruppe 3</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan9_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan9_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan10_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan10_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan11_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan11_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan12_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan12_clantag .'</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Gruppe 4</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan13_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan13_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan14_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan14_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan15_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan15_clantag . '</a>';
			} else {
				echo 'Platz frei';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan16_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan16_clantag . '</a>';
            } else {
				echo 'Platz frei';
            }
            ?>
        </td>
    </tr>
	<?php
    if (isset($clan_vor_1_id)) {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Viertelfinals</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_1_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_1_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_2_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_2_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_3_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_3_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_4_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_4_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_5_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_5_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_6_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_6_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_7_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_7_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_vor_8_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_vor_8_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
	<?php
    } else {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Viertelfinals</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
		<td align="center" width="30%">.vs.</td>
		<td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
    </tr>
	<?php
    }
    if (isset($clan_halb_1_id)) {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Halbfinals</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_halb_1_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_halb_1_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_halb_2_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_halb_2_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_halb_3_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_halb_3_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_halb_4_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_halb_4_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
	<?php
    } else {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Halbfinals</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
		<td align="center" width="30%">.vs.</td>
		<td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
    </tr>
	<?php
    }
    if (isset($clan_fin_1_id)) {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Finale</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_fin_1_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_fin_1_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_fin_2_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_fin_2_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
	<?php
    } else {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Finale</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
		<td align="center" width="30%">.vs.</td>
		<td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
    </tr>
	<?php
    }
    if (isset($clan_platz3_1_id)) {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Spiel um Platz 3</td>
    </tr>
    <tr>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_platz3_1_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_platz3_1_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
        <td align="center" width="30%">.vs.</td>
        <td bgcolor="#ebebeb" align="center" width="30%">
			<?php
            if (isset($clan_platz3_2_id)) {
				echo '<a style="color:#000; font-weight:bold; font-family:Arial; font-size:11px" href="?cup">' . $clan_platz3_2_clantag . '</a>';
            } else {
				echo 'n/a';
            }
            ?>
        </td>
    </tr>
	<?php
    } else {
	?>
    <tr>
        <td colspan="3" align="center" style="color:#2BB4FE;font-weight:bold;">Spiel um Platz 3</td>
    </tr>
    <tr>
		<td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
		<td align="center" width="30%">.vs.</td>
		<td bgcolor="#ebebeb" align="center" width="30%">n/a</td>
    </tr>
	<?php
    }
	?>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="font-size:9px"><a href="?cup">Zum Turnier</a></td>
    </tr>
    <tr>
        <td colspan="3"><br><br><?php echo $footer; ?></td>
    </tr>
</table>
</marquee>