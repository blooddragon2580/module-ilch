<?php
#   Copyright by mySEK.de; flanders
#	Support f�r das Modul gibt es auf www.mySEK.de
defined ('main') or die ( 'no direct access' );

include('include/includes/func/cup_abfragen.php');

$title = $allgAr['title'].' :: Cup';
$hmenu = 'Turnier';
$design = new design ( $title , $hmenu );
$design->header();

$gruppenanzeige =  		$gruppe;		// 1=Ja 0=Nein
$table_bg		=		$tablebg1;		// Allgemeiner Tabellenhintergrund
$table_bg2		=		$tablebg2;		// Allgemeiner Tabellenhintergrund
$head_bg		=		$headbg;		// Hintergrund Tabellenkopf
$head_font		=		$headfont;		// Schriftfarbe Tabellenkopf
$field_bg		=		$fieldbg;		// Turnierbaumfeld Hintergrund
$field_border	=		$fieldbor;		// Turnierbaumfeld Rahmen
$font_alg		=		$font1;			// 1. Allgemeine Schriftfarbe
$font_alg2		=		$font2;			// 2. Allgemeine Schriftfarbe

?>

<script type="text/javascript" src="include/includes/js/cCore.js"></script>

<!-- GRUPPENANZEIGE START -->
<?php 
if($gruppenanzeige == "ja" ) {
?>
    <table width="100%" cellpadding="2" cellspacing="2" bgcolor="<?=$table_bg?>">
        <tr valign="middle" align="center" bgcolor="<?=$head_bg?>">
            <td style="color:<?=$head_font?>;"><b>Gruppe1</b></td>
            <td style="color:<?=$head_font?>;"><b>Gruppe2</b></td>
            <td style="color:<?=$head_font?>;"><b>Gruppe3</b></td>
            <td style="color:<?=$head_font?>;"><b>Gruppe4</b></td>
        </tr>
        <tr>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan1_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan1_hp\">$clan1_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan1_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan5_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan5_hp\">$clan5_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan5_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan9_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan9_hp\">$clan9_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan9_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan13_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan13_hp\">$clan13_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan13_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
        </tr>
        <tr>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan2_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan2_hp\">$clan2_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan2_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan6_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan6_hp\">$clan6_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan6_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan10_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan10_hp\">$clan10_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan10_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan14_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan14_hp\">$clan14_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan14_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
        </tr>
        <tr>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan3_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan3_hp\">$clan3_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan3_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan7_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan7_hp\">$clan7_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan7_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan11_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan11_hp\">$clan11_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan11_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan15_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan15_hp\">$clan15_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan15_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
        </tr>
        <tr>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan4_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan4_hp\">$clan4_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan4_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan8_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan8_hp\">$clan8_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan8_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan12_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan12_hp\">$clan12_name <br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan12_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
            <td bgcolor="<?=$table_bg2?>">
			<?php
            if (isset($clan16_id)) {
				echo "<a style=\"color:#000; font-weight:bold; font-family:Arial; font-size:11px\" href=\"$clan16_hp\">$clan16_name<br> <span style=\"color:$font_alg2; font-family:tahoma; font-size:9px; font-weight:bold\">$clan16_hp</span><br>";
			} else {
				echo "<span style=\"color:$font_alg;\">Platz frei</span>";
			}
			?>
            </td>
        </tr>
        <tr>
            <td colspan="5"><hr></td>
        </tr>
    </table>
	<?php
} else { 
    echo ""; 
}
?>
<!-- GRUPPENANZEIGE ENDE -->

<!-- TURNIERBAUM START -->
<table width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="<?=$table_bg?>">
    <tr>
        <td colspan="5" align="center" style="color:<?=$head_font?>;border:solid 1px <?=$table_bg?>;" bgcolor="<?=$head_bg?>"><b>Turnierbaum</b></td>
    </tr>
    <tr valign="middle" align="center" style="border:solid 1px <?=$field_border?>;" bgcolor="<?=$head_bg?>">
        <td style="color:<?=$head_font?>;border:solid 1px <?=$table_bg?>;"><b>Vorrunden</b></td>
        <td style="color:<?=$head_font?>;border:solid 1px <?=$table_bg?>;"><b>Viertelfinale</b></td>
        <td style="color:<?=$head_font?>;border:solid 1px <?=$table_bg?>;"><b>Halbfinale</b></td>
        <td style="color:<?=$head_font?>;border:solid 1px <?=$table_bg?>;"><b>Finale</b></td>
        <td style="color:<?=$head_font?>;border:solid 1px <?=$table_bg?>;"><b>Gewinner</b></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" class="Bbody">Gruppe 1:</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan1_name;?><br><b>Website:</b> <?php echo $clan1_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan1_teamid ==0 ) {
				echo "<a href='$clan1_hp' target='_blank'>$clan1_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan1_teamid' target='_blank'>$clan1_clantag</a>";
			}
			?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan1_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan2_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_1_name;?><br><b>Website:</b> <?php echo $clan_vor_1_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_1_teamid ==0 ) {
				echo "<a href='$clan_vor_1_hp' target='_blank'>$clan_vor_1_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan_vor_1_teamid' target='_blank'>$clan_vor_1_clantag</a>";
			}
			?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan2_name;?><br><b>Website:</b> <?php echo $clan2_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
			if($clan2_teamid ==0 ) {
				echo "<a href='$clan2_hp' target='_blank'>$clan2_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan2_teamid' target='_blank'>$clan2_clantag</a>";
			}
			?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_vor_1_ev; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_vor_2_ev; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_halb_1_name;?><br><b>Website:</b> <?php echo $clan_halb_1_hp; ?>',CAPTION,'Halbfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_halb_1_teamid ==0 ) {
				echo "<a href='$clan_halb_1_hp' target='_blank'>$clan_halb_1_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan_halb_1_teamid' target='_blank'>$clan_halb_1_clantag</a>";
			}
			?>
        </td>
        <td>&nbsp;</td>    
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan3_name;?><br><b>Website:</b> <?php echo $clan3_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan3_teamid ==0 ) {
				echo "<a href='$clan3_hp' target='_blank'>$clan3_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan3_teamid' target='_blank'>$clan3_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan3_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan4_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px> Bronze<br><b>Name:</b> <?php echo $clan_vor_2_name;?><br><b>Website:</b> <?php echo $clan_vor_2_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
			if($clan_vor_2_teamid ==0 ) {
				echo "<a href='$clan_vor_2_hp' target='_blank'>$clan_vor_2_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan_vor_2_teamid' target='_blank'>$clan_vor_2_clantag</a>";
			}
			?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan4_name;?><br><b>Website:</b> <?php echo $clan4_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
			if($clan4_teamid ==0 ) {
				echo "<a href='$clan4_hp' target='_blank'>$clan4_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan4_teamid' target='_blank'>$clan4_clantag</a>";
			}
			?>
        </td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="Bbody">Gruppe 2:</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_halb_1_eh; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_halb_2_eh; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_fin_1_name;?><br><b>Website:</b> <?php echo $clan_fin_1_hp; ?>',CAPTION,'Finale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_fin_1_teamid ==0 ) {
            echo "<a href='$clan_fin_1_hp' target='_blank'>$clan_fin_1_clantag</a>";
            }
            else
            {
            echo "<a href='index.php?teams-show-$clan_fin_1_teamid' target='_blank'>$clan_fin_1_clantag</a>";
            }
            ?>
        </td>
    </tr>   
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan5_name;?><br><b>Website:</b> <?php echo $clan5_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
            <?php
            if($clan5_teamid ==0 ) {
				echo "<a href='$clan5_hp' target='_blank'>$clan5_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan5_teamid' target='_blank'>$clan5_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan5_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan6_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_3_name;?><br><b>Website:</b> <?php echo $clan_vor_3_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_3_teamid ==0 ) {
				echo "<a href='$clan_vor_3_hp' target='_blank'>$clan_vor_3_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_vor_3_teamid' target='_blank'>$clan_vor_3_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan6_name;?><br><b>Website:</b> <?php echo $clan6_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan6_teamid ==0 ) {
				echo "<a href='$clan6_hp' target='_blank'>$clan6_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan6_teamid' target='_blank'>$clan6_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_vor_3_ev; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_vor_4_ev; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">	
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_halb_2_name;?><br><b>Website:</b> <?php echo $clan_halb_2_hp; ?>',CAPTION,'Halbfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_halb_2_teamid ==0 ) {
				echo "<a href='$clan_halb_2_hp' target='_blank'>$clan_halb_2_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_halb_2_teamid' target='_blank'>$clan_halb_2_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td> 
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">	
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan7_name;?><br><b>Website:</b> <?php echo $clan7_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan7_teamid ==0 ) {
				echo "<a href='$clan7_hp' target='_blank'>$clan7_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan7_teamid' target='_blank'>$clan7_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan7_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan8_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_4_name;?><br><b>Website:</b> <?php echo $clan_vor_4_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_4_teamid ==0 ) {
				echo "<a href='$clan_vor_4_hp' target='_blank'>$clan_vor_4_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_vor_4_teamid' target='_blank'>$clan_vor_4_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan8_name;?><br><b>Website:</b> <?php echo $clan8_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan8_teamid ==0 ) {
				echo "<a href='$clan8_hp' target='_blank'>$clan8_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan8_teamid' target='_blank'>$clan8_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody">Gruppe 3:</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" style="color:<?=$font_alg?>;"><?php echo $clan_fin_1_ef; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_fin_2_ef; ?></td>
        <td class="Bbody" align="left" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/medal_g.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_winner_name;?><br><b>Website:</b> <?php echo $clan_winner_hp; ?><br><b>Preis:</b> <?php echo $clan_winner_preis1; ?>',CAPTION,'Gewinner:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/medal_g.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_winner_teamid ==0 ) {
				echo "<a href='$clan_winner_hp' target='_blank'><b>$clan_winner_clantag</b></a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_winner_teamid' target='_blank'><b>$clan_winner_clantag</b></a>";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="Bbody" align="left" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/medal_s.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_platz2_name;?><br><b>Website:</b> <?php echo $clan_platz2_hp; ?><br><b>Preis:</b> <?php echo $clan_platz2_preis2; ?>',CAPTION,'Platz 2:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/medal_s.png' width="16px" height="16px" border="0px"></a>
			<?php
            if (isset($clan_winner_name)) {
				echo "<a href='$clan_platz2_hp' target='_blank'><b>$clan_platz2_clantag</b></a>";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan9_name;?><br><b>Website:</b> <?php echo $clan9_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan9_teamid ==0 ) {
				echo "<a href='$clan9_hp' target='_blank'>$clan9_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan9_teamid' target='_blank'>$clan9_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="Bbody" align="left" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/medal_b.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_platz3_winner_name;?><br><b>Website:</b> <?php echo $clan_platz3_winner_hp; ?><br><b>Preis:</b> <?php echo $clan_platz3_winner_preis3; ?>',CAPTION,'Platz 3:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/medal_b.png' width="16px" height="16px" border="0px"></a>
			<?php
            echo "<a href='$clan_platz3_winner_hp' target='_blank'><b>$clan_platz3_winner_clantag</b></a>";
			?>
        </td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan9_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan10_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_5_name;?><br><b>Website:</b> <?php echo $clan_vor_5_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_5_teamid ==0 ) {
				echo "<a href='$clan_vor_5_hp' target='_blank'>$clan_vor_5_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_vor_5_teamid' target='_blank'>$clan_vor_5_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan10_name;?><br><b>Website:</b> <?php echo $clan10_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan10_teamid ==0 ) {
				echo "<a href='$clan10_hp' target='_blank'>$clan10_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan10_teamid' target='_blank'>$clan10_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_vor_5_ev; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_vor_6_ev; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_halb_3_name;?><br><b>Website:</b> <?php echo $clan_halb_3_hp; ?>',CAPTION,'Halbfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_halb_3_teamid ==0 ) {
				echo "<a href='$clan_halb_3_hp' target='_blank'>$clan_halb_3_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_halb_3_teamid' target='_blank'>$clan_halb_3_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan11_name;?><br><b>Website:</b> <?php echo $clan11_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
			if($clan11_teamid ==0 ) {
				echo "<a href='$clan11_hp' target='_blank'>$clan11_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan11_teamid' target='_blank'>$clan11_clantag</a>";
			}
			?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan11_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan12_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_6_name;?><br><b>Website:</b> <?php echo $clan_vor_6_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_6_teamid ==0 ) {
				echo "<a href='$clan_vor_6_hp' target='_blank'>$clan_vor_6_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_vor_6_teamid' target='_blank'>$clan_vor_6_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan12_name;?><br><b>Website:</b> <?php echo $clan12_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan12_teamid ==0 ) {
				echo "<a href='$clan12_hp' target='_blank'>$clan12_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan12_teamid' target='_blank'>$clan12_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="Bbody">Gruppe 4:</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_halb_3_eh; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_halb_4_eh; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_fin_2_name;?><br><b>Website:</b> <?php echo $clan_fin_2_hp; ?>',CAPTION,'Finale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_fin_2_teamid ==0 ) {
				echo "<a href='$clan_fin_2_hp' target='_blank'>$clan_fin_2_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_fin_2_teamid' target='_blank'>$clan_fin_2_clantag</a>";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan13_name;?><br><b>Website:</b> <?php echo $clan13_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan13_teamid ==0 ) {
				echo "<a href='$clan13_hp' target='_blank'>$clan13_clantag</a>";
			} else {
				echo "<a href='index.php?teams-show-$clan13_teamid' target='_blank'>$clan13_clantag</a>";
			}
			?>
        </td>
        <td>&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan13_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan14_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_7_name;?><br><b>Website:</b> <?php echo $clan_vor_7_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_7_teamid ==0 ) {
				echo "<a href='$clan_vor_7_hp' target='_blank'>$clan_vor_7_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_vor_7_teamid' target='_blank'>$clan_vor_7_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan14_name;?><br><b>Website:</b> <?php echo $clan14_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan14_teamid ==0 ) {
				echo "<a href='$clan14_hp' target='_blank'>$clan14_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan14_teamid' target='_blank'>$clan14_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_vor_7_ev; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_vor_8_ev; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_halb_4_name;?><br><b>Website:</b> <?php echo $clan_halb_4_hp; ?>',CAPTION,'Halbfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_halb_4_teamid ==0 ) {
				echo "<a href='$clan_halb_4_hp' target='_blank'>$clan_halb_4_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_halb_4_teamid' target='_blank'>$clan_halb_4_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td> 
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan15_name;?><br><b>Website:</b> <?php echo $clan15_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
            <?php
            if($clan15_teamid ==0 ) {
                echo "<a href='$clan15_hp' target='_blank'>$clan15_clantag</a>";
            } else {
                echo "<a href='index.php?teams-show-$clan15_teamid' target='_blank'>$clan15_clantag</a>";
            }
            ?>
        </td>
        <td style="border-right:solid 1px #000;">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan15_eg; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan16_eg; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_vor_8_name;?><br><b>Website:</b> <?php echo $clan_vor_8_hp; ?>',CAPTION,'Viertelfinale:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan_vor_8_teamid ==0 ) {
				echo "<a href='$clan_vor_8_hp' target='_blank'>$clan_vor_8_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan_vor_8_teamid' target='_blank'>$clan_vor_8_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>; border-right:solid 1px #000;">
            <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan16_name;?><br><b>Website:</b> <?php echo $clan16_hp; ?>',CAPTION,'Vorrunde:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
			<?php
            if($clan16_teamid ==0 ) {
				echo "<a href='$clan16_hp' target='_blank'>$clan16_clantag</a>";
            } else {
				echo "<a href='index.php?teams-show-$clan16_teamid' target='_blank'>$clan16_clantag</a>";
            }
            ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" colspan="2" style="color:<?=$head_font?>;border-left:solid 1px #000;border-top:solid 1px #000;border-right:solid 1px #000;"><b>Spiel um Platz 3<b></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
		<?php if (isset($clan_fin_1_name)) {?>
        <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_platz3_1_name;?><br><b>Website:</b> <?php echo $clan_platz3_1_hp; ?>',CAPTION,'Spielt um Platz3:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
		<?php
        echo "<a href='$clan_platz3_1_hp' target='_blank'>$clan_platz3_1_clantag</a>";
		}
		?>
        </td>
        <td>&nbsp;</td>
    </tr>
	<tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" style="color:<?=$font_alg?>;border-right:solid 1px #000;"><?php echo $clan_platz3_1_ep3; ?> &nbsp; <b>:</b> &nbsp; <?php echo $clan_platz3_2_ep3; ?></td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;">
		<?php if (isset($clan_fin_2_name)) {?>
        <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_platz3_winner_name;?><br><b>Website:</b> <?php echo $clan_platz3_winner_hp; ?><br><b>Preis:</b> <?php echo $clan_platz3_winner_preis3; ?>',CAPTION,'Gewinner Platz3:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
		<?php
        echo "<a href='$clan_platz3_winner_hp' target='_blank'>$clan_platz3_winner_clantag</a>";
		}
		?>
        </td>
    </tr>
	<tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="Bbody" bgcolor="<?=$field_bg?>" style="border:solid 1px <?=$field_border?>;border-right:solid 1px #000;">
		<?php if (isset($clan_fin_2_name)) {?>
        <a href="javascript:void(0)" onmouseover="return coolTip('<img src=include/images/icons/admin/teams.png width=16px height=16px border=0px><br><b>Name:</b> <?php echo $clan_platz3_2_name;?><br><b>Website:</b> <?php echo $clan_platz3_2_hp; ?>',CAPTION,'Spielt um Platz3:',CAPCOLOR,'#ff0000',BGCOLOR,'#000000',FGCOLOR,'#f7f7f7')" onmouseout="nd()"><img src='include/images/icons/admin/teams.png' width="16px" height="16px" border="0px"></a>
		<?php
        echo "<a href='$clan_platz3_2_hp' target='_blank'>$clan_platz3_2_clantag</a>";
		}
		?>
        </td>
        <td>&nbsp;</td>
    </tr>
	<tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" colspan="2" style="border-left:solid 1px #000;border-bottom:solid 1px #000;border-right:solid 1px #000;">&nbsp;</td>
    </tr>
</table>
<div align="center"><br><br><?php echo $footer; ?><br></div>

<!-- TURNIERBAUM ENDE -->
<?php
$design->footer();
?>