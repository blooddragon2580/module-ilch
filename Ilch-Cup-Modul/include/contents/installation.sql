DROP TABLE IF EXISTS `prefix_cup_teams`;

CREATE TABLE `prefix_cup_teams` (
	`id` int(11) NOT NULL auto_increment,
	`teamid` int(11) NOT NULL,
	`clantag` varchar(10) NOT NULL,
	`name` text NOT NULL,
	`gruppe` int(3) NOT NULL,
	`anordnung` int(2) NOT NULL,
	`hp` varchar(50) NOT NULL,
	`viertel` int(11) NOT NULL,
	`halb` int(11) NOT NULL,
	`finale` int(11) NOT NULL,
	`p1` int(11) NOT NULL,
	`p2` int(11) NOT NULL,
	`p3` int(11) NOT NULL,
	`eg` int(2) NOT NULL,
	`ev` int(2) NOT NULL,
	`eh` int(2) NOT NULL,
	`ef` int(2) NOT NULL,
	`ep3` int(2) NOT NULL,
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM;

DROP TABLE IF EXISTS `prefix_cup_config`;

CREATE TABLE `prefix_cup_config` (
	`id` int(11) NOT NULL,
	`gruppe` varchar(11) NOT NULL,
	`tablebg1` varchar(10) NOT NULL,
	`tablebg2` varchar(10) NOT NULL,
	`headbg` varchar(10) NOT NULL,
	`headfont` varchar(10) NOT NULL,
	`fieldbg` varchar(10) NOT NULL,
	`fieldbor` varchar(10) NOT NULL,
	`font1` varchar(10) NOT NULL,
	`font2` varchar(10) NOT NULL,
	`preis1` varchar(50) NOT NULL,
	`preis2` varchar(50) NOT NULL,
	`preis3` varchar(50) NOT NULL,
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM;

INSERT INTO `prefix_cup_config` 
    (`id`, `gruppe`, `tablebg1`, `tablebg2`, `headbg`, `headfont`, `fieldbg`, `fieldbor`, `font1`, `font2`, `preis1`, `preis2`, `preis3`) 
	VALUES
	(1, 'ja', '#e1e1e1', '#ebebeb', '#ffffff', '#000000', '#ffffff', '#cccccc', '#000000', '#2BB4FE', 'Preis1', 'Preis2', 'Preis3');

INSERT INTO `prefix_modules` 
    (`id`,`url`,`name`,`gshow`,`ashow`,`fright`)
    VALUES
    ('','cup','Cup', 0, 1, 1);
