<?php 

defined ('main') or die ( 'no direct access' );
defined ('admin') or die ( 'only admin access' );


$design = new design ( 'Admins Area', 'Admins Area', 2 );
$design->header();
include 'include/includes/css/pa_forum/forum.css.php';

$tpl = new tpl ('forenmod', 1);
  	if(!isset($_POST['forenmod'])) {
			$sql = db_query("SELECT 
					textcolor,
					bgcolor,
					bgimage,
					infotextcolor,
					infobgcolor,
					infobgimage,
					cattextcolor,
					catbgcolor,
					catbghover,
					catbgimage,
					catbgimagehover,
					bgletzter,
					bginfo,
					infountentextcolor,
					infountenbgcolor,
					infountenbgimage,
					info,
					avar,
					stattextcolor,
					statbgcolor,
					statbgimage,
					stat,
					catfestbgcolor,
					catfesthbghover,
					catfestbgimage,
					catfesthbgimagehover,
					infowidth
					FROM prefix_forenmod");

			$forenmod = db_fetch_assoc($sql);
			
			$tpl->set('textcolor' ,$forenmod['textcolor']);
			$tpl->set('bgcolor' ,$forenmod['bgcolor']);
			$tpl->set('bgimage' ,$forenmod['bgimage']);
			$tpl->set('infotextcolor' ,$forenmod['infotextcolor']);
			$tpl->set('infobgcolor' ,$forenmod['infobgcolor']);
			$tpl->set('infobgimage' ,$forenmod['infobgimage']);
			$tpl->set('cattextcolor' ,$forenmod['cattextcolor']);
			$tpl->set('catbgcolor' ,$forenmod['catbgcolor']);
			$tpl->set('catbghover' ,$forenmod['catbghover']);
			$tpl->set('catbgimage' ,$forenmod['catbgimage']);
			$tpl->set('catbgimagehover' ,$forenmod['catbgimagehover']);
			$tpl->set('bgletzter' ,$forenmod['bgletzter']);
			$tpl->set('bginfo' ,$forenmod['bginfo']);
			$tpl->set('infountentextcolor' ,$forenmod['infountentextcolor']);
			$tpl->set('infountenbgcolor' ,$forenmod['infountenbgcolor']);
			$tpl->set('infountenbgimage' ,$forenmod['infountenbgimage']);
			$tpl->set('infoj' ,($forenmod['info'] == 1 ? 'checked' : ''));
			$tpl->set('infon' ,($forenmod['info'] == 0 ? 'checked' : ''));
			$tpl->set('avarj' ,($forenmod['avar'] == 1 ? 'checked' : ''));
			$tpl->set('avarn' ,($forenmod['avar'] == 0 ? 'checked' : ''));
			$tpl->set('stattextcolor' ,$forenmod['stattextcolor']);
			$tpl->set('statbgcolor' ,$forenmod['statbgcolor']);
			$tpl->set('statbgimage' ,$forenmod['statbgimage']);
			$tpl->set('catfestbgcolor' ,$forenmod['catfestbgcolor']);
			$tpl->set('catfesthbghover' ,$forenmod['catfesthbghover']);
			$tpl->set('catfestbgimage' ,$forenmod['catfestbgimage']);
			$tpl->set('catfesthbgimagehover' ,$forenmod['catfesthbgimagehover']);
			$tpl->set('statj' ,($forenmod['stat'] == 1 ? 'checked' : ''));
			$tpl->set('statn' ,($forenmod['stat'] == 0 ? 'checked' : ''));
			$tpl->set('lastj' ,($forenmod['infowidth'] == 1 ? 'checked="checked"' : ''));
			$tpl->set('lastn' ,($forenmod['infowidth'] == 0 ? 'checked' : ''));
		}
		
		else {
		
			db_query("UPDATE prefix_forenmod SET 
				textcolor = '".$_POST['textcolor']."', 
				bgcolor = '".$_POST['bgcolor']."',
				bgimage = '".$_POST['bgimage']."', 
				infotextcolor = '".$_POST['infotextcolor']."',
				infobgcolor = '".$_POST['infobgcolor']."',
				infobgimage = '".$_POST['infobgimage']."',
				cattextcolor = '".$_POST['cattextcolor']."',
				catbgcolor = '".$_POST['catbgcolor']."',
				catbghover = '".$_POST['catbghover']."',
				catbgimage = '".$_POST['catbgimage']."',
				catbgimagehover = '".$_POST['catbgimagehover']."',
				bgletzter = '".$_POST['bgletzter']."',
				bginfo = '".$_POST['bginfo']."',
				infountentextcolor = '".$_POST['infountentextcolor']."',
				infountenbgcolor = '".$_POST['infountenbgcolor']."',
				infountenbgimage = '".$_POST['infountenbgimage']."',
				info = '".$_POST['info']."',
				avar = '".$_POST['avar']."',
				stattextcolor = '".$_POST['stattextcolor']."',
				statbgcolor = '".$_POST['statbgcolor']."',
				statbgimage = '".$_POST['statbgimage']."',
				stat = '".$_POST['stat']."',
				catfestbgcolor = '".$_POST['catfestbgcolor']."',
				catfesthbghover = '".$_POST['catfesthbghover']."',
				catfestbgimage = '".$_POST['catfestbgimage']."',
				catfesthbgimagehover = '".$_POST['catfesthbgimagehover']."',
				infowidth = '".$_POST['infowidth']."'
				");

			$tpl->set('textcolor' ,$_POST['textcolor']);
			$tpl->set('bgcolor' ,$_POST['bgcolor']);
			$tpl->set('bgimage' ,$_POST['bgimage']);
			$tpl->set('infotextcolor' ,$_POST['infotextcolor']);
			$tpl->set('infobgcolor' ,$_POST['infobgcolor']);
			$tpl->set('infobgimage' ,$_POST['infobgimage']);
			$tpl->set('cattextcolor' ,$_POST['cattextcolor']);
			$tpl->set('catbgcolor' ,$_POST['catbgcolor']);
			$tpl->set('catbghover' ,$_POST['catbghover']);
			$tpl->set('catbgimage' ,$_POST['catbgimage']);
			$tpl->set('catbgimagehover' ,$_POST['catbgimagehover']);
			$tpl->set('bgletzter' ,$_POST['bgletzter']);
			$tpl->set('bginfo' ,$_POST['bginfo']);
			$tpl->set('infountentextcolor' ,$_POST['infountentextcolor']);
			$tpl->set('infountenbgcolor' ,$_POST['infountenbgcolor']);
			$tpl->set('infountenbgimage' ,$_POST['infountenbgimage']);
			$tpl->set('info' ,$_POST['info']);
			$tpl->set('avar' ,$_POST['avar']);
			$tpl->set('stattextcolor' ,$_POST['stattextcolor']);
			$tpl->set('statbgcolor' ,$_POST['statbgcolor']);
			$tpl->set('statbgimage' ,$_POST['statbgimage']);
			$tpl->set('stat' ,$_POST['stat']);
			$tpl->set('catfestbgcolor' ,$_POST['catfestbgcolor']);
			$tpl->set('catfesthbghover' ,$_POST['catfesthbghover']);
			$tpl->set('catfestbgimage' ,$_POST['catfestbgimage']);
			$tpl->set('catfesthbgimagehover' ,$_POST['catfesthbgimagehover']);
			$tpl->set('infowidth' ,$_POST['infowidth']);
		}
		
		
		if(!isset($_POST['forenmodpost'])) {
			$sql = db_query("SELECT 
					uebercolor,
					ueberbgcolor,
					ueberbgimage,
					uebertextcolor,
					ueberinfobgcolor,
					ueberinfobgimage,
					posttextcolor,
					postbgcolor,
					postbgimage,
					untentextcolor,
					untenbgcolor,
					untenbgimage,
					infopost,
					postbg
					FROM prefix_forenmod");

			$forenmodpost = db_fetch_assoc($sql);
			$tpl->set('uebercolor' ,$forenmodpost['uebercolor']);
			$tpl->set('ueberbgcolor' ,$forenmodpost['ueberbgcolor']);
			$tpl->set('ueberbgimage' ,$forenmodpost['ueberbgimage']);
			$tpl->set('uebertextcolor' ,$forenmodpost['uebertextcolor']);
			$tpl->set('ueberinfobgcolor' ,$forenmodpost['ueberinfobgcolor']);
			$tpl->set('ueberinfobgimage' ,$forenmodpost['ueberinfobgimage']);
			$tpl->set('posttextcolor' ,$forenmodpost['posttextcolor']);
			$tpl->set('postbgcolor' ,$forenmodpost['postbgcolor']);
			$tpl->set('postbgimage' ,$forenmodpost['postbgimage']);
			$tpl->set('untentextcolor' ,$forenmodpost['untentextcolor']);
			$tpl->set('untenbgcolor' ,$forenmodpost['untenbgcolor']);
			$tpl->set('untenbgimage' ,$forenmodpost['untenbgimage']);
			$tpl->set('infopostj' ,($forenmodpost['infopost'] == 1 ? 'checked' : ''));
			$tpl->set('infopostn' ,($forenmodpost['infopost'] == 0 ? 'checked' : ''));
			$tpl->set('postbg' ,$forenmodpost['postbg']);
		}
		
		else {
		
			db_query("UPDATE prefix_forenmod SET 
				uebercolor = '".$_POST['uebercolor']."', 
				ueberbgcolor = '".$_POST['ueberbgcolor']."',
				ueberbgimage = '".$_POST['ueberbgimage']."', 
				uebertextcolor = '".$_POST['uebertextcolor']."',
				ueberinfobgcolor = '".$_POST['ueberinfobgcolor']."',
				ueberinfobgimage = '".$_POST['ueberinfobgimage']."',
				posttextcolor = '".$_POST['posttextcolor']."',
				postbgcolor = '".$_POST['postbgcolor']."',
				postbgimage = '".$_POST['postbgimage']."',
				untentextcolor = '".$_POST['untentextcolor']."',
				untenbgcolor = '".$_POST['untenbgcolor']."',
				untenbgimage = '".$_POST['untenbgimage']."',
				infopost = '".$_POST['infopost']."',
				postbg = '".$_POST['postbg']."'
				");

			$tpl->set('uebercolor' ,$_POST['uebercolor']);
			$tpl->set('ueberbgcolor' ,$_POST['ueberbgcolor']);
			$tpl->set('ueberbgimage' ,$_POST['ueberbgimage']);
			$tpl->set('uebertextcolor' ,$_POST['uebertextcolor']);
			$tpl->set('ueberinfobgcolor' ,$_POST['ueberinfobgcolor']);
			$tpl->set('ueberinfobgimage' ,$_POST['ueberinfobgimage']);
			$tpl->set('posttextcolor' ,$_POST['posttextcolor']);
			$tpl->set('postbgcolor' ,$_POST['postbgcolor']);
			$tpl->set('postbgimage' ,$_POST['postbgimage']);
			$tpl->set('untentextcolor' ,$_POST['untentextcolor']);
			$tpl->set('untenbgcolor' ,$_POST['untenbgcolor']);
			$tpl->set('untenbgimage' ,$_POST['untenbgimage']);
			$tpl->set('infopost' ,$_POST['infopost']);
			$tpl->set('postbg' ,$_POST['postbg']);
		}
		
		if(!isset($_POST['forenbutton'])) {
			$sql = db_query("SELECT 
					boben,
					bunten,
					bcolor,
					bhoben,
					bhunten,
					bhcolor
					FROM prefix_forenmod");

			$forenbutton = db_fetch_assoc($sql);
			$tpl->set('boben' ,$forenbutton['boben']);
			$tpl->set('bunten' ,$forenbutton['bunten']);
			$tpl->set('bcolor' ,$forenbutton['bcolor']);
			$tpl->set('bhoben' ,$forenbutton['bhoben']);
			$tpl->set('bhunten' ,$forenbutton['bhunten']);
			$tpl->set('bhcolor' ,$forenbutton['bhcolor']);
		}
		
		else {
		
			db_query("UPDATE prefix_forenmod SET 
				boben = '".$_POST['boben']."', 
				bunten = '".$_POST['bunten']."',
				bcolor = '".$_POST['bcolor']."', 
				bhoben = '".$_POST['bhoben']."',
				bhunten = '".$_POST['bhunten']."',
				bhcolor = '".$_POST['bhcolor']."'
				");

			$tpl->set('boben' ,$_POST['boben']);
			$tpl->set('bunten' ,$_POST['bunten']);
			$tpl->set('bcolor' ,$_POST['bcolor']);
			$tpl->set('bhoben' ,$_POST['bhoben']);
			$tpl->set('bhunten' ,$_POST['bhunten']);
			$tpl->set('bhcolor' ,$_POST['bhcolor']);
		}
		

$tpl->out(0);
$design->footer();

?>