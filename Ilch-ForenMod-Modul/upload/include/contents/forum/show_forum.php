<?php 
#   Copyright by: Manuel
#   Support: www.ilch.de


defined ('main') or die ( 'no direct access' );
function user_online_today_liste(){
  $dif = mktime(0,0,0,date('m'),date('d'),date('Y'));
    $ergt = db_query("SELECT a.id, a.avatar, a.name, a.recht, a.llogin, b.bez, a.spezrank FROM `prefix_user` a LEFT JOIN prefix_ranks b ON b.id = a.spezrank  WHERE a.llogin > '". $dif."' ORDER BY llogin DESC, recht ASC");
    while($rowt = db_fetch_object($ergt)) {
      if ( $rowt->spezrank <> 0 ) {
      $OnListe .= '<a class="kleintext" href="index.php?user-details-'.$rowt->id.'">'.($rowt->name).'</a>, ';
    } else {
      $OnListe .= '<a class="kleintext" href="index.php?user-details-'.$rowt->id.'">'.($rowt->name).'</a>, ';
      }
  }
  return ($OnListe);
      }
	  


$title = $allgAr['title'].' :: Forum';
$hmenu = $extented_forum_menu.'Forum'.$extented_forum_menu_sufix;
$design = new design ( $title , $hmenu, 1);
$design->header();
include 'include/includes/css/pa_forum/forum.css.php';
if ($menu->get(1) == 'markallasread') {
  user_markallasread ();
}


$tpl = new tpl ( 'forum/showforum' );
$tpl->out (0);

$category_array = array();
$forum_array = array();

$q = "SELECT
  a.id, a.cid, a.name, a.besch,
  a.topics, a.posts, b.name as topic, b.art,
  c.id as pid, c.tid, b.rep, b.erst, b.icon, c.erst, c.time,
  a.cid, k.id as kid, k.name as cname
FROM prefix_forums a
  LEFT JOIN prefix_forumcats k ON k.id = a.cid
  LEFT JOIN prefix_posts c ON a.last_post_id = c.id
  LEFT JOIN prefix_topics b ON c.tid = b.id
	
  LEFT JOIN prefix_groupusers vg ON vg.uid = ".$_SESSION['authid']." AND vg.gid = a.view
  LEFT JOIN prefix_groupusers rg ON rg.uid = ".$_SESSION['authid']." AND rg.gid = a.reply
  LEFT JOIN prefix_groupusers sg ON sg.uid = ".$_SESSION['authid']." AND sg.gid = a.start
	
WHERE ((".$_SESSION['authright']." <= a.view AND a.view < 1) 
   OR (".$_SESSION['authright']." <= a.reply AND a.reply < 1)
   OR (".$_SESSION['authright']." <= a.start AND a.start < 1)
	 OR vg.fid IS NOT NULL
	 OR rg.fid IS NOT NULL
	 OR sg.fid IS NOT NULL
	 OR -9 = ".$_SESSION['authright'].")
	 AND k.cid = 0
ORDER BY k.pos, a.pos";
$erg1 = db_query($q);
$xcid = 0;
while ($r = db_fetch_assoc($erg1) ) {
	$avatar = @db_result(db_query("SELECT avatar FROM `prefix_user` WHERE name = '".$r['erst']."'"),0);
	if ($avatar == '') {$ava = 'include/includes/css/pa_forum/images/noavatar.png';} else {$ava = $avatar;}
	$r['topicl'] = $r['topic'];
	$r['topic']  = html_enc_substr($r['topic'],0,23);
	if ($r['rep'] >= 30) {
		$r['ORD'] = (forum_get_ordner($r['time'],$r['id'])).'brisant';
	}else{
		$r['ORD'] = forum_get_ordner($r['time'],$r['id']);
	}
	if ($r['art'] == 1) {
		$r['ORD'] = (forum_get_ordner($r['time'],$r['id'])).'fest';
	}else{
		$r['ORD'] = forum_get_ordner($r['time'],$r['id']);
	}
  $r['mods']   = getmods($r['id']);
  if ($r['time'] == null) {$r['datum'] = 'keine Eintr&auml;ge vorhanden!';} else {
  $r['datum']  = date('d.m.y - H:i', $r['time']);}
  if ($r['icon'] == 0) {$r['icon'] = '<img src="include/includes/css/pa_forum/images/themen_icon/keins.png" alt="" />';} else {
  $r['icon'] = '<img src="include/includes/css/pa_forum/images/themen_icon/'.$r['icon'].'.png" alt="" />';}
  $r['page']   = ceil ( ($r['rep']+1)  / $allgAr['Fpanz'] );
  $r['poster'] = $r['erst'];
  $r['name'] = ((strlen($r['name'])<63) ? $r['name'] : substr($r['name'],0,60).' ...');
  $r['avatar'] = $ava;
  
  $tpl->set_ar ($r);
  
  if ($r['cid'] <> $xcid) {
    $tpl->out(1);
    //Unterkategorien
    $sql = db_query("SELECT DISTINCT a.name as cname, a.id as cid FROM `prefix_forumcats` a LEFT JOIN `prefix_forums` b ON a.id = b.cid WHERE a.cid = {$r['cid']} AND a.id = b.cid ORDER BY a.pos, a.name");
    while ($ucat = db_fetch_assoc($sql)) {
		$ucat['cname'] = ((strlen($ucat['cname'])<53) ? $ucat['cname'] : substr($ucat['cname'],0,50).' ...');
      $tpl->set_ar_out($ucat,2);
    }
    //Unterkategorien - Ende
    $xcid = $r['cid'];
	$r['count'] = db_result(db_query("SELECT COUNT(ID) FROM `prefix_forums` WHERE cid = ".$r['cid']),0);
	$r['themen'] = db_result(db_query("SELECT SUM(topics) FROM `prefix_forums` WHERE cid = ".$r['cid']),0);
	$r['post'] = db_result(db_query("SELECT SUM(posts) FROM `prefix_forums` WHERE cid = ".$r['cid']),0);
  }
  $tpl->set_ar_out($r,3);
}

# statistic #
$ges_online_user = ges_online();
if (user_bday_liste() >= 1) {$bday = '<img src="" alt="" />';} else {$bday = '';}
$stats_array = array (
  //'privmsgpopup' => check_for_pm_popup (),
  'topics' => db_result(db_query("SELECT COUNT(ID) FROM `prefix_topics`"),0),
  'posts' => db_result(db_query("SELECT COUNT(ID) FROM `prefix_posts`"),0),
  'users' => db_result(db_query("SELECT COUNT(ID) FROM `prefix_user`"),0),
  'istsind' => ( $ges_online_user > 1 ? 'sind' : 'ist' ),
  'gesonline' => $ges_online_user,
  'gastonline' => ges_gast_online(),
  'useronline' => ges_user_online(),
  'userliste' => user_online_liste(),
  'userliste_today' => user_online_today_liste(),
  'newbenutzer' => user_lastreg_liste(),
  'bday' => user_bday_liste(),
  'imgbday' => $bday,
  'foren' => db_result(db_query("SELECT COUNT(ID) FROM `prefix_forums`"),0)
);

$tpl->set_ar_out($stats_array,4);

$design->footer();
?>