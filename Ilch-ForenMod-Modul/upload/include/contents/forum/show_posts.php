<?php
#   Copyright by: Manuel
#   Support: www.ilch.de


defined ('main') or die ( 'no direct access' );

# check ob ein fehler aufgetreten ist.
check_forum_failure($forum_failure);

# toipc als gelesen markieren
$_SESSION['forumSEE'][$fid][$tid] = time();

$title = $allgAr['title'].' :: Forum :: '.$aktTopicRow['name'].' :: Beitr&auml;ge zeigen';
$hmenu  = $extented_forum_menu.'<a class="smalfont" href="index.php?forum">Forum</a><b> &raquo; </b>'.aktForumCats($aktForumRow['kat']).'<b> &raquo; </b><a class="smalfont" href="index.php?forum-showtopics-'.$fid.'">'.$aktForumRow['name'].'</a><b> &raquo; </b>';
$hmenu .= $aktTopicRow['name'].$extented_forum_menu_sufix;
$design = new design ( $title , $hmenu, 1);
$design->header();
include 'include/includes/css/pa_forum/forum.css.php';

# Topic Hits werden eins hochgesetzt.
db_query('UPDATE `prefix_topics` SET hit = hit + 1 WHERE id = "'.$tid.'"');

# mehrere seiten fals gefordert
$limit = $allgAr['Fpanz'];  // Limit
$page = ($menu->getA(3) == 'p' ? $menu->getE(3) : 1 );
$MPL = db_make_sites ($page , "WHERE tid = ".$tid , $limit , 'index.php?forum-showposts-'.$tid , 'posts' );
$anfang = ($page - 1) * $limit;

$antworten = '';
if (($aktTopicRow['stat'] == 1 AND $forum_rights['reply'] == TRUE) OR ($_SESSION['authright'] <= '-7' OR $forum_rights['mods'] == TRUE)) {
  $antworten = '<a class="forumbutton" href="index.php?forum-newpost-'.$tid.'">'.$lang['answer'].'</a>';
}

$class = 'Cmite';

$tpl = new tpl ( 'forum/showpost' );
$ar = array (
  'SITELINK' => $MPL,
  'tid' => $tid,
	'ANTWORTEN' => $antworten,
	'TOPICNAME' => $aktTopicRow['name']
);
$tpl->set_ar_out($ar,0);
$i = $anfang +1;
$ges_ar = array ('wurstegal', 'maennlich', 'weiblich');
$erg = db_query("SELECT geschlecht, prefix_posts.id,txt,edit,lastname,date,time,erstid,erst,sig,recht,llogin,avatar,homepage,posts FROM `prefix_posts` LEFT JOIN prefix_user ON prefix_posts.erstid = prefix_user.id WHERE tid = ".$tid." ORDER BY time LIMIT ".$anfang.",".$limit);
while($row = db_fetch_assoc($erg)) {

	$class = ( $class == 'Cnorm' ? 'Cmite' : 'Cnorm' );
################################################################# Auszeichnung Icon Ausgabe #######################
$auszeichiconausgabe = '';
    $str = @db_result ( db_query ("SELECT `auszeichnung` FROM prefix_user WHERE id = ".$row['erstid']) , 0 );
    $auszeichiconarr = explode("#", $str);
    foreach($auszeichiconarr as $value)
    {
	if (file_exists('include/images/profilmod/auszeichnung/'.$value) and !empty($value) )
    {
    $auszeichiconausgabe .=  '<img width="20" onmouseover="Tip(\'<img src=&quot;include/images/profilmod/auszeichnung/'.$value.'&quot; style=&quot;width:80px;&quot; alt=&quot;&quot; /><br />'.substr($value,0,-4).'\')" onmouseout="UnTip()" src="include/images/profilmod/auszeichnung/'.$value.'" alt="" />';
    } else {
    $auszeichiconausgabe .=  '';
    }
    }
##################################################
	# define some vars.
	$row['sig'] = ( empty($row['sig']) ? '' : '<br /><hr style="width: 50%;" align="left">'.bbcode($row['sig']) );
	$row['TID'] = $tid;
	$row['class'] = $class;
	$row['date'] = date ('d.m.Y', $row['time'] );
	$row['zeit'] = date ('H:i:s', $row['time'] );
	$row['delete'] = '';
	$row['change'] = '';
	$row['ers'] = $row['erst'];
	if (!is_numeric($row['geschlecht'])) { $row['geschlecht'] = 0; }
	if (file_exists($row['avatar'])) { $row['avatar'] = '<img src="'.$row['avatar'].'" alt="User Pic" border="0" width="45" />'; }
	elseif ($allgAr['forum_default_avatar']) { $row['avatar'] = '<img src="include/includes/css/pa_forum/images/no-avatar.png" width="45" alt="User Pic" border="0" />'; }
 	else { $row['avatar'] = ''; }
	$rechtname = @db_result(db_query('SELECT name FROM prefix_grundrechte WHERE id ='.$row['recht']),0);
	$row['rechtname'] = $rechtname;
	if ($row['edit'] > 0) {$row['edit'] = '<span class="kleintext">Dieser Bertrag wurde '.$row['edit'].' mal bearbeitet.</span><br /><span class="kleintext">zuletzt von '.$row['lastname'].' am: '.$row['date'].'</span>';} else {$row['edit'] = '';}
	$row['danke'] = '';
	$row['ausgaben'] = $auszeichiconausgabe;
	$row['online'] = '<span class="kleintext">zuletzt online: '.date('d.m.Y - H:i', $row['llogin']).'</span>';
	$post = @db_result(db_query ("SELECT COUNT(id) FROM prefix_posts WHERE erstid = ".$row['erstid']),0);
		
	if ($post >= 250 ) {
		$row['aktiv'] = '<img onmouseover="Tip(\'<div align=&quot;center&quot;><img style=&quot;width:80px;border:none&quot; src=&quot;include/images/autoauszeichnung/sehr aktives Mitglied.png&quot;><br />sehr aktives Mitglied</div>\')" onmouseout="UnTip()" src="include/images/autoauszeichnung/sehr aktives Mitglied.png" alt="" width="20" />';
	} else{
		$row['aktiv'] = '';
	}
	
	if ($thx >= 100 ) {
		$row['dank'] = '<img onmouseover="Tip(\'<div align=&quot;center&quot;><img style=&quot;width:80px;border:none&quot; src=&quot;include/images/autoauszeichnung/100 hilfreiche Beitraege.png&quot;><br />100 hilfreiche Beitr&auml;ge</div>\')" onmouseout="UnTip()" src="include/images/autoauszeichnung/100 hilfreiche Beitraege.png" alt="" width="20" />';
	} else{
		$row['dank'] = '';
	}
    $row['rang']   = userrang ($post,$row['erstid']);
	$row['txt']    = (isset($_GET['such']) ? markword(bbcode ($row['txt']),$_GET['such']) : bbcode ($row['txt']) );
	$row['i']      = $i;
    $row['page']   = $page;

	if ( $post != 0 ) {
		$row['erst'] = '<a href="index.php?user-details-'.$row['erstid'].'"><b>'.$row['erst'].'</b></a>';
	} elseif ( $row['erstid'] != 0 ) {
        $row['rang'] = 'gel&ouml;schter User';
    }

	if ($forum_rights['mods'] == TRUE AND $i>1) {
	  $row['delete'] = '<div id="forum__post" style="margin-right:1px;"><a href="index.php?forum-delpost-'.$tid.'-'.$row['id'].'" class="delete"><img class="deletehover" src="include/includes/css/pa_forum/images/forum_icon/delete.png" alt="" title="Beitrag von '.$row['ers'].' l&ouml;schen?" /></a></div>';
	}
	if ( $forum_rights['reply'] == TRUE AND loggedin() ) {
	  $row['change'] = '<div id="forum__post" style="margin-right:1px;"><a href="index.php?forum-editpost-'.$tid.'-'.$row['id'].'" class="edit"><img class="edithover" src="include/includes/css/pa_forum/images/forum_icon/edit.png" alt="" title="Beitrag von '.$row['ers'].' bearbeiten!" /></a></div>';
	}
	
	$row['profil'] = '<a href="?user-details-'.$row['erstid'].'"><img src="include/includes/css/pa_forum/images/forum_icon/forum_profil.png" alt="" title="Profil von '.$row['ers'].' ansehen" align="absmiddle" /></a>';
	if ($row['homepage'] != 0) {$row['hp'] = '<img src="include/includes/css/pa_forum/images/forum_icon/forum_website_none.png" alt="" />';} else {
	$row['hp'] = '<a href="'.$row['homepage'].'" target="_blank"><img src="include/includes/css/pa_forum/images/forum_icon/forum_website.png" alt=""  title="Webseite von '.$row['ers'].' besuchen" align="absmiddle" /></a>';}
	$row['pn'] = '<a href="?forum-privmsg-new=0&empfid='.$row['erstid'].'"><img src="include/includes/css/pa_forum/images/forum_icon/forum_pn.png" alt="" title="PN an '.$row['ers'].' schreiben" align="absmiddle" /></a>';
	if ($_SESSION['authid'] >= 1) {
	$row['druck'] = '<a href="javascript:window.print()"><img src="include/includes/css/pa_forum/images/forum_icon/forum_drucken.png" alt="" title="diese Seite drucken" align="absmiddle" /></a>';} else {$row['druck'] = '';}
	//Forumex - begin
    $row['txt'] = FE_Vote2HTML($row['id'],$row['txt']);
    //Forumex - end
	$row['posts']  = ($row['posts']?'Posts: '.$post:'').'';
// Danke-Link anzeigen oder ausblenden falls user == ersteller oder Gast
if ($row['erstid'] == $_SESSION['authid'] or $_SESSION['authid'] == 0) {
$row['THX'] = '<img src="include/includes/css/pa_forum/images/forum_icon/forum_danke_none.png" align="absmiddle" alt="" />';
} else {
# Zufallszahl generieren um Missbrauch vorzubeugen
if (!isset($_SESSION['thx_rand']) OR empty($_SESSION['thx_rand'][$row['id']])) {
$_SESSION['thx_rand'][$row['id']] = rand(000,999);
}
$row['THX'] = '<a href="index.php?danke-'.$row['id'].'-'.$_SESSION['thx_rand'][$row['id']].'-'.$tid.'-'.$row['erstid'].'-'.$_SESSION['authid'].'-'.$_SESSION['authname'].'" style="text-decoration:none;"><img src="include/includes/css/pa_forum/images/forum_icon/forum_danke.png" align="top" alt=""  title="bei '.$row['ers'].' bedanken" align="absmiddle" /></a>';

}

$thx = db_count_query("SELECT COUNT(id) FROM `prefix_danke` WHERE erstid = ".$row['id']."",0);
// Ausgeben der Danke-Liste im Post
$thxcount = db_count_query("SELECT COUNT(id) FROM `prefix_danke` WHERE pid = ".$row['id']."");
if ($thxcount >= 1 ) {
$row['danke'] .= '<br /><strong class="kleintext">Es bedankten sich '.$thxcount.' User</strong><br />';

$thx_qry = db_query("SELECT bedankername,bedankerid,id FROM `prefix_danke` WHERE pid = ".$row['id']." ORDER BY id DESC LIMIT 1");
while ($thx_row = db_fetch_assoc($thx_qry)) {

$row['danke'] .= '<span class="kleintext">letzter:&nbsp;<a href="index.php?user-details-'.$thx_row['bedankerid'].'">'.$thx_row['bedankername'].'</a></span> ';

}
}
else
{
$row['danke'] .= '';
}
	//Forumex - begin
    $row['txt'] = FE_Vote2HTML($row['id'],$row['txt']);
    //Forumex - end
	$tpl->set_ar_out($row,1);

  $i++;
}

$tpl->set_ar_out( array ( 'SITELINK' => $MPL, 'ANTWORTEN' => $antworten ) , 2 );
// anfang qpost
if (loggedin()) {

$dppk_time = time();
$time = time();
if (!isset($_SESSION['klicktime'])) { $_SESSION['klicktime'] = 0; }

$topic = '';
$txt   = '';
$xnn   = '';

if (isset($_POST['txt_qp'])) {
  $txt = trim(escape($_POST['txt_qp'], 'textarea'));
}


$tpl = new tpl ('forum/qpost');
   $ar = array (
     'txt_qp'    => escape_for_fields(unescape($txt)),
     'tid'    => $tid,
     'antispam'=> get_antispam('qpost',1)

   );

   $tpl->set_ar_out($ar,1);

if (($_SESSION['klicktime'] + 150) > $dppk_time OR empty($txt) OR !empty($_POST['priview']) OR (empty($_POST['Gname']) AND !loggedin())) {



}
else
{
# save qpost
  $_SESSION['klicktime'] = $dppk_time;

  $design = new design ( $title , $hmenu, 1);
  $design->header();

  if (loggedin()) {
    $uid = $_SESSION['authid'];
                $erst = escape($_SESSION['authname'],'string');
          db_query("UPDATE `prefix_user` set posts = posts+1 WHERE id = ".$uid);
  } else  {
          $erst = $xnn;
                $uid = 0;
  }
  db_query ("INSERT INTO `prefix_posts` (tid,fid,erst,erstid,time,txt) VALUES ( ".$tid.", ".$fid.", '".$erst."', ".$uid.", ".$time.", '".$txt."')");
  $pid = db_last_id();

        db_query("UPDATE `prefix_topics` SET last_post_id = ".$pid.", rep = rep + 1 WHERE id = ".$tid);
        db_query("UPDATE `prefix_forums` SET posts = posts + 1, last_post_id = ".$pid." WHERE id = ".$fid );
        $page = ceil ( ($aktTopicRow['rep']+1)  / $allgAr['Fpanz'] );
          # topic als gelesen markieren
  $_SESSION['forumSEE'][$fid][$tid] = time();

        wd ( array (
          $lang['backtotopic'] => 'index.php?forum-showposts-'.$tid.'-p'.$page.'#'.$pid,
                $lang['backtotopicoverview'] => 'index.php?forum-showtopics-'.$fid
        ) , $lang['createpostsuccessful'] , 3 );
}
}

$tpl = new tpl ( 'forum/showpost' );
// end qpost
if (loggedin()) {
  if ($menu->get(3) == 'topicalert') {
    if (1 == db_result(db_query("SELECT COUNT(*) FROM prefix_topic_alerts WHERE uid = ".$_SESSION['authid']." AND tid = ".$tid),0)) {
      db_query("DELETE FROM prefix_topic_alerts WHERE uid = ".$_SESSION['authid']." AND tid = ".$tid);
    } else {
      db_query("INSERT INTO prefix_topic_alerts (tid,uid) VALUES (".$tid.", ".$_SESSION['authid'].")");
    }
  }

  echo '<span class="kleintext"><b>Optionen:</b></span> ';
  if (1 == db_result(db_query("SELECT COUNT(*) FROM prefix_topic_alerts WHERE uid = ".$_SESSION['authid']." AND tid = ".$tid),0)) {
    echo '<a class="forumbutton" href="index.php?forum-showposts-'.$tid.'-topicalert">keine E-Mail mehr erhalten</a><br />';
  } else {
    echo '<a class="forumbutton" href="index.php?forum-showposts-'.$tid.'-topicalert">bei Antwort E-Mail senden</a><br />';
  }
}

if ( $forum_rights['mods'] == TRUE ) {
  $tpl->set ( 'status', ($aktTopicRow['stat'] == 1 ? $lang['close'] : $lang['open'] ) );
	$tpl->set ( 'festnorm', ($aktTopicRow['art'] == 0 ? $lang['fixedtopic'] : $lang['normaltopic'] ) );
	$tpl->set('tid',$tid);
	$tpl->out(3);
}
$design->footer();
?>