<?php
# ilchClan Script (c) by Manuel Staechele
# Installation file (c) by Ithron
# Support: http://www.ilch.de

defined ('main') or die ( 'no direct access' );
if(user_has_admin_right($menu,false) == false)
	die ( 'F&uuml;r diese Installation ben&ouml;tigt man Administratorenrechte !<br /><a href="index.php">Zur Startseite</a>' );


// Script Konfiguration
$scripter		= '<a class="backup" href="http://pintura-arts.de/index.php?user-details-9" target="_blank">lutzip0</a> <a class="backup" href="http://pintura-arts.de" target="_blank">Pintura-Arts.de</a>';								// Name des Autors des Moduls
$script_name	= 'Pintura Forum Mod';				// Name des Moduls
$script_vers	= '1.0';								// Version des Moduls
$ilch_vers		= '1.1';								// Version des ilchClan Scripts
$ilch_update	= 'P';									// Update des ilchClan Scripts
$erfolg			= '';									// Benutzerdefinierte Erfolgsnachricht
$fehler			= '';									// Benutzerdefinierte Fehlermeldung
// Ende der Konfiguration

$title = 'INSTALLATION:&nbsp;&nbsp;'.$script_name;
$hmenu  = $script_name.' Vers.: '.$script_vers.' f&uuml;r ilchClan '.$ilch_vers.''.$ilch_update;
$design = new design ( $title , $hmenu, 1);
$design->header();


if(!isset($_POST['do']))
{
?>
		<link rel="stylesheet" type="text/css" href="http://referenzen.pintura-arts.de/lutzip0/install_css/install.css" />
		<form action="index.php?install" method="POST">
		<input type="hidden" name="do" value="1">
		<div id="cont">
			<div id="logo"><div class="pintura"><a href="http://pintura-arts.de/" target="_blank" title="Webseite des Autor besuchen!"><img src="http://referenzen.pintura-arts.de/lutzip0/install_css/pintura-logo.png" alt="" /></a></div><div class="install"></div></div>
		 <div id="instal">
		 
			<div id="text">
				<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center">
					<tr>
						<td colspan="2"><strong><u>Informationen:</u></strong></td>
					</tr>
					<tr>
						<td width="20%"><strong>Modulname:</strong></td><td><?php echo $script_name; ?></td>
					</tr>
					<tr>
						<td><strong>Version:</strong></td><td><?php echo $script_vers; ?></td>
					</tr>
					<tr>
						<td><strong>Entwickler:</strong></td><td><?php echo $scripter; ?></td>
					</tr>
					<tr>				
						<td colspan="2"><br />Entwickelt f&uuml;r ilchClan Version <strong><?php echo $ilch_vers; ?> <?php echo $ilch_update; ?></strong><br />
							<br />
							<i>Andere Versionen k&ouml;nnen eventuell Fehler verursachen!</i>
							<br />
							<strong><u>Wichtig:</u></strong><br /><br />
							Machen Sie zuerst ein <a href="admin.php?backup" target="_blank" class="backup" title="Datenbank Backup erstellen!">Backup</a> Ihrer Datenbank!<br />
							<br />
						</td>
					</tr>
				</table>
			</div>
		  </div>
		 
			<div id="absenden"><input class="absenden" type="submit" value="Installieren" /></div>
		 
		</div>
		</form>
<?php
}
elseif ($_POST['do'] == '1')
{
	$error = '';
	$sql_file = implode('',file('include/contents/install.sql'));
	$sql_file = preg_replace ("/(\015\012|\015|\012)/", "\n", $sql_file);
	$sql_statements = explode(";\n",$sql_file);
	foreach ( $sql_statements as $sql_statement )
	{
	  if ( trim($sql_statement) != '' )
	  {
		#echo '<pre>'.$sql_statement.'</pre><hr>';
		db_query($sql_statement) OR $error .= mysql_errno().': '.mysql_error().'<br />';
	  }
	}
	
	
	// Ausgabe
?>
		<link rel="stylesheet" type="text/css" href="http://referenzen.pintura-arts.de/lutzip0/install_css/install.css" />
		<div id="cont">
			<div id="logo"><div class="pintura"><a href="http://pintura-arts.de/" target="_blank" title="Webseite des Autor besuchen!"><img src="http://referenzen.pintura-arts.de/lutzip0/install_css/pintura-logo.png" alt="" /></a></div><div class="install"></div></div>
		 <div id="instal">
		 
			<div style="margin: 5px 0;">
<?php
	
	if(!empty($error))
	{
		if(empty($fehler))
		{
			$fehler = 'Es sind Fehler bei der Installation aufgetreten!<br />Bitte benachrichtigen Sie den Entwickler.';
		}
		$fehler .= '<br /><br />Oben sollten Sie eine ausf&uuml;hrlichere Fehlermeldung sehen<br />(ab ilchClan Version 1.1 I).';
		
		echo $fehler.'<br /><br /><hr /><br /><strong style="text-decoration:underline;">Fehlermeldungen:</strong><br /><br /><span style="color:#FF0000;font-size:bold;">'.$error.'</span>';
	}
	else
	{
		if(empty($erfolg))
		{
			$erfolg = 'Die Installation wurde erfolgreich abgeschlossen!';
		}
		if(@unlink('include/contents/install.php') && @unlink('include/contents/install.sql'))
		{
			$erfolg .= '<br /><br />Diese Installationsdateien wurden erfolgreich gel&ouml;scht. Es muss nichts mehr getan werden.';
		}
		else
		{
			$erfolg .= '<br /><br /><strong>Die Installationsdateien konnten nicht automatisch gel&ouml;scht werden. L&ouml;schen Sie folgende Dateien:</strong><br /><br /><i>include/contents/install.php</i><br /><i>include/contents/install.sql</i>';
		}
		
		echo $erfolg;
	}

?>
			<br />
			<br />
		    </div>
		  </div>
		   <div id="absenden">
			<button class="absenden" onclick="javascript:window.location.href = 'index.php';">Auf die Startseite</button>
		   </div>
		</div>
<?php
}
$design->footer();
?>