DROP TABLE IF EXISTS `prefix_forenmod`;
DROP TABLE IF EXISTS `prefix_danke`;
DROP TABLE IF EXISTS `prefix_posts_poll`;

ALTER TABLE prefix_topics ADD icon int(2) NOT NULL DEFAULT '0';
ALTER TABLE prefix_posts ADD edit int(100) NOT NULL DEFAULT '0';
ALTER TABLE prefix_posts ADD lastname varchar(100) NOT NULL;
ALTER TABLE prefix_posts ADD date varchar(100) NOT NULL;
ALTER TABLE prefix_user ADD auszeichnung text NOT NULL;

CREATE TABLE IF NOT EXISTS `prefix_posts_poll` (
`post_id` MEDIUMINT( 9 ) NOT NULL ,
`voters` TEXT NOT NULL ,
`results` TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS `prefix_forenmod` (
  `statbgcolor` varchar(100) DEFAULT NULL,
  `statbgimage` varchar(100) DEFAULT NULL,
  `stat` varchar(2) NOT NULL DEFAULT '0',
  `textcolor` varchar(100) DEFAULT NULL,
  `bgcolor` varchar(100) DEFAULT NULL,
  `bgimage` varchar(100) DEFAULT NULL,
  `infotextcolor` varchar(100) DEFAULT NULL,
  `infowidth` varchar(10) NOT NULL,
  `infobgcolor` varchar(100) DEFAULT NULL,
  `infobgimage` varchar(100) DEFAULT NULL,
  `cattextcolor` varchar(100) DEFAULT NULL,
  `catbgcolor` varchar(100) DEFAULT NULL,
  `catbghover` varchar(100) DEFAULT NULL,
  `catbgimage` varchar(100) DEFAULT NULL,
  `catbgimagehover` varchar(100) DEFAULT NULL,
  `catfestbgcolor` varchar(100) NOT NULL,
  `catfesthbghover` varchar(100) NOT NULL,
  `catfestbgimage` varchar(100) NOT NULL,
  `catfesthbgimagehover` varchar(100) NOT NULL,
  `bgletzter` varchar(100) DEFAULT NULL,
  `bginfo` varchar(100) DEFAULT NULL,
  `infountentextcolor` varchar(100) DEFAULT NULL,
  `infountenbgcolor` varchar(100) DEFAULT NULL,
  `infountenbgimage` varchar(100) DEFAULT NULL,
  `info` varchar(2) NOT NULL DEFAULT '0',
  `avar` varchar(2) NOT NULL DEFAULT '0',
  `stattextcolor` varchar(100) DEFAULT NULL,
  `uebercolor` varchar(100) NOT NULL,
  `ueberbgcolor` varchar(100) NOT NULL,
  `ueberbgimage` varchar(100) NOT NULL,
  `uebertextcolor` varchar(100) NOT NULL,
  `ueberinfobgcolor` varchar(100) NOT NULL,
  `ueberinfobgimage` varchar(100) NOT NULL,
  `posttextcolor` varchar(100) NOT NULL,
  `postbgcolor` varchar(100) NOT NULL,
  `postbgimage` varchar(100) NOT NULL,
  `untentextcolor` varchar(100) NOT NULL,
  `untenbgcolor` varchar(100) NOT NULL,
  `untenbgimage` varchar(100) NOT NULL,
  `infopost` varchar(2) NOT NULL,
  `postbg` varchar(100) NOT NULL,
  `boben` varchar(7) NOT NULL,
  `bunten` varchar(7) NOT NULL,
  `bhoben` varchar(7) NOT NULL,
  `bhunten` varchar(7) NOT NULL,
  `bcolor` varchar(7) NOT NULL,
  `bhcolor` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO `prefix_forenmod` (`statbgcolor`, `statbgimage`, `stat`, `textcolor`, `bgcolor`, `bgimage`, `infotextcolor`, `infowidth`, `infobgcolor`, `infobgimage`, `cattextcolor`, `catbgcolor`, `catbghover`, `catbgimage`, `catbgimagehover`, `catfestbgcolor`, `catfesthbghover`, `catfestbgimage`, `catfesthbgimagehover`, `bgletzter`, `bginfo`, `infountentextcolor`, `infountenbgcolor`, `infountenbgimage`, `info`, `avar`, `stattextcolor`, `uebercolor`, `ueberbgcolor`, `ueberbgimage`, `uebertextcolor`, `ueberinfobgcolor`, `ueberinfobgimage`, `posttextcolor`, `postbgcolor`, `postbgimage`, `untentextcolor`, `untenbgcolor`, `untenbgimage`, `infopost`, `postbg`, `boben`, `bunten`, `bhoben`, `bhunten`, `bcolor`, `bhcolor`) VALUES
('640000', '', '1', 'DEDEDE', '950101', '', 'DEDEDE', '1', 'BD3232', '', 'DEDEDE', '640000', '480000', '', '', '003900', '005000', '', '', '380000', '280000', 'DEDEDE', '480000', '', '1', '1', 'EDEDED', 'DEDEDE', '950101', '', '000000', 'BD3232', 'oben.png', '000000', 'DEDEDE', '', 'DEDEDE', '480000', '', '1', '380000', 'FFFFFF', 'DDDDDD', 'FFFFFF', '1CFFF0', '000000', 'ff0000');

CREATE TABLE IF NOT EXISTS `prefix_posts_poll` (
`post_id` MEDIUMINT( 9 ) NOT NULL ,
`voters` TEXT NOT NULL ,
`results` TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS `prefix_danke` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `erstid` int(11) NOT NULL,
  `erstname` varchar(30) NOT NULL,
  `bedankerid` int(11) NOT NULL,
  `bedankername` varchar(30) NOT NULL,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='sag Danke by GeCk0';