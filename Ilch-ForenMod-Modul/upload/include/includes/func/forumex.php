<?php
// Erstellt von Mairu
defined ('main') or die ('no direct access');

//Optionen
//An -> true | Aus -> false
$showResultBeforeVote = false; //Ergebnis schon bevor man selbst gevotet hat anschauen
$saveAndShowVotersOpinion = false; //Speichere wer was gew�hlt hat und zeige es sp�ter an

//�berpr�ft ob in einem Text eine Umfrage vorkommt, wenn nicht wird false zur�ckgegeben,
//wenn ja wird ein Array in Form array(
//'question' => 'Frage', 'options' => array('1' => 'Antwort1', '2' => 'Antwort2', ...));
//zur�ckgegeben
//Wenn der 2. Parameter $remove true ist, wird der Umfragetext ausgeschnitten und im Output ein Eintrag 'offset' gemacht,
//in dem die Position im Text gespeichert wird, wo der Umfragetext war
function FE_ParseVote(&$text, $remove = false){
    $vote_start = strpos($text, '[vote]');
    $vote_end = strpos($text, '[/vote]');

    if ($vote_start === false OR $vote_end === false OR $vote_start > $vote_end) {
        return false;
    }

    $vote_txt = substr($text, $vote_start + 6, $vote_end - 6);

    if (preg_match('%\[question](.*)\[/question]%i', $vote_txt, $patterns) == 1) {
        $output = array('question' => $patterns[1], 'options' => array());

        if (preg_match_all('%\[option=(\d+)](.*)%', $vote_txt, $patterns) > 1) {
            foreach ($patterns[1] as $key => $value){
                $output['options'][$value] = $patterns[2][$key];
            }
        } else {
            return false;
        }
    } else {
        return false;
    }

    if ($remove !== false) {
        $text = substr($text, 0, $vote_start) . substr($text, $vote_end + 7);
        $output['offset'] = $vote_start;
    }

    return $output;
}

// Wandelt den Votecode aus dem Forum in HTML um
function FE_Vote2HTML ($post_id, $txt, $preview = 0) // FE_Vote2HTML(int Postid, string Posttext,[int View])   View: 0 = Normal; 1 = Preview; 2 = Ergebnis
{
    if ($preview == true AND $preview < 2) $preview = 1;

    $voteAr = FE_ParseVote($txt, true);

    if ($voteAr === false) {
        return $txt;
    }

    switch ($preview) {
        case 0:
            //Anzeige normal im Post
            $erg = db_query('SELECT * FROM prefix_posts_poll WHERE post_id = ' . $post_id);
            if (db_num_rows($erg) == 1) {
                $row = db_fetch_assoc($erg);
                $voters = explode('#', $row['voters']);
                $results = explode('#', $row['results']);
                $gesamt = array_sum($results);
                if ($_SESSION['authid'] == 0) $voter = getip();
                else $voter = $_SESSION['authid'];

                if (FE_has_voted($voter, $voters)) {
                    ob_start();
                    $tpl = new tpl('forum/vote/post_voted');
                    $tpl->set_out('question', $voteAr['question'], 0);
                    foreach ($voteAr['options'] as $key => $value){
                        $tpl->set('id', $key);
                        $tpl->set('option', $value);
                        $tpl->set('res_percent', $gesamt > 0 ? round($results[$key] / $gesamt * 100, 0) : 0);
                        $tpl->set('result', $results[$key]);
                        $tplvoters = '';
                        if ($GLOBALS['saveAndShowVotersOpinion']) {
                            $tplvoters = FE_getVotersOpinions($voters, $key-1);
                           }
                         $tpl->set('voters', $tplvoters);
                        $tpl->set('showvoters', strlen($tplvoters) > 0 ? 1 : 0);
                        $tpl->out(1);
                    }
                    $tpl->set_out('gesvotes', $gesamt, 2);
                    $vote_txt = ob_get_clean();

                } else { // User hat noch nicht abgestimmt -> Abstimmungsformular
                    ob_start();
                    $tpl = new tpl('forum/vote/post_tovote');
                    $tpl->set('post_id', $post_id);
                    $tpl->set_out('question', $voteAr['question'], 0);
                    foreach ($voteAr['options'] as $key => $value){
                        $tpl->set('id', $key);
                        $tpl->set('option', $value);
                        $tpl->out(1);
                    }
                    $tpl->set_out('showResult', $GLOBALS['showResultBeforeVote'] ? 1 : 0, 2);
                    $vote_txt = ob_get_clean();
                }
                return substr($txt, 0 , $voteAr['offset']) . $vote_txt . substr($txt, $voteAr['offset']);
            }
            break;
        case 1:
            //Vorschau bei Erstellung eines Posts/Themas
            ob_start();
            $tpl = new tpl('forum/vote/preview');
            $tpl->set_out('question', $voteAr['question'], 0);
            foreach ($voteAr['options'] as $key => $value){
                $tpl->set('id', $key);
                $tpl->set('option', $value);;
                $tpl->out(1);
            }
            $tpl->out(2);
            $vote_txt = ob_get_clean();

            return substr($txt, 0 , $voteAr['offset']) . $vote_txt . substr($txt, $voteAr['offset']);
            break;
        case 2:
            // Ergebnis anzeigen
            $erg = db_query('SELECT * FROM prefix_posts_poll WHERE post_id = ' . $post_id);
            if (db_num_rows($erg) == 1) {
                $row = db_fetch_assoc($erg);
                $results = explode('#', $row['results']);
                $gesamt = array_sum($results);


                ob_start();
                $tpl = new tpl('forum/vote/show_result');
                $tpl->set_out('question', $voteAr['question'], 0);
                foreach ($voteAr['options'] as $key => $value){
                    $tpl->set('id', $key);
                    $tpl->set('option', $value);
                    $tpl->set('res_percent', $gesamt > 0 ? round($results[$key] / $gesamt * 100, 0) : 0);
                    $tpl->set('result', $results[$key]);
                    $tpl->out(1);
                }
                $tpl->set_out('gesvotes', $gesamt, 2);
                $vote_txt = ob_get_clean();

                return substr($txt, 0 , $voteAr['offset']) . $vote_txt . substr($txt, $voteAr['offset']);
            }
            break;
    }
}
// �berpr�ft ob der �bergebene Text eine richtige Vote enth�lt und
// gibt die Anzahl der Antwortm�glichkeiten oder 0 zur�ck
function FE_ValidVote($txt)
{
    $vote_start = strpos($txt, '[vote]');
    $vote_end = strpos($txt, '[/vote]');

    if ($vote_start === false OR $vote_end === false OR $vote_start > $vote_end) {
        return(0);
    }
    $vote_txt = substr($txt, $vote_start + 6, $vote_end - $vote_start - 6);
    $q1 = strpos($vote_txt, '[question]');
    $q2 = strpos($vote_txt, '[/question]');
    if ($q1 === false OR $q2 === false OR $q1 > $q2) $valid = false;
    else $valid = true;
    $i = 1;
    while (strpos($vote_txt, '[option=' . $i . ']')) {
        $opt[$i] = strpos($vote_txt, '[option=' . $i . ']');
        if ($i > 1) for($j = 1;$j < $i;$j++) if ($opt[$i] < $opt[$j]) $valid = false; #�berpr�ft ob die Reihenfolge richtig ist
        if (strpos($vote_txt, '[option=' . $i . ']', $opt[$i] + 1)) $valid = false; #�berpr�ft ob eine Option doppelt da ist
        $i++;
    }
    if ($valid AND $i > 2) return($i - 1);
    return(0);
}
// Tr�gt Vote ein, wenn der Eintrag in der Datenbank existiert
function FE_Vote($post_id, $option)
{
    $erg = db_query('SELECT * FROM `prefix_posts_poll` WHERE post_id = ' . $post_id);
    if (db_num_rows($erg) == 1) {
        $row = db_fetch_assoc($erg);
        $voters = explode('#', $row['voters']);
        $results = explode('#', $row['results']);
        if ($_SESSION['authid'] == 0) $voter = getip();
        else $voter = $_SESSION['authid'];
        $voters[] = $GLOBALS['saveAndShowVotersOpinion'] ? $voter . '_' . $option : $voter;
        if (array_key_exists($option, $results)) $results[$option]++;
        else return(false);
        $voters = implode('#', $voters);
        $results = implode('#', $results);
        db_query('UPDATE `prefix_posts_poll` SET voters = "' . $voters . '", results = "' . $results . '" WHERE post_id = ' . $post_id);
    }
}
// Erstellt einen Tabelleneintrag f�r eine Umfrage oder editiert/l�scht eine Vorhandene, nur l�schen oder hinzuf�gen m�glich
function FE_CreateVote($post_id, $txt)
{
    $options = FE_ValidVote($txt);
    $erg = db_query('SELECT * FROM `prefix_posts_poll` WHERE post_id = ' . $post_id);
    if (db_num_rows($erg) == 1) {
        if ($options == 0) { // L�sche den Datenbankeintrag, wenn kein Vote mehr in dem Post enthalten ist
            db_query('DELETE FROM `prefix_posts_poll` WHERE post_id = ' . $post_id);
            return(true);
        } else {
            $row = db_fetch_assoc($erg);
            $results = explode('#', $row['results']);
            $resanz = count($results) - 1;
            if ($resanz == $options) return(true); //�ndere nichts, wenn sich am Vote nichts ge�ndert hat (Anzahl der Optionen)
            elseif ($resanz > $options) {
                // Bei Verringerung der Optionen Anzahl der Results l�schen
                $results = array_slice($results, 0, $options + 1);
            } else for ($i = $resanz; $i <= $options; $i++) {
                // F�ge noch leere Results ein
                $results[$i] = 0;
            }
            $results = implode('#', $results);
            db_query('UPDATE `prefix_posts_poll` SET results = "' . $results . '" WHERE post_id = ' . $post_id);
            return(true);
        }
    } else {
        // Erstellt Eintrag, wenn noch keiner vorhanden ist
        if ($options == 0) return(false);
        for ($i = 1; $i <= $options; $i++) {
            $results[$i] = 0;
        }
        $results = '#' . implode('#', $results);
        db_query('INSERT INTO `prefix_posts_poll` (post_id,voters,results) VALUES (' . $post_id . ', "", "' . $results . '")');
        return(true);
    }
}
// Pr�ft ob der User schon abgestimmt hat
function FE_has_voted($voter, $voters)
{
    if ($GLOBALS['saveAndShowVotersOpinion']) {
        foreach ($voters as $v) {
            list($vot, $erg) = explode('_', $v);
            if ($vot == $voter) {
                return true;
            }
        }
        return false;
    } else {
        return in_array($voter, $voters);
    }
}
//Stellt die Namen einer Otion zusammen
function FE_getVotersOpinions($voters, $opinion)
{
    $output = array();
    foreach ($voters as $tmp){
        list($v, $vo) = explode('_', $tmp);
        if ($opinion == $vo) {
            $output[] = get_n($v);
        }
    }
    return implode(', ', $output);
}
?>