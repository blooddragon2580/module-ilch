	var tagOpen = '[';
	var tagClos = ']';
	var tagEnde = '/';


  
function simple(name) {
  aTag = tagOpen + name + tagClos;
  eTag = tagOpen + tagEnde + name + tagClos;
  simple_insert ( aTag, eTag );
}

function simple_insert(aTag,eTag) {
  
  var input = document.forms['form'].elements['txt'];
  input.focus();
  /* f�r Internet Explorer */
  if(typeof document.selection != 'undefined') {
    /* Einf�gen des Formatierungscodes */
    var range = document.selection.createRange();
    var insText = range.text;
    range.text = aTag + insText + eTag;
    /* Anpassen der Cursorposition */
    range = document.selection.createRange();
    if (insText.length == 0) {
      range.move('character', -eTag.length);
    } else {
      range.moveStart('character', aTag.length + insText.length + eTag.length);      
    }
    range.select();
  }
  /* f�r neuere auf Gecko basierende Browser */
  else if(typeof input.selectionStart != 'undefined')
  {
    /* Einf�gen des Formatierungscodes */
    var start = input.selectionStart;
    var end = input.selectionEnd;
    var insText = input.value.substring(start, end);
    input.value = input.value.substr(0, start) + aTag + insText + eTag + input.value.substr(end);
    /* Anpassen der Cursorposition */
    var pos;
    if (insText.length == 0) {
      pos = start + aTag.length;
    } else {
      pos = start + aTag.length + insText.length + eTag.length;
    }
    input.selectionStart = pos;
    input.selectionEnd = pos;
  }
  /* f�r die �brigen Browser */
  else
  {
    /* Abfrage der Einf�geposition */
    var pos = input.value.length;
    
    /* Einf�gen des Formatierungscodes */
    var insText = prompt("Bitte geben Sie den zu formatierenden Text ein:");
    input.value = input.value.substr(0, pos) + aTag + insText + eTag + input.value.substr(pos);
  }
}

function simple_liste () {
  var x = '';
  var l = '';
  while ( x != null ) {
    x = prompt ("Um die Liste zu beenden 'Abbrechen' eingeben");
    if ( x != null ) {
      l = l + "[*]" + x + "\n";
    }
  }
  if ( l != '' ) {
    l = "[list]\n" + l + "[/list]"; 
    simple_insert ( l, '' );
  }
}

function  put ( towrite ) {
  simple_insert ( towrite, '' );
}

function check() {
	if ( form.txt.value == '' ) {
	  alert ( 'Bis jetzt wurde wohl noch nichts eingegeben, also schnell nachholen!' );
	  return false;
	} else {
	  if ( form.pageName.value == '' ) {
	    alert ( 'Bitte gib noch schnell einen Namen ein!' );
	    return false;
	  } else {
	    return true;
	  }
	}
  
}

//Funktion zum Einf�gen des Umfragecodes
function insertvote(){
  var x = '';
  var l = '';
  var i = 0;
 
  var q = prompt("Hier die Umfragebeschreibung eingeben:");
  while ( x != null ) {
    x = prompt ("Hier die Optionen der Umfrage einzeln eintragen, jede mit OK best�tigen!\nUm die Aufnahme von Optionen zu beenden 'Abbrechen' dr�cken");
    i++;
    if ( x != null ) {
      l = l + "[option=" + i + "]" + x + "\n";
    }
  }
if ( (( q != '' ) && ( l != '')) && (i >= 2) ) {
    l = "[vote]\n" + '[question]' + q + '[/question]\n' + l + "[/vote]";
    simple_insert ( l, '' );
  }
}
