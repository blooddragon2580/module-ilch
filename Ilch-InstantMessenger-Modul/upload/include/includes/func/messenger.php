<?php

/*

Copyright (c) 2009 Anant Garg (anantgarg.com | inscripts.com)
Modified 2009 Florian Koerner (ilch.de | Flomavali)

This script may be used for non-commercial purposes only. For any
commercial purposes, please contact the author at 
anant.garg@inscripts.com

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

defined ('main') or die ( 'no direct access' );

if ( loggedin() ) {
	if ($_GET['messenger'] == "chatheartbeat") { chatHeartbeat(); } 
	if ($_GET['messenger'] == "sendchat") { sendChat(); } 
	if ($_GET['messenger'] == "closechat") { closeChat(); } 
	if ($_GET['messenger'] == "startchatsession") { startChatSession(); } 

	if (!isset($_SESSION['chatHistory'])) {
		$_SESSION['chatHistory'] = array();	
	}

	if (!isset($_SESSION['openChatBoxes'])) {
		$_SESSION['openChatBoxes'] = array();	
	}
} else {
	if ($_GET['messenger'] == "chatheartbeat") {
		if ( isset ( $_SESSION['guestblock'] ) ) {
			echo '{"items":[{"s":"0","i":"'.$_SESSION['guestblock'].'","f":"System","m":"<i>Es können nur Mitglieder an dieser Sitzung teilnehmen.</i>"}]}';
			unset( $_SESSION['guestblock'] );
		} else {
			echo '{"items":[]}';
		}
	}
	if ($_GET['messenger'] == "sendchat") {
		$_SESSION['guestblock'] = escape($_POST['eid'],'integer');
		echo '1';
	} 
	if ($_GET['messenger'] == "startchatsession") {
		echo '{"username":"Gast","items":[]}';
	}
	
	if (isset($_SESSION['chatHistory'])) {
		unset($_SESSION['chatHistory']);	
	}

	if (isset($_SESSION['openChatBoxes'])) {
		unset($_SESSION['openChatBoxes']);	
	}
	exit;
}

function chatHeartbeat() {
	
	$sql = 'SELECT * FROM `prefix_messenger` WHERE (`eid` = '.$_SESSION['authid'].' AND `recd` = 0) ORDER BY `id` ASC';
	$query = db_query($sql);
	$items = '';

	$chatBoxes = array();

	while ($chat = db_fetch_assoc($query)) {
		if (!isset($_SESSION['openChatBoxes'][$chat['aid']]) && isset($_SESSION['chatHistory'][$chat['aid']])) {
			$items = $_SESSION['chatHistory'][$chat['aid']];
		}

		$chat['txt'] = sanitize($chat['txt']);

		$items .= '{"s":"0","i":"'.$chat['aid'].'","f":"'.utf8_encode(get_n($chat['aid'])).'","m":"'.$chat['txt'].'"},';

		if (!isset($_SESSION['chatHistory'][$chat['aid']])) {
			$_SESSION['chatHistory'][$chat['aid']] = '';
		}

		$_SESSION['chatHistory'][$chat['aid']] .= '{"s":"0","i":"'.$chat['aid'].'","f":"'.utf8_encode(get_n($chat['aid'])).'","m":"'.$chat['txt'].'"},';
			
		unset($_SESSION['tsChatBoxes'][$chat['aid']]);
		$_SESSION['openChatBoxes'][$chat['aid']] = $chat['sent'];
	}

	if (!empty($_SESSION['openChatBoxes'])) {
		foreach ($_SESSION['openChatBoxes'] as $chatbox => $time) {
			if (!isset($_SESSION['tsChatBoxes'][$chatbox])) {
				$now = time()-strtotime($time);
				$time = date('g:iA M dS', strtotime($time));

				$message = "Sent at $time";
				if ($now > 180) {
						$items .= '{"s":"2","i":"'.$chatbox.'","f":"'.utf8_encode(get_n($chatbox)).'","m":"'.$message.'"},';

					if (!isset($_SESSION['chatHistory'][$chatbox])) {
						$_SESSION['chatHistory'][$chatbox] = '';
					}

					$_SESSION['chatHistory'][$chatbox] .= '{"s":"2","i":"'.$chatbox.'","f":"'.utf8_encode(get_n($chatbox)).'","m":"'.$message.'"},';
					$_SESSION['tsChatBoxes'][$chatbox] = 1;
				}
			}
		}
	}

	$sql = 'UPDATE `prefix_messenger` SET `recd` = 1 WHERE `eid` = '.$_SESSION['authid'].' AND `recd` = 0';
	$query = db_query($sql);

	if ($items != '') {
		$items = substr($items, 0, -1);
	}
	
	header('Content-type: application/json');
	echo '{"items":['.$items.']}';
	exit(0);
}

function chatBoxSession($chatbox) {
	
	$items = '';
	
	if (isset($_SESSION['chatHistory'][$chatbox])) {
		$items = $_SESSION['chatHistory'][$chatbox];
	}

	return $items;
}

function startChatSession() {
	$items = '';
	if (!empty($_SESSION['openChatBoxes'])) {
		foreach ($_SESSION['openChatBoxes'] as $chatbox => $void) {
			$items .= chatBoxSession($chatbox);
		}
	}


	if ($items != '') {
		$items = substr($items, 0, -1);
	}

	header('Content-type: application/json');
	echo '{"username":"'.$_SESSION['authname'].'","items":['.$items.']}';
	exit(0);
}

function sendChat() {
	$aid = $_SESSION['authid'];
	$eid = escape($_POST['eid'],'integer');
	$txt = escape($_POST['txt'],'string');

	$_SESSION['openChatBoxes'][$eid] = date('Y-m-d H:i:s', time());
	
	$c_txt = sanitize($txt);

	if (!isset($_SESSION['chatHistory'][$_POST['eid']])) {
		$_SESSION['chatHistory'][$_POST['eid']] = '';
	}

	$_SESSION['chatHistory'][$_POST['eid']] .= '{"s":"1","i":"'.$eid.'","f":"'.utf8_encode(get_n($eid)).'","m":"'.$c_txt.'"},';

	unset($_SESSION['tsChatBoxes'][$_POST['eid']]);

	$sql = 'INSERT INTO `prefix_messenger` (`aid`,`eid`,`txt`,`sent`) VALUES ('.$aid.','.$eid.',"'.$txt.'",NOW())';
	db_query($sql);
	echo "1";
	exit(0);
}

function closeChat() {

	unset($_SESSION['openChatBoxes'][$_POST['chatbox']]);
	
	echo "1";
	exit(0);
}

function sanitize($text) {
	$text = htmlspecialchars($text, ENT_QUOTES);
	$text = str_replace("\n\r","\n",$text);
	$text = str_replace("\r\n","\n",$text);
	$text = str_replace("\n","<br>",$text);
	return $text;
}