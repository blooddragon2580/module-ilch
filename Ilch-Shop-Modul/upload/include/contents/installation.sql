CREATE TABLE IF NOT EXISTS `prefix_pps_bestellungen` (
  `OrderID` int(11) NOT NULL auto_increment,
  `ProductID` int(11) default NULL,
  `CustomerID` int(11) default NULL,
  `DLCode` varchar(16) default NULL,
  `Active` int(11) default NULL,
  `LastUse` varchar(20) NOT NULL,
  PRIMARY KEY  (`OrderID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;




CREATE TABLE IF NOT EXISTS `prefix_pps_cat` (
  `CatID` int(10) NOT NULL auto_increment,
  `CatPos` int(10) NOT NULL,
  `CatName` varchar(50) NOT NULL,
  PRIMARY KEY  (`CatID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

--
-- Daten f�r Tabelle `ic1_pps_cat`
--

INSERT INTO `ic1_pps_cat` (`CatID`, `CatPos`, `CatName`) VALUES
(1, 1, 'Kategorie 1'),
(2, 2, 'Kategorie 2');




CREATE TABLE IF NOT EXISTS `prefix_pps_config` (
  `id` int(5) NOT NULL,
  `yourname` varchar(50) NOT NULL,
  `ppemail` varchar(50) NOT NULL,
  `token` varchar(100) NOT NULL,
  `sitepath` varchar(50) NOT NULL,
  `anzahl` int(5) NOT NULL,
  `text1` varchar(50) NOT NULL,
  `text2` varchar(50) NOT NULL,
  `curr` varchar(10) NOT NULL,
  `voting` smallint(6) NOT NULL,
  `emptycat` smallint(6) NOT NULL,
  `breite` varchar(7) NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `bordercolor` varchar(7) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO `prefix_pps_config` (`id`, `yourname`, `ppemail`, `token`, `sitepath`, `anzahl`, `text1`, `text2`, `curr`, `voting`, `emptycat`, `breite`, `linecolor`, `bordercolor`) VALUES
(1, 'Ihr Name', 'Ihre@email.de', '', 'http://urlzumilchscript.de', 5, '[b]Inkl.[/b] 19% MwSt', '[b]Lieferung:[/b] Download', '�', 1, 1, '100%', '#000000', '#232323');


CREATE TABLE IF NOT EXISTS `prefix_pps_kunden` (
  `CustomerID` int(11) NOT NULL auto_increment,
  `FirstName` varchar(25) default NULL,
  `LastName` varchar(25) default NULL,
  `EMail` varchar(50) default NULL,
  PRIMARY KEY  (`CustomerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;



CREATE TABLE IF NOT EXISTS `prefix_pps_produkte` (
  `ProductID` int(11) NOT NULL auto_increment,
  `CatID` int(10) NOT NULL,
  `Recht` smallint(6) NOT NULL,
  `ProductName` varchar(50) default NULL,
  `ProductPrice` float default NULL,
  `FileName` varchar(50) default NULL,
  `Text` text NOT NULL,
  `Datum` varchar(50) NOT NULL,
  `ProductPic` varchar(50) NOT NULL,
  `rate` int(10) NOT NULL,
  `klick` int(10) NOT NULL,
  PRIMARY KEY  (`ProductID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

INSERT INTO `prefix_pps_produkte` (`ProductID`, `CatID`, `Recht`, `ProductName`, `ProductPrice`, `FileName`, `Text`, `Datum`, `ProductPic`, `rate`, `klick`) VALUES
('', '', '', 'Test Produkt', 5.5, 'testprodukt.rar', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.\r\n[PREVIEWENDE]\r\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '17.07.2011 17:00', 'nopic.gif', 5, '');

INSERT INTO `prefix_modules` (`id`, `url`, `name`, `gshow`, `ashow`, `fright`) VALUES
('', 'pps', 'PayPal Shop', 1, 1, 1);