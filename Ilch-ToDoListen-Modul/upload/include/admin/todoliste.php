<?php

### Copyright by [BU*M]BöhserOnkel
### Support www.baduncles.de

defined ('main') or die ( 'no direct access' );
defined ('admin') or die ( 'only admin access' );

$design = new design ( 'Admins Area', 'Admins Area', 2 );
$design->header();

## some vars
$did = escape($menu->get(2), 'integer');
$ty = escape($menu->get(1), 'string');
$stat_ar = array("0"=>"Neu","1"=>"Bearbeitung","2"=>"Erledigt",);

## sql
if(isset($_POST['submit']) AND !empty($_POST['submit'])){
 $text = escape($_POST['text'], 'string');
 $status = escape($_POST['status'], 'integer');
 $link = escape($_POST['link'], 'string');
 if($status > 0){
 	$date = date("Y-m-d H:i:s",time());
 }else{
 	$date = 0;
 }
 $doLis = escape($_POST['doLis'], 'string');
 if($doLis == "neu"){
 $doLis = escape($_POST['ndo'], 'string');
 }
 if(empty($_POST['did'])){
 	db_query("INSERT INTO prefix_todoliste (id,kate,status,text,date,link) VALUES ('','".$doLis."','".$status."','".$text."','','".$link."')");
 }else{
 	$did = escape($_POST['did'], 'integer');
 	$stcheck = @db_result(db_query("SELECT status FROM prefix_todoliste WHERE id = $did"),0);
 	if($stcheck == 1 AND $status == 1){ 
 	 db_query('UPDATE `prefix_todoliste` SET link = "'.$link.'",text = "'.$text.'", kate = "'.$doLis.'", status = "'.$status.'" WHERE id = "'.$did.'"');
  }else{
   db_query('UPDATE `prefix_todoliste` SET link = "'.$link.'",text = "'.$text.'", kate = "'.$doLis.'", status = "'.$status.'", date = "'.$date.'" WHERE id = "'.$did.'"');
  }
 }
}
if(is_numeric($did) AND $ty == "del"){
	db_query("DELETE FROM prefix_todoliste WHERE id = $did LIMIT 1");
  $delcheck = "Eintrag wurde gelöscht";
}
if($menu->getA(1) == 's'){
 $kate = @db_result(db_query("SELECT DISTINCT kate FROM prefix_todoliste ORDER by kate"),$menu->getE(1));
 $show = "WHERE kate = '$kate'";
 $p = "-".$menu->getA(1).$menu->getE(1);
}else{
 $show = "";
 $p = '';
}
## Kate as choose
$b = db_num_rows(db_query("SELECT DISTINCT kate FROM prefix_todoliste"));
for($i=0;$i<$b;$i++){
 $a = @db_result(db_query("SELECT DISTINCT kate FROM prefix_todoliste ORDER by kate ASC"),$i);
 $c[$i] = $a;
}
$kateanz = arlistee(($menu->getA(1) == 's' ? $menu->getE(1) : '-1'), $c);

## page display
$limit = 15;  // Limit 
$page = ($menu->getA(1) == 'p' ? $menu->getE(1) : ($menu->getA(2) == 'p' ? $menu->getE(2) : 1 ) );
$MPL = db_make_sites ($page , $show , $limit , "?todoliste".$p , 'todoliste' );
$anfang = ($page - 1) * $limit;

## display
$tpl = new tpl ( 'todoliste', 1);
if(is_numeric($did) AND $ty == "ed"){
 $sql = db_fetch_assoc(db_query("SELECT * FROM prefix_todoliste WHERE id = $did"));
 $sql['do'] = '';
 if($sql['status'] == 2){$sql['do'] = 'am';}elseif($sql['status'] == 1){$sql['do'] = 'seid';}
 $sql['status'] = arlistee($sql['status'],$stat_ar);
 $sql['doliste'] = dblistee($sql['kate'], "SELECT DISTINCT kate,kate FROM prefix_todoliste ORDER by kate ASC");
 $sql['enter'] = "&Auml;ndern";
 $sql['did'] = $did;
 $sql['date'] = ($sql['date'] > 0 ? $sql['date'].' Uhr' : '');
}else{
 $sql = array();
 $sql['status'] = arlistee($sql['status'],$stat_ar);
 $sql['doliste'] = dblistee($sql['kate'], "SELECT DISTINCT kate,kate FROM prefix_todoliste ORDER by kate ASC");
 $sql['enter'] = "Speichern";
 $sql['text'] = "";
 $sql['did'] = "";
 $sql['date'] = '';
 $sql['do'] = '';
 $sql['link'] = 'http://';
}
$tpl->set_ar_out($sql,0);

$tpl->set_ar_out(array('MPL'=> $MPL,'KATEANZ' => $kateanz),1);

$multi = db_query("SELECT * FROM prefix_todoliste $show ORDER BY status,id ASC LIMIT $anfang,$limit");
while ($row = db_fetch_assoc($multi) ) {
 $row['class'] = ($row['class'] == 'Cmite' ? 'Cnorm' : 'Cmite' );
 $row['status'] = "<img src=\"include/images/icons/".$stat_ar[$row['status']].".gif\" alt=".$stat_ar[$row['status']]." title=".$stat_ar[$row['status']].">";
 $row['text'] = ((strlen($row['text'])<50) ? $row['text'] : substr($row['text'],0,47).'...');
 $tpl->set_ar_out($row,2);
}
$tpl->out(3);
$design->footer();
?>