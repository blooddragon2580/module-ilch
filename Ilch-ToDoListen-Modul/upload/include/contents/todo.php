<?php

### Copyright by [BU*M]BöhserOnkel
### Support www.baduncles.de

defined ('main') or die ( 'no direct access' );

$title = $allgAr['title'].' :: ToDo - Liste';
$hmenu = 'ToDo - Liste';
$design = new design ( $title , $hmenu );
$design->header();
$stat_ar = array("0"=>"Neu","1"=>"Bearbeitung","2"=>"Erledigt",);
## Kates
$b = db_num_rows(db_query("SELECT DISTINCT kate FROM prefix_todoliste"));

for($i=0;$i<$b;$i++){
 $a = @db_result(db_query("SELECT DISTINCT kate FROM prefix_todoliste ORDER by kate ASC"),$i);
 echo '<table width="100%" border="0" cellpadding="5" cellspacing="1" class="border">
        <tr class="Chead">
         <td colspan="3" align="left"><b>'.$a.'</b></td>
        </tr>';
 $erg = db_query("SELECT * FROM prefix_todoliste WHERE kate = '$a' ORDER BY status ASC");
 while ($row = db_fetch_assoc($erg)) {
  if($row['link'] == 'http://' OR $row['link'] == ''){
   $link = "";
  }else{
   $link = 'Link';
  }
  echo '<tr class="Cmite">
 	       <td align="left" valign="top" nowrap>&nbsp;<img src="include/images/icons/'.$stat_ar[$row['status']].'.gif" alt="'.$stat_ar[$row['status']].'" title="'.$stat_ar[$row['status']].'">&nbsp;</td>
 	       <td align="left" width="100%">'.$row['text'].'</td>
 	       <td align="left" valign="top" nowrap>&nbsp;<a href="'.$row['link'].'" target="_blank">'.$link.'</a>&nbsp;</td>
        </tr>';
  }
 echo '</table><br>';
}
$design->footer();
?>