<?php

define ( 'main' , TRUE );

require_once ('include/includes/config.php');
require_once ('include/includes/func/db/mysql.php');

db_connect();
if ( empty ($_POST['step']) ) {

$admincon = 'include/admin/todoliste.php';
$admintem = 'include/admin/templates/todoliste.htm';
$adminpic = 'include/images/icons/admin/todoliste.png';
$con = 'include/contents/todo.php';
$pic1 = 'include/images/icons/Erledigt.gif';
$pic2 = 'include/images/icons/Bearbeitung.gif';
$pic3 = 'include/images/icons/Geplant.gif';

$adminco = (file_exists($admincon)?'Auf dem WebSpace':'Bitte Uploaden!');
$adminte = (file_exists($admintem)?'Auf dem WebSpace':'Bitte Uploaden!');
$adminpi = (file_exists($adminpic)?'Auf dem WebSpace':'Bitte Uploaden!');
$co = (file_exists($con)?'Auf dem WebSpace':'Bitte Uploaden!');
$pi1 = (file_exists($pic1)?'Auf dem WebSpace':'Bitte Uploaden!');
$pi2 = (file_exists($pic2)?'Auf dem WebSpace':'Bitte Uploaden!');
$pi3 = (file_exists($pic3)?'Auf dem WebSpace':'Bitte Uploaden!');


echo '<form action="install.php" method="POST">
 <input type="hidden" name="step" value="2" />
 <table width="100%" border="0" cellspacing="1" cellpadding="3" align="center">
  <tr>
   <td colspan="2"><b>Willkommen bei der Installation der ToDoListe vom BöhsenDavid</b>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td><input type="submit" value="Weiter ->">
   </td>
  </tr>
 </table>
</form>
</table>
<table width="40%" border="2" cellspacing="1" cellpadding="1">
 <tr>
 	<td width="20%">AdminContent</td><td width="20%" align="center">'.$adminco.'</td>
 </tr><tr>
 	<td width="20%">AdminTemplate</td><td width="20%" align="center">'.$adminte.'</td>
 </tr><tr>
 	<td width="20%">AdminPic</td><td width="20%" align="center">'.$adminpi.'</td>
 </tr><tr>
 	<td width="20%">Content</td><td width="20%" align="center">'.$co.'</td>
 </tr><tr>
 	<td width="20%">Erledigt.gif</td><td width="20%" align="center">'.$pi1.'</td>
 </tr><tr>
 	<td width="20%">Bearbeitung.gif</td><td width="20%" align="center">'.$pi2.'</td>
 </tr><tr>
 	<td width="20%">Geplant.gif</td><td width="20%" align="center">'.$pi3.'</td>
 </tr>
</table>';



} elseif ( $_POST['step'] == 2 ) {

db_query("CREATE TABLE IF NOT EXISTS `prefix_todoliste` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `kate` varchar(70) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `text` varchar(150) NOT NULL,
  `date` datetime default NULL,
  `link` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM COMMENT='powered by BöhserDavid' ;");
$check = @db_result(db_query("SELECT id FROM prefix_modules WHERE url = 'todoliste'"),0);
if($check == ''){
db_query("INSERT INTO `prefix_modules`(`url`,`name`,`gshow`,`ashow`,`fright`) VALUES ('todoliste','ToDo Listee', 1, 1, 1)");
}
?>
<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center">
 <tr>
  <td colspan="2"><b>Die ToDoListe wurde erfolgreich installiert</b>
  </td>
 </tr>
 <tr>
  <td colspan="2"><b>Du solltest nun die install.php l&ouml;schen!</b>
  </td>
 </tr>
 <tr>
  <td colspan="2"><br /><br /><b>Copyright [BU*M]BöhserOnkel alias BöhserDavid</b>
  </td>
 </tr>
</table>
<?php
}
?>