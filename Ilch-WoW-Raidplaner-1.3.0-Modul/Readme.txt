# WoW Raidplaner 1.3.0


Beschreibung:
-------------
Was macht das Modul?

Mit diesem Raidplaner-Modul kannst du auf deiner Gildenseite Raids verwalten und Termine f�r Gilden-Raids erstellen 
woran deine Gildenmitglieder teilnehmen k�nnen.


Entwickelt
----------
� von Cristian Gheorghiu - www.cristiang.de
� auf Basis von IlchClan 1.1 P



Installation:
-------------

� alle Dateien im Ordner UPLOAD, in ihrer Ordnerstruktur hochladen

� Die Installation aufrufen unter www.deinegilde.de/index.php?install


Fertig!



Einschr�nkungen:
----------------

- bisher keine bekannt



Fehler:
-------

- bisher keine bekannt



Haftungsausschluss:
-------------------
Ich �bernehme keine Haftung f�r Sch�den, die durch dieses Modul entstehen.
Benutzung ausschlie�lich AUF EIGENE GEFAHR.



Support: www.cristiang.de
eMail: info@cristiang.de


Viel Spa�! :D