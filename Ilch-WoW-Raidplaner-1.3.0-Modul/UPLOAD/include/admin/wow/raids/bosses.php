<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//
defined('main') or die('no direct access');
defined('admin') or die('only admin access');
// -----------------------------------------------------------|
// #
// ##
// ###
// #### A k t i o n e n
if ($menu->get(1) == 'edit') {
    if (!empty($_REQUEST['um'])) {
        $um = $_REQUEST['um'];
        $Pname = escape($_POST['name'], 'string');
        $Plevel = escape($_POST['level'], 'string');
        $Pnormal = escape($_POST['n'], 'string');
        $Pheroic = escape($_POST['hc'], 'string');
        $Pmythical = escape($_POST['myt'], 'string');

        $PbossID = escape($_POST['bossID'], 'integer');
        if ($um == 'insert2' && !empty($Pname)) {
            $pos = mysql_num_rows(db_query("SELECT * FROM `prefix_wow_raids_boss` WHERE raid = " . $menu->get(2)));
            $pos = $pos + 1;
            // insert
            db_query("INSERT INTO `prefix_wow_raids_boss` (pos,raid,name,lvl,normal,heroic,mythical)
		VALUES ('" . $pos . "','" . $menu->get(2) . "','" . $Pname . "','" . $Plevel . "','" . $Pnormal . "','" . $Pheroic . "','" . $Pmythical . "')");
            $bossID = mysql_insert_id();
            // insert
            // image upload
            if (!empty($_FILES['avatar']['name'])) {
                $uploadfile = "include/images/wowraids/bosses/" . $menu->get(2) . "/" . $_FILES['avatar']['name'];
                move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile);
                // save in DB
                db_query('UPDATE `prefix_wow_raids_boss` SET
				avatar  = "' . $_FILES['avatar']['name'] . '" WHERE id = "' . $bossID . '" LIMIT 1');
            }
            // image upload
        } elseif ($um == 'change2') {
            $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_boss` WHERE id = " . $PbossID . ""));
            // edit
            db_query('UPDATE `prefix_wow_raids_boss` SET
				name = "' . $Pname . '",
				lvl = "' . $Plevel . '",
				normal = "' . $Pnormal . '",
				heroic = "' . $Pheroic . '",
				mythical = "' . $Pmythical . '" WHERE id = "' . $PbossID . '" LIMIT 1');
            // edit
            // image edit
            if (!empty($_FILES['avatar']['name'])) {
                // delete
                if (!empty($row->avatar)) {
                    unlink("include/images/wowraids/bosses/" . $menu->get(2) . "/" . $row->avatar);
                }
                // upload
                $uploadfile = "include/images/wowraids/bosses/" . $menu->get(2) . "/" . $_FILES['avatar']['name'];
                move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile);
                // save in DB
                db_query('UPDATE `prefix_wow_raids_boss` SET avatar  = "' . $_FILES['avatar']['name'] . '" WHERE id = "' . $PbossID . '" LIMIT 1');
            }
            // image edit
        }
    }
// del Boss
    if ($menu->get(3) == 'del') {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_boss` WHERE id = " . $menu->get(4) . ""));
        // delete Avatar
        if (!empty($row->avatar)) {
            unlink("include/images/wowraids/bosses/" . $menu->get(2) . "/" . $row->avatar);
        }
        // delte DB entry
        db_query('DELETE FROM `prefix_wow_raids_boss` WHERE raid = "' . $menu->get(2) . '" AND id = "' . $menu->get(4) . '" LIMIT 1');
    }
// del Boss
// pos Raid
    if ($menu->get(3) == 'up') {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_boss` WHERE id = " . $menu->get(4)));
        if ($row->pos != 1) {
            $newpos = $row->pos - 1;
            $oldpos = $newpos + 1;
            db_query('UPDATE prefix_wow_raids_boss SET pos = "' . $oldpos . '" WHERE raid = ' . $menu->get(2) . ' AND pos = ' . $newpos);
            db_query('UPDATE prefix_wow_raids_boss SET pos = "' . $newpos . '" WHERE id = ' . $menu->get(4));
        }
    }
    if ($menu->get(3) == 'down') {
        $row = db_fetch_object(db_query("SELECT * FROM prefix_wow_raids_boss WHERE id = " . $menu->get(4)));
        $max = db_result(db_query("SELECT COUNT(id) FROM prefix_wow_raids_boss WHERE raid = " . $menu->get(2)));
        if ($row->pos != $max) {
            $newpos = ($row->pos != $max ? $newpos = $row->pos + 1 : $row->pos );
            $oldpos = $newpos - 1;
            if ($newpost <= $max) {
                db_query('UPDATE prefix_wow_raids_boss SET pos = "' . $oldpos . '" WHERE raid = ' . $menu->get(2) . ' AND pos = ' . $newpos);
                db_query('UPDATE prefix_wow_raids_boss SET pos = "' . $newpos . '" WHERE id = ' . $menu->get(4));
            }
        }
    }
//pos Raid
// #### A k t i o n e n
// ###
// ##
// #
// #
// ##
// ###
// #### h t m l   E i n g a b e n
    if ($menu->get(3) != 'edit') {
        $FbossID = '';
        $Faktion = 'insert2';
        $Favatar = '';
        $Fname = '';
        $Flevel = '';
        $Fnormal = '';
        $Fheroic = '';
        $Fmythical = '';
        $Fsub = 'Eintragen';
    } else {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_boss` WHERE id = " . $menu->get(4)));
        $FbossID = $row->id;
        $Faktion = 'change2';
        $Favatar = $row->avatar;
        $Fname = $row->name;
        $Flevel = $row->lvl;
        $Fnormal = ($row->normal == '1' ? ' checked="checked"' : '');
        $Fheroic = ($row->heroic == '1' ? ' checked="checked"' : '');
        $Fmythical = ($row->mythical == '1' ? ' checked="checked"' : '');
        $Fsub = '&Auml;ndern';
    }
    $tpl = new tpl('wow/raids/bosses', 1);

    $ar = array
        (
        'RAIDID' => $menu->get(2),
        'BOSSID' => $FbossID,
        'AKTION2' => $Faktion,
        'AVATAR' => $Favatar,
        'NAME' => $Fname,
        'LEVEL' => $Flevel,
        'N' => $Fnormal,
        'HC' => $Fheroic,
        'MYT' => $Fmythical,
        'FSUB' => $Fsub
    );

    $tpl->set_ar_out(array('RAIDID' => $menu->get(2)), 0);

    // B O S S E S  O U T
    $erg = db_query('SELECT * FROM `prefix_wow_raids_boss` WHERE raid = ' . $menu->get(2) . ' ORDER BY pos ASC');
    $class = '';
    while ($row = db_fetch_object($erg)) {
        // Type
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');

        $normal = ($row->normal == '1' ? 'ok' : '');
        $heroic = ($row->heroic == '1' ? 'ok' : '');
        $mythical = ($row->mythical == '1' ? 'ok' : '');

        $tpl->set_ar_out(array(
            'RAIDID' => $menu->get(2),
            'ID' => $row->id,
            'class' => $class,
            'AVATAR' => $row->avatar,
            'BOSS' => $row->name,
            'LvL' => $row->lvl,
            'N' => $normal,
            'HC' => $heroic,
            'MYT' => $mythical
                ), 1);
    }

    // F O R M U L A R  O U T
    $tpl->set_ar_out($ar, 2);
}
?>