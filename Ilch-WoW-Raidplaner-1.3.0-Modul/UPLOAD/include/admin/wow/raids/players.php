<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//
defined('main') or die('no direct access');
defined('admin') or die('only admin access');

// -----------------------------------------------------------|
// #
// ##
// ###
// #### F u n k t i o n e n
function getRoles($raid, $player) {
    $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_plan_players` WHERE raid = " . $raid . " AND player = " . $player));
    $str .= '<option value="admin.php?wowraidplaner-edit-' . $raid . '-role-' . $player . '-1"' . ($row->role == '1' ? ' selected="selected"' : '') . '>Tank</option>';
    $str .= '<option value="admin.php?wowraidplaner-edit-' . $raid . '-role-' . $player . '-2"' . ($row->role == '2' ? ' selected="selected"' : '') . '>Heiler</option>';
    $str .= '<option value="admin.php?wowraidplaner-edit-' . $raid . '-role-' . $player . '-3"' . ($row->role == '3' ? ' selected="selected"' : '') . '>Schaden</option>';
    return ($str);
}

// #### F u n k t i o n
// ###
// ##
// #
// #
// ##
// ###
// #### A k t i o n e n
if ($menu->get(1) == 'edit') {
// edit
    if ($menu->get(3) == 'role') {
        db_query('UPDATE `prefix_wow_raids_plan_players` SET
		role = "' . $menu->get(5) . '" WHERE player = "' . $menu->get(4) . '" AND raid = "' . $menu->get(2) . '" LIMIT 1');
    }
// edit
// join
    if ($menu->get(3) == 'join') {
        db_query('UPDATE `prefix_wow_raids_plan_players` SET
		status = "1" WHERE id = "' . $menu->get(4) . '" LIMIT 1');
    }
// join
// del Player
    if ($menu->get(3) == 'del') {
        db_query('DELETE FROM `prefix_wow_raids_plan_players` WHERE raid = "' . $menu->get(2) . '" AND id = "' . $menu->get(4) . '" LIMIT 1');
    }
// del Player
// #### A k t i o n e n
// ###
// ##
// #
// #
// ##
// ###
// #### h t m l   E i n g a b e n
    $tpl = new tpl('wow/raids/players', 1);

    $tpl->set_ar_out(array('PLANID' => $menu->get(2)), 0);

    // P L A Y E R S  O U T
    $roles = array('null', 'tank', 'heal', 'damage');
    $rolesN = array('null', 'Tank', 'Healer', 'Schaden');


    $tpl->out(1);
    $tpl->out(2);
    $erg2 = db_query('SELECT * FROM `prefix_wow_raids_plan_players` WHERE raid = ' . $menu->get(2) . ' AND status = 1 ORDER BY role ASC');
    $class = '';
    while ($row = db_fetch_object($erg2)) {
        $row2 = db_fetch_object(db_query("SELECT * FROM `prefix_user` WHERE id = " . $row->player));
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');

        $tpl->set_ar_out(array(
            'class' => $class,
            'PLAYERID' => $row->id,
            'ID' => $row->id,
            'RAID' => $row->raid,
            'ROLE' => $roles[$row->role],
            'ROLES' => getRoles($menu->get(2), $row2->id),
            'ROLEN' => $rolesN[$row->role],
            'CLASS' => $row->class,
            'PLAYER' => $row2->name,
            'CHARACTER' => $row->playerchar,
            'GEARSCORE' => $row->gearscore,
            'EXP' => $row->exp,
            'ARSENAL' => $row->arsenal,
                ), 4);
    }
    $tpl->out(6);


    $tpl->out(1);
    $tpl->out(3);
    $erg = db_query('SELECT * FROM `prefix_wow_raids_plan_players` WHERE raid = ' . $menu->get(2) . ' AND status = 0 ORDER BY role ASC');
    $class2 = '';
    while ($row = db_fetch_object($erg)) {
        $row2 = db_fetch_object(db_query("SELECT * FROM `prefix_user` WHERE id = " . $row->player));
        $class2 = ($class2 == 'Cmite' ? 'Cnorm' : 'Cmite');

        $tpl->set_ar_out(array(
            'class' => $class2,
            'PLAYERID' => $row->id,
            'ID' => $row->id,
            'RAID' => $menu->get(2),
            'ROLE' => $roles[$row->role],
            'ROLEN' => $rolesN[$row->role],
            'ROLES' => getRoles($menu->get(2), $row2->id),
            'CLASS' => $row->class,
            'PLAYER' => $row2->name,
            'CHARACTER' => $row->playerchar,
            'GEARSCORE' => $row->gearscore,
            'EXP' => $row->exp,
            'ARSENAL' => $row->arsenal,
                ), 5);
    }
    $tpl->out(6);
}
?>