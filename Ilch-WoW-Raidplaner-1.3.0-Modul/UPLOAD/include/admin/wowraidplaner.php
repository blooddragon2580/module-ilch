<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//
defined('main') or die('no direct access');
defined('admin') or die('only admin access');

$design = new design('Admins Area', 'Admins Area', 2);
$design->header();

// -----------------------------------------------------------|
// #
// ##
// ###
// #### F u n k t i o n e n
function getRaids($raid) {
    $erg = db_query('SELECT * FROM `prefix_wow_addons` ORDER BY pos ASC');
    while ($row = db_fetch_object($erg)) {
        // Type
        $raids .= '<optgroup label="' . $row->title . '">';
        $erg2 = db_query('SELECT * FROM `prefix_wow_raids_type` ORDER BY pos ASC');
        while ($row2 = db_fetch_object($erg2)) {
            // Raids
            $erg3 = db_query('SELECT * FROM `prefix_wow_raids` WHERE addon = "' . $row->id . '" AND type = "' . $row2->id . '" ORDER BY pos ASC');
            while ($row3 = db_fetch_object($erg3)) {
                $raids .= '<option value="' . $row3->id . '"' . ($row3->id == $raid ? ' selected="selected"' : '') . '>' . $row3->title . '</option>';
            }
        }
    }
    return ($raids);
}

function getPlayers($players) {
    $str .= '<option value="10"' . ($players == '10' ? ' selected="selected"' : '') . '>10</option>';
    $str .= '<option value="20"' . ($players == '20' ? ' selected="selected"' : '') . '>20</option>';
    $str .= '<option value="30"' . ($players == '30' ? ' selected="selected"' : '') . '>30</option>';
    return ($str);
}

// #### F u n k t i o n
// ###
// ##
// #
// #
// ##
// ###
// #### A k t i o n e n
if (!empty($_REQUEST['um'])) {
    $um = $_REQUEST['um'];
    $Praid = escape($_POST['raid'], 'integer');
    $Pheroic = escape($_POST['heroic'], 'integer');
    $Pplayers = escape($_POST['players'], 'integer');
    $Pgearscore = escape($_POST['gearscore'], 'integer');
    $Pdate = escape($_POST['date'], 'string');
    $Ptime = escape($_POST['time'], 'string');
    $Pleader = escape($_POST['leader'], 'string');

    $PplanID = escape($_POST['planID'], 'integer');
    if ($um == 'insert') {
        // insert
        $setDate = explode('.', $Pdate);
        $setTime = explode(':', $Ptime);
        $time = mktime($setTime['0'], $setTime['1'], 0, $setDate['1'], $setDate['0'], $setDate['2']);
        db_query("INSERT INTO `prefix_wow_raids_plan` (time,raid,heroic,players,gearscore,leader)
		VALUES ('" . $time . "','" . $Praid . "','" . $Pheroic . "','" . $Pplayers . "','" . $Pgearscore . "','" . $Pleader . "')");
        // insert
    } elseif ($um == 'change') {
        // edit
        $setDate = explode('.', $Pdate);
        $setTime = explode(':', $Ptime);
        $time = mktime($setTime['0'], $setTime['1'], 0, $setDate['1'], $setDate['0'], $setDate['2']);
        db_query('UPDATE `prefix_wow_raids_plan` SET
				time = "' . $time . '",
				raid = "' . $Praid . '",
				heroic = "' . $Pheroic . '",
				players = "' . $Pplayers . '",
				gearscore = "' . $Pgearscore . '",
				leader  = "' . $Pleader . '" WHERE id = "' . $PplanID . '" LIMIT 1');
    }
}
// edit
// del
if ($menu->get(1) == 'del') {
    // Raid
    db_query('DELETE FROM `prefix_wow_raids_plan` WHERE id = ' . $menu->get(2) . ' LIMIT 1');
    // Players
    db_query('DELETE FROM `prefix_wow_raids_plan_players` WHERE raid = ' . $menu->get(2));
}
// del
// #### A k t i o n e n
// ###
// ##
// #
// #
// ##
// ###
// #### h t m l   E i n g a b e n
if (empty($doNoIn)) {
    $limit = 20; // Limit
    $page = ($menu->getA(1) == 'p' ? $menu->getE(1) : 1);
    $MPL = db_make_sites($page, '', $limit, "?wowraidplaner", 'wow_raids_plan');
    $anfang = ($page - 1) * $limit;
    if ($menu->get(1) != 'edit') {
        $FplanID = '';
        $Faktion = 'insert';
        $Fraid = '';
        $Fheroic = '';
        $Fplayers = '10';
        $Fgearscore = '';
        $Fdate = date('d.m.Y', time());
        $Ftime = date('H:i', time());
        $Fleader = '';
        $Fsub = 'Eintragen';
    } else {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_plan` WHERE id = " . $menu->get(2)));
        $FplanID = $row->id;
        $Faktion = 'change';
        $Fraid = $row->raid;
        $Fheroic = ($row->heroic == '1' ? ' checked="checked"' : '');
        $Fplayers = $row->players;
        $Fgearscore = $row->gearscore;
        $Fdate = date('d.m.Y', $row->time);
        $Ftime = date('H:i', $row->time);
        $Fleader = $row->leader;
        $Fsub = '&Auml;ndern';
    }
    $tpl = new tpl('wowraidplaner', 1);

    $ar = array
        (
        'PLANID' => $FplanID,
        'AKTION' => $Faktion,
        'MPL' => $MPL,
        'RAIDS' => getRaids($Fraid),
        'HEROIC' => $Fheroic,
        'PLAYERS' => getPlayers($Fplayers),
        'GEARSCORE' => $Fgearscore,
        'DATE' => $Fdate,
        'TIME' => $Ftime,
        'LEADER' => $Fleader,
        'FSUB' => $Fsub
    );
    $tpl->set_ar_out($ar, 0);

    // P L A Y E R S
    require_once ('include/admin/wow/raids/players.php');
    // P L A Y E R S

    $tpl = new tpl('wowraidplaner', 1);
    $tpl->set_ar_out(array('MPL' => $MPL), 1);

    // e d i t , d e l e t e
    $erg = db_query('SELECT * FROM `prefix_wow_raids_plan` ORDER BY time DESC LIMIT ' . $anfang . ',' . $limit . '');
    $class = '';
    while ($row = db_fetch_object($erg)) {
        $row2 = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $row->raid));
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $heroic = ($row->heroic == '1' ? ' <img src="include/images/icons/skull.png" alt="heroisch" title="heroisch" style="margin-bottom: -3px" />' : '');
        $time = date('d.m.Y - H:i', $row->time);
        $all_players = @db_result(db_query("SELECT COUNT(id) FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 1"));
        $new_players = @db_result(db_query("SELECT COUNT(id) FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 0"));
        $players = $all_players . '/' . $row->players;
        $time = ($row->time < time() ? '<s>' . $time . '</s>' : $time);
        $tpl->set_ar_out(array('ID' => $row->id, 'class' => $class, 'TIME' => $time, 'HEROIC' => $heroic, 'TITLE' => $row2->title, 'PLAYERS' => $players, 'NEW' => $new_players), 2);
    }
    // e d i t , d e l e t e
    $tpl->set_ar_out(array('MPL' => $MPL), 3);
}

$design->footer();
?>