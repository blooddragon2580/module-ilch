<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//
defined('main') or die('no direct access');
defined('admin') or die('only admin access');

$design = new design('Admins Area', 'Admins Area', 2);
$design->header();

// -----------------------------------------------------------|
// #
// ##
// ###
// #### F u n k t i o n e n
function getAddons($addon) {
    $erg = db_query("SELECT * FROM `prefix_wow_addons` ORDER BY pos");
    while ($row = db_fetch_object($erg)) {
        $addons .= '<option value="' . $row->id . '" ' . ($addon == $row->id ? 'selected="selected"' : '') . '>' . $row->title . '</option>';
    }
    return ($addons);
}

function getTypes($type) {
    $erg = db_query("SELECT * FROM `prefix_wow_raids_type` ORDER BY pos");
    while ($row = db_fetch_object($erg)) {
        $types .= '<option value="' . $row->id . '" ' . ($type == $row->id ? 'selected="selected"' : '') . '>' . $row->name . '</option>';
    }
    return ($types);
}

// #### F u n k t i o n e n
// ###
// ##
// #
// #
// ##
// ###
// #### A k t i o n e n
if (!empty($_REQUEST['um'])) {
    $um = $_REQUEST['um'];
    $Paddon = escape($_POST['addon'], 'string');
    $Ptype = escape($_POST['type'], 'string');
    $Ptitle = escape($_POST['title'], 'string');
    $Ppreview = escape($_POST['preview'], 'string');
    $Pmap = escape($_POST['map'], 'string');
    $Pheroic = escape($_POST['heroic'], 'string');
    $Pgearscore = escape($_POST['gearscore'], 'string');
    $Plevel = escape($_POST['level'], 'string');
    $Pplayers = escape($_POST['players'], 'string');
    $Pplace = escape($_POST['place'], 'string');

    $PraidID = escape($_POST['raidID'], 'integer');
    if ($um == 'insert' && !empty($Ptitle)) {
        $pos = mysql_num_rows(db_query("SELECT * FROM `prefix_wow_raids` WHERE addon = " . $Paddon));
        $pos = $pos + 1;
        // insert
        db_query("INSERT INTO `prefix_wow_raids` (pos,addon,type,title,preview,map,heroic,gearscore,level,players,place)
		VALUES ('" . $pos . "','" . $Paddon . "','" . $Ptype . "','" . $Ptitle . "','" . $Ppreview . "','" . $Pmap . "','" . $Pheroic . "','" . $Pgearscore . "','" . $Plevel . "','" . $Pplayers . "','" . $Pplace . "')");
        $raidID = mysql_insert_id();
        // insert
        // Verzeichnis erstellen
        mkdir("include/images/wowraids/bosses/" . $raidID, 0700);
        mkdir("include/images/wowraids/maps/" . $raidID, 0700);
        mkdir("include/images/wowraids/raids/" . $raidID, 0700);
        // Verzeichnis erstellen
        // image upload
        if (!empty($_FILES['map']['name'])) {
            $uploadfile = "include/images/wowraids/maps/" . $raidID . "/" . $_FILES['map']['name'];
            move_uploaded_file($_FILES['map']['tmp_name'], $uploadfile);
            // save in DB
            db_query('UPDATE `prefix_wow_raids` SET
				map  = "' . $_FILES['map']['name'] . '" WHERE id = "' . $raidID . '" LIMIT 1');
        }
        if (!empty($_FILES['preview']['name'])) {
            $uploadfile = "include/images/wowraids/raids/" . $raidID . "/" . $_FILES['preview']['name'];
            move_uploaded_file($_FILES['preview']['tmp_name'], $uploadfile);
            // save in DB
            db_query('UPDATE `prefix_wow_raids` SET
				preview  = "' . $_FILES['preview']['name'] . '" WHERE id = "' . $raidID . '" LIMIT 1');
        }
        // image upload
    } elseif ($um == 'change') {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $PraidID . ""));
        // edit
        db_query('UPDATE `prefix_wow_raids` SET
				addon = "' . $Paddon . '",
				type = "' . $Ptype . '",
				title = "' . $Ptitle . '",
				heroic = "' . $Pheroic . '",
				gearscore = "' . $Pgearscore . '",
				level = "' . $Plevel . '",
				players = "' . $Pplayers . '",
				place  = "' . $Pplace . '" WHERE id = "' . $PraidID . '" LIMIT 1');
        // edit
        // image edit
        if (!empty($_FILES['map']['name'])) {
            // delete
            if (!empty($row->map)) {
                unlink("include/images/wowraids/maps/" . $PraidID . "/" . $row->map);
            }
            // upload
            $uploadfile = "include/images/wowraids/maps/" . $PraidID . "/" . $_FILES['map']['name'];
            move_uploaded_file($_FILES['map']['tmp_name'], $uploadfile);
            // save in DB
            db_query('UPDATE `prefix_wow_raids` SET
				map  = "' . $_FILES['map']['name'] . '" WHERE id = "' . $PraidID . '" LIMIT 1');
        }
        if (!empty($_FILES['preview']['name'])) {
            // delete
            if (!empty($row->preview)) {
                unlink("include/images/wowraids/raids/" . $raidID . "/" . $row->preview);
            }
            // upload
            $uploadfile = "include/images/wowraids/raids/" . $PraidID . "/" . $_FILES['preview']['name'];
            move_uploaded_file($_FILES['preview']['tmp_name'], $uploadfile);
            // save in DB
            db_query('UPDATE `prefix_wow_raids` SET
				preview  = "' . $_FILES['preview']['name'] . '" WHERE id = "' . $PraidID . '" LIMIT 1');
        }
        // image edit
    }
}
// del Raid
if ($menu->get(1) == 'del') {
    $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $menu->get(2) . ""));
    // delete images
    if (!empty($row->map)) {
        unlink("include/images/wowraids/maps/" . $menu->get(2) . "/" . $row->map);
    }
    if (!empty($row->preview)) {
        unlink("include/images/wowraids/raids/" . $menu->get(2) . "/" . $row->preview);
    }
    // delete bosses
    $erg = db_query('SELECT * FROM `prefix_wow_raids_boss` WHERE raid = ' . $menu->get(2));
    while ($row = db_fetch_object($erg)) {
        // delete image
        if (!empty($row->avatar)) {
            unlink("include/images/wowraids/bosses/" . $menu->get(2) . "/" . $row->avatar);
        }
        // delete entry
        db_query('DELETE FROM `prefix_wow_raids_boss` WHERE id = "' . $row->id . '" LIMIT 1');
    }
    // delete files
    rmdir("include/images/wowraids/bosses/" . $menu->get(2));
    rmdir("include/images/wowraids/maps/" . $menu->get(2));
    rmdir("include/images/wowraids/raids/" . $menu->get(2));
    // delte DB entry
    db_query('DELETE FROM `prefix_wow_raids` WHERE id = "' . $menu->get(2) . '" LIMIT 1');
    // delete raid plan
    $erg2 = db_query('SELECT * FROM `prefix_wow_raids_plan` WHERE raid = ' . $menu->get(2));
    while ($row = db_fetch_object($erg2)) {
        // delete players
        db_query('DELETE FROM `prefix_wow_raids_plan_players` WHERE raid = ' . $row->id);
        // delete plan
        db_query('DELETE FROM `prefix_wow_raids_plan` WHERE raid = "' . $menu->get(2) . '" LIMIT 1');
    }
}
// del Raid
// pos Raid
if ($menu->get(1) == 'up') {
    $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $menu->get(2)));
    if ($row->pos != 1) {
        $newpos = $row->pos - 1;
        $oldpos = $newpos + 1;
        db_query('UPDATE prefix_wow_raids SET pos = "' . $oldpos . '" WHERE addon = ' . $row->addon . ' AND pos = ' . $newpos);
        db_query('UPDATE prefix_wow_raids SET pos = "' . $newpos . '" WHERE id = ' . $menu->get(2));
    }
}
if ($menu->get(1) == 'down') {
    $row = db_fetch_object(db_query("SELECT * FROM prefix_wow_raids WHERE id = " . $menu->get(2)));
    $max = db_result(db_query("SELECT COUNT(id) FROM prefix_wow_raids WHERE addon = " . $row->addon));
    if ($row->pos != $max) {
        $newpos = ($row->pos != $max ? $newpos = $row->pos + 1 : $row->pos );
        $oldpos = $newpos - 1;
        if ($newpost <= $max) {
            db_query('UPDATE prefix_wow_raids SET pos = "' . $oldpos . '" WHERE addon = ' . $row->addon . ' AND pos = ' . $newpos);
            db_query('UPDATE prefix_wow_raids SET pos = "' . $newpos . '" WHERE id = ' . $menu->get(2));
        }
    }
}
//pos Raid
// #### A k t i o n e n
// ###
// ##
// #
// #
// ##
// ###
// #### h t m l   E i n g a b e n
if (empty($doNoIn)) {
    if ($menu->get(1) != 'edit') {
        $FraidID = '';
        $Faktion = 'insert';
        $Faddon = '';
        $Ftype = '';
        $Ftitle = '';
        $Fpreview = '';
        $Fmap = '';
        $Fheroic = '';
        $Fgearscore = '';
        $Flevel = '';
        $Fplayers = '';
        $Fplace = '';
        $Fsub = 'Eintragen';
    } else {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $menu->get(2)));
        $FraidID = $row->id;
        $Faktion = 'change';
        $Faddon = $row->addon;
        $Ftype = $row->type;
        $Ftitle = $row->title;
        $Fpreview = (!empty($row->preview) ? 'wowraids/raids/' . $row->id . '/' . $row->preview : 'icons/blank.png');
        $Fmap = (!empty($row->map) ? 'wowraids/maps/' . $row->id . '/' . $row->map : 'icons/blank.png');
        $Fheroic = $row->heroic;
        $Fgearscore = $row->gearscore;
        $Flevel = $row->level;
        $Fplayers = $row->players;
        $Fplace = $row->place;
        $Fhccheck = ($row->heroic == '1' ? ' checked="checked"' : '');
        $Fsub = '&Auml;ndern';
    }
    $tpl = new tpl('wowraids', 1);

    $ar = array
        (
        'RAIDID' => $FraidID,
        'AKTION' => $Faktion,
        'ADDONS' => getAddons($Faddon),
        'TYPES' => getTypes($Ftype),
        'TITLE' => $Ftitle,
        'PREVIEW' => $Fpreview,
        'MAP' => $Fmap,
        'HEROIC' => $Fheroic,
        'GEARSCORE' => $Fgearscore,
        'LEVEL' => $Flevel,
        'PLAYERS' => $Fplayers,
        'PLACE' => $Fplace,
        'HCCHECK' => $Fhccheck,
        'FSUB' => $Fsub
    );

    $tpl->set_ar_out($ar, 0);



    // R A I D - B O S S E S
    require_once ('include/admin/wow/raids/bosses.php');
    // R A I D - B O S S E S



    $tpl = new tpl('wowraids', 1);
    $tpl->out(2);
    // e d i t , d e l e t e , r a i d s
    $erg3 = db_query('SELECT * FROM `prefix_wow_addons` ORDER BY pos ASC');
    $class = '';
    while ($row = db_fetch_object($erg3)) {
        // Type
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $tpl->set_ar_out(array('TITEL' => $row->title), 3);
        $erg4 = db_query('SELECT * FROM `prefix_wow_raids_type` ORDER BY pos ASC');
        while ($row2 = db_fetch_object($erg4)) {
            // Raids
            $erg5 = db_query('SELECT * FROM `prefix_wow_raids` WHERE addon = "' . $row->id . '" AND type = "' . $row2->id . '" ORDER BY pos ASC');
            while ($row3 = db_fetch_object($erg5)) {
                $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
                $tpl->set_ar_out(array('ID' => $row3->id, 'class' => $class, 'TITEL' => $row3->title), 4);
            }
        }
    }
    // e d i t , d e l e t e , r a i d s
    $tpl->out(5);

    // n e x t   r a i d s
    $erg6 = db_query('SELECT * FROM `prefix_wow_raids_plan` WHERE time > ' . time() . ' ORDER BY time ASC');
    $class = '';
    while ($row = db_fetch_object($erg6)) {
        $row2 = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $row->raid));
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $heroic = ($row->heroic == '1' ? ' <img src="include/images/icons/skull.png" alt="heroisch" title="heroisch" style="margin-bottom: -3px" />' : '');
        $time = date('d.m.Y - H:i', $row->time);
        $all_players = @db_result(db_query("SELECT COUNT(id) FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 1"));
        $new_players = @db_result(db_query("SELECT COUNT(id) FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 0"));
        $players = $all_players . '/' . $row->players;
        $tpl->set_ar_out(array('ID' => $row->id, 'class' => $class, 'TIME' => $time, 'HEROIC' => $heroic, 'TITLE' => $row2->title, 'PLAYERS' => $players, 'NEW' => $new_players), 6);
    }
    // n e x t   r a i d s
    $tpl->out(7);
}

$design->footer();
?>