<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//

defined('main') or die('no direct access');

$erg = db_query('SELECT * FROM `prefix_wow_raids_plan` WHERE time > ' . time() . ' ORDER BY time ASC LIMIT 5');
echo '<table width="100%" border="0" cellpadding="2" cellspacing="0">';
while ($row = db_fetch_object($erg)) {
    $raid = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $row->raid));
    echo '<tr><td>&raquo; <a href="index.php?raidplaner-details-' . $row->id . '">' . $raid->title . '<br /><span class="smalfont">' . date('d.m.Y - H:i', $row->time) . '</span></a></td></tr>';
}
echo '</table>';
?>