<?php
// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//


defined ('main') or die ( 'no direct access' );


if ( loggedin() && user_has_admin_right($menu,false) ) {

	// Tabellen anlegen
	if( isset($_POST['submit']) ) {
	
		
		// Admin > Kofiguration
		
		db_query("INSERT INTO `prefix_config` (`schl`, `typ`, `kat`, `frage`, `wert`, `pos`) VALUES ('wow_raidplaner_autojoin', 'r2', 'WoW Optionen', 'Spieler automatisch zum Raid hinzuf�gen?', '0', 6);");
		
		
		// Admin > Module
		
		db_query("INSERT INTO `prefix_modules` (`url`, `name`, `gshow`, `ashow`, `fright`, `adshow`) VALUES
		('wowraidplaner', 'Raidplaner', 1, 0, 1, 0),
		('wowraids', 'Raids', 1, 1, 1, 0);");
		
		
		// WoW Addons
		
		db_query("CREATE TABLE IF NOT EXISTS `prefix_wow_addons` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
		`pos` int(1) NOT NULL,
  		`title` varchar(250) COLLATE latin1_general_ci NOT NULL,
  		`short` varchar(20) COLLATE latin1_general_ci NOT NULL,
  		PRIMARY KEY (`id`));");
		
		db_query("INSERT INTO `prefix_wow_addons` (`id`, `pos`, `title`, `short`) VALUES
		(1, 1, 'Classic', 'Classic'),
		(2, 4, 'Cataclysm', 'Cataclysm'),
		(3, 3, 'Wrath of the Lich King', 'WotLK'),
		(4, 2, 'The Burning Crusade', 'BC'),
		(5, 5, 'Mysts of Pandaria', 'MoP'),
		(6, 6, 'Warlords of Draenor', 'WoD');");
		
				
		// WoW Raid Types
		
		db_query("CREATE TABLE IF NOT EXISTS `prefix_wow_raids_type` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  		`pos` int(1) NOT NULL,
  		`name` varchar(250) COLLATE latin1_general_ci NOT NULL,
  		PRIMARY KEY (`id`) );");
		
		db_query("INSERT INTO `prefix_wow_raids_type` (`id`, `pos`, `name`) VALUES
		(1, 1, 'Schlachtz�ge'),
		(2, 2, 'Dungeons');");
		
		
		// WoW Raids
		
		db_query("CREATE TABLE IF NOT EXISTS `prefix_wow_raids` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  		`pos` int(2) NOT NULL,
  		`addon` varchar(2) COLLATE latin1_general_ci NOT NULL,
  		`type` varchar(1) COLLATE latin1_general_ci NOT NULL,
  		`title` varchar(150) COLLATE latin1_general_ci NOT NULL,
  		`preview` varchar(250) COLLATE latin1_general_ci NOT NULL,
  		`map` varchar(250) COLLATE latin1_general_ci NOT NULL,
  		`heroic` varchar(1) COLLATE latin1_general_ci NOT NULL,
  		`gearscore` varchar(3) COLLATE latin1_general_ci NOT NULL,
  		`level` varchar(7) COLLATE latin1_general_ci NOT NULL,
  		`players` varchar(5) COLLATE latin1_general_ci NOT NULL,
  		`place` varchar(150) COLLATE latin1_general_ci NOT NULL,
  		PRIMARY KEY (`id`) );");
		
		db_query("INSERT INTO `prefix_wow_raids` (`id`, `pos`, `addon`, `type`, `title`, `preview`, `map`, `heroic`, `gearscore`, `level`,	 `players`, `place`) VALUES
		(1, 1, '5', '2', 'Belagerung des Niuzaotempels', 'siege-of-niuzao-temple.jpg', 'siege-of-niuzao-temple1-large.jpg', '1', '393', '88', '5', 'Tonlongsteppe'),
		(2, 1, '5', '2', 'Brauerei Sturmbr�u', 'stormstout-brewery.jpg', 'stormstout-brewery1-large.jpg', '1', '325', '85', '5', 'Tal der vier Winde'),
		(3, 2, '5', '2', 'Das Shado-Pan-Kloster', 'shadopan-monastery.jpg', 'shadopan-monastery1-large.jpg', '1', '366', '87', '5', 'Kun-Lai-Gipfel'),
		(4, 4, '5', '2', 'Das Tor der Untergehenden Sonne', 'gate-of-the-setting-sun.jpg', 'gate-of-the-setting-sun1-large.jpg', '1', '393', '88', '5', 'Tal der ewigen Bl�ten'),
		(5, 5, '5', '2', 'Die Scharlachroten Hallen', 'scarlet-halls.jpg', 'scarlet-halls1-large.jpg', '1', '', '90', '5', 'Tirisfal'),
		(6, 6, '5', '2', 'Mogu''shanpalast', 'mogushan-palace.jpg', 'mogushan-palace1-large.jpg', '1', '366', '87', '5', 'Tal der ewigen Bl�ten'),
		(7, 7, '5', '2', 'Scharlachrotes Kloster', 'scarlet-monastery.jpg', 'scarlet-monastery1-large.jpg', '1', '', '90', '5', 'Tirisfal'),
		(8, 8, '5', '2', 'Scholomance', 'scholomance.jpg', 'scholomance1-large.jpg', '1', '', '90', '5', 'Westliche Pestl�nder'),
		(9, 9, '5', '2', 'Tempel der Jadeschlange', 'temple-of-the-jade-serpent.jpg', 'temple-of-the-jade-serpent1-large.jpg', '1', '325', '85', '5', 'Der Jadewald'),
		(10, 10, '5', '1', 'Das Herz der Angst', 'heart-of-fear.jpg', 'heart-of-fear1-large.jpg', '1', '', '90', '10/25', 'Schreckens�de'),
		(11, 11, '5', '1', 'Mogu''shangew�lbe', 'mogushan-vaults.jpg', 'mogushan-vaults1-large.jpg', '1', '', '90', '10/25', 'Kun-Lai-Gipfel'),
		(12, 12, '5', '1', 'Schlacht um Orgrimmar', 'siege-of-orgrimmar.jpg', 'siege-of-orgrimmar1-large.jpg', '1', '531', '90', '10/25', 'Orgrimmar'),
		(13, 13, '5', '1', 'Terrasse des Endlosen Fr�hlings', 'terrace-of-endless-spring.jpg', 'terrace-of-endless-spring1-large.jpg', '1', '', '90', '10/25', 'Die verh�llte Treppe'),
		(14, 14, '5', '1', 'Thron des Donners', 'throne-of-thunder.jpg', 'throne-of-thunder1-large.jpg', '1', '', '90', '10/25', 'Insel des Donners');");
		
		
		// WoW Raids > Boss
		
		db_query("CREATE TABLE IF NOT EXISTS `prefix_wow_raids_boss` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  		`pos` int(1) NOT NULL,
  		`raid` int(5) NOT NULL,
  		`name` varchar(150) COLLATE latin1_general_ci NOT NULL,
  		`avatar` varchar(150) COLLATE latin1_general_ci NOT NULL,
  		`lvl` int(3) NOT NULL,
  		`normal` int(1) NOT NULL,
  		`heroic` int(1) NOT NULL,
  		`mythical` int(1) NOT NULL,
  		PRIMARY KEY (`id`) );");
		
		db_query("INSERT INTO `prefix_wow_raids_boss` (`id`, `pos`, `raid`, `name`, `avatar`, `lvl`, `normal`, `heroic`, `mythical`) VALUES
		(1, 1, 1, 'Wesir Jin''bak', 'jinbak.jpg', 91, 0, 0, 0),
		(2, 2, 1, 'General Pa''valak', 'pavalak.jpg', 91, 0, 0, 0),
		(3, 3, 1, 'Kommandant Vo''jak', 'vojak.jpg', 91, 0, 0, 0),
		(4, 4, 1, 'Schwadronsf�hrer Ner''onok', 'neronok.jpg', 91, 0, 0, 0),
		(5, 1, 2, 'Uuk-Uuk', 'uukuuk.jpg', 89, 0, 0, 0),
		(6, 2, 2, 'Hopsallus', 'hopsallus.jpg', 87, 0, 0, 0),
		(7, 3, 2, 'Yan-Zhu der Dekantierte', 'yanzhu.jpg', 87, 0, 0, 0),
		(8, 1, 3, 'Gu Wolkenschlange', 'gu.jpg', 89, 0, 0, 0),
		(9, 2, 3, 'Meister Schneewehe', 'schneewehe.jpg', 89, 0, 0, 0),
		(10, 3, 3, 'Sha der Gewalt', 'shadergewalt.jpg', 89, 0, 0, 0),
		(11, 4, 3, 'Taran Zhu', 'taranzhu.jpg', 89, 0, 0, 0),
		(12, 1, 4, 'Saboteur Kip''tilak', 'kiptilak.jpg', 91, 0, 0, 0),
		(13, 2, 4, 'Bomber Ga''dok', 'gadok.jpg', 91, 0, 0, 0),
		(14, 3, 4, 'Kommandant Ri''mok', 'rimok.jpg', 91, 0, 0, 0),
		(15, 4, 4, 'Raigonn', 'raigonn.jpg', 92, 0, 0, 0),
		(16, 1, 5, 'Hundemeister Braun', 'braun.jpg', 92, 0, 0, 0),
		(17, 2, 5, 'Waffenmeister Harlan', 'harlan.jpg', 92, 0, 0, 0),
		(18, 3, 5, 'Flammenwirker Koegler', 'koegler.jpg', 92, 0, 0, 0),
		(19, 1, 6, 'Kuai der Grobian', 'kuai.jpg', 89, 0, 0, 0),
		(20, 2, 6, 'Gekkan', 'gekkan.jpg', 89, 0, 0, 0),
		(21, 3, 6, 'Xin der Waffenmeister', 'xin.jpg', 89, 0, 0, 0),
		(22, 1, 7, 'Thalnos der Seelenfetzer', 'thalnos.jpg', 92, 0, 0, 0),
		(23, 2, 7, 'Bruder Korloff', 'korloff.jpg', 92, 0, 0, 0),
		(24, 3, 7, 'Hochinquisitorin Wei�str�hne', 'weissstraehne.jpg', 92, 0, 0, 0),
		(25, 1, 8, 'Ausbildering Kaltherz', 'kaltherz.jpg', 92, 0, 0, 0),
		(26, 2, 8, 'Jandice Barov', 'jandicebarov.jpg', 92, 0, 0, 0),
		(27, 3, 8, 'Blutrippe', 'blutrippe.jpg', 92, 0, 0, 0),
		(28, 4, 8, 'Lilian Voss', 'lilianvoss.jpg', 92, 0, 0, 0),
		(29, 5, 8, 'Dunkelmeister', 'gandling.jpg', 92, 0, 0, 0),
		(30, 1, 9, 'Der weise Mari', 'mari.jpg', 87, 0, 0, 0),
		(31, 2, 9, 'Lehrensucher Steinschritt', 'steinschritt.jpg', 90, 0, 0, 0),
		(32, 3, 9, 'Liu Flammenherz', 'liu.jpg', 87, 0, 0, 0),
		(33, 4, 9, 'Sha des Zweifels', 'shadeszweifels.jpg', 87, 0, 0, 0),
		(34, 1, 10, 'Kaiserlicher Wesir Zor''lok', 'creature62980.jpg', 93, 0, 0, 0),
		(35, 2, 10, 'Klingenf�rst Ta''yak ', 'creature62543.jpg', 93, 0, 0, 0),
		(36, 3, 10, 'Garalon', 'creature62164.jpg', 93, 0, 0, 0),
		(37, 4, 10, 'Windf�rst Mel''jarak ', 'creature62397.jpg', 93, 0, 0, 0),
		(38, 5, 10, 'Bernformer Un''sok ', 'creature62511.jpg', 93, 0, 0, 0),
		(39, 6, 10, 'Gro�kaiserin Shek''zeer ', 'creature62837.jpg', 93, 0, 0, 0),
		(40, 1, 11, 'Die Steinwache', 'creature60047.jpg', 93, 0, 0, 0),
		(41, 2, 11, 'Feng der Verfluchte', 'creature60009.jpg', 93, 0, 0, 0),
		(42, 3, 11, 'Gara''jal der Geisterbinder ', 'creature60143.jpg', 93, 0, 0, 0),
		(43, 4, 11, 'Die Geisterk�nige', 'creature60701.jpg', 93, 0, 0, 0),
		(44, 5, 11, 'Elegon', 'creature60410.jpg', 93, 0, 0, 0),
		(45, 6, 11, 'Der Wille des Kaisers', 'creature60399.jpg', 92, 0, 0, 0),
		(46, 1, 12, 'Immerseus', 'immerseus.jpg', 93, 0, 0, 0),
		(47, 2, 12, 'Die gefallenen Besch�tzer', 'beschuetzer.jpg', 93, 0, 0, 0),
		(48, 3, 12, 'Norushen', 'norushen.jpg', 93, 0, 0, 0),
		(49, 4, 12, 'Sha des Stolzes', 'shadesstolzes.jpg', 93, 0, 0, 0),
		(50, 5, 12, 'Galakras', 'galakras.jpg', 93, 0, 0, 0),
		(51, 6, 12, 'Eiserner Koloss', 'koloss.jpg', 93, 0, 0, 0),
		(52, 7, 12, 'Dunkelschamanen der Kor''kron ', 'korkron.jpg', 93, 0, 0, 0),
		(53, 8, 12, 'General Nazgrim', 'nazgrim.jpg', 93, 0, 0, 0),
		(54, 9, 12, 'Malkorok', 'malkorok.jpg', 93, 0, 0, 0),
		(55, 10, 12, 'Die Sch�tze Pandarias', 'schaetze.jpg', 93, 0, 0, 0),
		(56, 11, 12, 'Thok der Blutr�nstige', 'thok.jpg', 93, 0, 0, 0),
		(57, 12, 12, 'Belagerungsingenieur Ru�schmied', 'russschmied.jpg', 93, 0, 0, 0),
		(58, 13, 12, 'Die Getreuen der Klaxxi', 'klaxxi.jpg', 93, 0, 0, 0),
		(59, 14, 12, 'Garrosh H�llschrei', 'garrosh.jpg', 93, 0, 0, 0),
		(60, 1, 13, 'Besch�tzer des Endlosen', 'beschuetzer.jpg', 93, 0, 0, 0),
		(61, 2, 13, 'Tsulong', 'tsulong.jpg', 93, 0, 0, 0),
		(62, 3, 13, 'Lei Shi', 'leishi.jpg', 93, 0, 0, 0),
		(63, 4, 13, 'Sha der Angst', 'shaderangst.jpg', 93, 0, 0, 0),
		(64, 1, 14, 'Jin''rokh der Zerst�rer ', 'jinrokh.jpg', 93, 0, 0, 0),
		(65, 2, 14, 'Horridon', 'horridon.jpg', 93, 0, 0, 0),
		(66, 3, 14, 'Rat der �ltesten', 'rat.jpg', 93, 0, 0, 0),
		(67, 4, 14, 'Tortos', 'tortos.jpg', 93, 0, 0, 0),
		(68, 5, 14, 'Megaera', 'megaera.jpg', 93, 0, 0, 0),
		(69, 6, 14, 'Ji-kun', 'jikun.jpg', 93, 0, 0, 0),
		(70, 7, 14, 'Durumu der Vergessene', 'durumu.jpg', 93, 0, 0, 0),
		(71, 8, 14, 'Primordius', 'primordius.jpg', 93, 0, 0, 0),
		(72, 9, 14, 'Dunkler Animus', 'animus.jpg', 93, 0, 0, 0),
		(73, 10, 14, 'Der eiserne Qon', 'qon.jpg', 93, 0, 0, 0),
		(74, 11, 14, 'Zwillingskonkubinen', 'zwillingskonkubinen.jpg', 93, 0, 0, 0),
		(75, 12, 14, 'Lei Shen', 'lei shen.jpg', 93, 0, 0, 0),
		(76, 13, 14, 'Ra-den', 'raden.jpg', 93, 0, 0, 0);");
				
		
		// WoW Raids > Planer
		
		db_query("CREATE TABLE IF NOT EXISTS `prefix_wow_raids_plan` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  		`time` int(200) NOT NULL,
  		`raid` int(5) NOT NULL,
  		`heroic` int(1) NOT NULL,
  		`players` int(5) NOT NULL,
  		`gearscore` int(3) NOT NULL,
  		`leader` varchar(150) COLLATE latin1_general_ci NOT NULL,
  		PRIMARY KEY (`id`) );");
				
		
		// WoW Raids > Planer > Players
		
		db_query("CREATE TABLE IF NOT EXISTS `prefix_wow_raids_plan_players` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  		`raid` int(5) NOT NULL,
  		`status` int(1) NOT NULL,
  		`player` int(5) NOT NULL,
  		`playerchar` varchar(50) NOT NULL,
  		`role` int(1) NOT NULL,
  		`class` varchar(20) COLLATE latin1_general_ci NOT NULL,
  		`exp` varchar(10) COLLATE latin1_general_ci NOT NULL,
  		`gearscore` int(3) NOT NULL,
  		`arsenal` varchar(250) COLLATE latin1_general_ci NOT NULL,
  		PRIMARY KEY (`id`) );");
		
		
		
		
	
	
		echo '<p>Raidplaner wurde installiert.</p>';
		echo '<p>Die Installationsdatei wurde automatisch gel&ouml;scht.</p>';
		echo '<p><a href="admin.php">jetzt konfigurieren</a></p>';
		
		
		// delete install file
		unlink('include/contents/install.php');


	} else { 
	
		echo '<p><h2>Raidplaner 1.3.0 Installation</h2>Wenn du den Raidplaner installiert hast, wird die install.php aus dem Verzeichnis include/contents/ automatisch gel&ouml;scht.</p>
<form action="" method="post"><input type="submit" name="submit" value="Jetzt installieren"  /></form>';

	}


} else { echo 'Du ben&ouml;tigst Adminrechte um die Installation durchzuf&uuml;hren.'; }

?>
