<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//
defined('main') or die('no direct access');

#
##
###  H T M L  -  A U S G A B E  ###

if ($menu->get(1) == 'details') {


    // Form
    if (!empty($_REQUEST['um'])) {
        $um = $_REQUEST['um'];
        $Pcharacter = escape($_POST['character'], 'string');
        $Prole = escape($_POST['role'], 'integer');
        $Pclass = escape($_POST['class'], 'string');
        $Pgearscore = escape($_POST['gearscore'], 'integer');
        $Pexperience = escape($_POST['experience'], 'string');
        $Parsenal = escape($_POST['arsenal'], 'string');

        $PplanID = escape($_POST['planID'], 'integer');

        $plan = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_plan` WHERE id = " . $menu->get(2)));

        $RaidJoined = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $menu->get(2) . " AND player = " . $_SESSION["authid"]));
        $JoinStatus = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND player = " . $_SESSION["authid"] . " AND status = 1"));

        $PlayerStatus = ($allgAr['wow_raidplaner_autojoin'] == '1' ? '1' : '0');

        if ($um == 'join' && $Prole > 0 && $RaidJoined == 0 && loggedin() && $plan->time > time()) {
            // insert
            db_query("INSERT INTO `prefix_wow_raids_plan_players` (raid,status,player,playerchar,role,class,exp,gearscore,arsenal) VALUES (
		'" . $menu->get(2) . "','" . $PlayerStatus . "','" . $_SESSION['authid'] . "','" . $Pcharacter . "','" . $Prole . "','" . $Pclass . "','" . $Pexperience . "','" . $Pgearscore . "','" . $Parsenal . "')");
            // insert
        } elseif ($um == 'left' && $RaidJoined == 1 && $JoinStatus == 1 && loggedin() && $plan->time > time()) {
            // delete
            db_query('DELETE FROM `prefix_wow_raids_plan_players` WHERE raid = ' . $PplanID . ' AND player = ' . $_SESSION['authid'] . ' LIMIT 1');
            // delete
        }
    }



    #
    ###  R A I D   -  D E T A I L S  ###
    #

	$plan = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_plan` WHERE id = " . $menu->get(2)));
    $raid = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $plan->raid));
    $AllPlayers = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND status = 1"));
    $RaidJoined = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND player = " . $_SESSION["authid"]));
    $JoinStatus = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND player = " . $_SESSION["authid"] . " AND status = 1"));
    $bosses = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_boss WHERE raid = " . $plan->raid));


    $title = $allgAr['title'] . ' :: Raidplaner - ' . $raid->title;
    $hmenu = '<a href="index.php?raidplaner">Raidplaner</a> &raquo; ' . $raid->title;
    $design = new design($title, $hmenu);
    $design->header();

    $tpl = new tpl('raidplaner');


    $heroic = ($plan->heroic == '1' ? '&nbsp;<img src="include/images/icons/skull.png" alt="Heroisch" title="Heroisch" style="margin-bottom: -3px" />' : '');
    $preview = (!empty($raid->preview) ? '<a href="include/images/wowraids/raids/' . $raid->id . '/' . $raid->preview . '" target="_blank"><img src="include/images/wowraids/raids/' . $raid->id . '/' . $raid->preview . '" alt="Vorschau" width="100%" border="0" /></a>' : '<img src="include/images/icons/blank.png" alt="Keine Vorschau" />');


    $tpl->out(3);

    // Raid-Details
    $tpl->set_ar_out(array(
        'TITLE' => $raid->title,
        'heroic' => $heroic,
        'PREVIEW' => $preview,
        'DATE' => date('d.m.Y', $plan->time),
        'TIME' => date('H:i', $plan->time),
        'PLAYERS' => $plan->players,
        'GEARSCORE' => $plan->gearscore,
        'LEADER' => $plan->leader,
        'BOSSES' => $bosses
            ), 4);

    $tpl->out(5);

    // Player-Statistic
    $tanks = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND status = 1 AND role = 1"));
    $heal = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND status = 1 AND role = 2"));
    $damage = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $plan->id . " AND status = 1 AND role = 3"));
    $tpl->set_ar_out(array(
        'STANKS' => $tanks,
        'SHEALER' => $heal,
        'SDAMAGE' => $damage,
        'ALLPLAYERS' => $AllPlayers
            ), 6);

    // Players
    $roles = array('null', 'tank', 'heal', 'damage');
    $rolesN = array('null', 'Tank', 'Heiler', 'Schaden');

    $erg = db_query('SELECT * FROM `prefix_wow_raids_plan_players` WHERE raid = "' . $plan->id . '" AND status = 1 ORDER BY role ASC');
    $class = '';
    while ($row = db_fetch_object($erg)) {
        $class = ($class == 'Cnorm' ? 'Cmite' : 'Cnorm');
        $user = db_fetch_object(db_query("SELECT * FROM `prefix_user` WHERE id = " . $row->player));
        $tpl->set_ar_out(array(
            'class' => $class,
            'ID' => $row->player,
            'ROLE' => $roles[$row->role],
            'ROLEN' => $rolesN[$row->role],
            'CHARACTER' => $row->playerchar,
            'CLASS' => $row->class,
            'GEARSCORE' => $row->gearscore
                ), 7);
    }

    $tpl->out(8);


    // Join Form
    if ($AllPlayers < $plan->players && $RaidJoined == 0 && loggedin() && $plan->time > time()) {
        $experience = $bosses . '/' . $bosses;
        $tpl->set_ar_out(array(
            'ID' => $plan->id,
            'CHARACTER' => '',
            'GEARSCORE' => $plan->gearscore,
            'EXPERIENCE' => $experience,
            'ARSENAL' => ''
                ), 9);
    }


    // Left Form
    if ($RaidJoined == 1 && $JoinStatus == 1 && loggedin() && $plan->time > time()) {
        $tpl->set_ar_out(array('ID' => $plan->id), 10);
    }
} else {


    #
    ###  R A I D S  -  U E B E R S I C H T  ###
    #
	
	$title = $allgAr['title'] . ' :: Raidplaner';
    $hmenu = 'Raidplaner';
    $design = new design($title, $hmenu);
    $design->header();

    $tpl = new tpl('raidplaner');


    // Next Raids	
    $tpl->set_ar_out(array('TABLENAME' => 'Kommende Raids'), 0);

    $erg = db_query('SELECT * FROM prefix_wow_raids_plan WHERE TIME > ' . time() . ' ORDER by time ASC');
    $class = '';
    while ($row = db_fetch_object($erg)) {
        $raid = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $row->raid));
        $class = ($class == 'Cnorm' ? 'Cmite' : 'Cnorm');
        $time = date('d.m.Y - H:i', $row->time);
        $heroic = ($row->heroic == '1' ? '&nbsp;<img src="include/images/icons/skull.png" alt="Heroisch" title="Heroisch" style="margin-bottom: -3px" />' : '');
        $allPlayers = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 1"));
        $players = $allPlayers . '/' . $row->players;
        $tpl->set_ar_out(array(
            'class' => $class,
            'ID' => $row->id,
            'TIME' => $time,
            'NAME' => $raid->title,
            'heroic' => $heroic,
            'PLAYERS' => $players
                ), 1);
    }

    $tpl->out(2);


    // Last Raids	
    $tpl->set_ar_out(array('TABLENAME' => 'Vorherige Raids'), 0);

    $erg2 = db_query('SELECT * FROM prefix_wow_raids_plan WHERE TIME < ' . time() . ' ORDER by time ASC');
    $class2 = '';
    while ($row = db_fetch_object($erg2)) {
        $raid = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $row->raid));
        $class2 = ($class2 == 'Cnorm' ? 'Cmite' : 'Cnorm');
        $time = date('d.m.Y - H:i', $row->time);
        $heroic = ($row->heroic == '1' ? '&nbsp;<img src="include/images/icons/skull.png" alt="Heroisch" title="Heroisch" style="margin-bottom: -3px" />' : '');
        $allPlayers = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 1"));
        $players = $allPlayers . '/' . $row->players;
        $tpl->set_ar_out(array(
            'class' => $class2,
            'ID' => $row->id,
            'TIME' => $time,
            'NAME' => $raid->title,
            'heroic' => $heroic,
            'PLAYERS' => $players
                ), 1);
    }

    $tpl->out(2);
}

###  H T M L  -  A U S G A B E  ###
##
#

$design->footer();
?>