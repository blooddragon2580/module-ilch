<?php

// Copyright by: Cristian Gheorghiu
// Support: www.ilch.de / www.cristiang.de
//
// Raidplaner v.1.3.0
//
defined('main') or die('no direct access');

#
##
###  H T M L  -  A U S G A B E  ###

if ($menu->get(1) == 'details') {


    #
    ###  R A I D   -  D E T A I L S  ###
    #

	$raid = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids` WHERE id = " . $menu->get(2)));
    $type = db_fetch_object(db_query("SELECT * FROM `prefix_wow_raids_type` WHERE id = " . $raid->type));

    $title = $allgAr['title'] . ' :: WoW Raids - ' . $raid->title;
    $hmenu = '<a href="index.php?raids">WoW Raids</a> &raquo; ' . $raid->title;
    $design = new design($title, $hmenu);
    $design->header();

    $tpl = new tpl('raids');


    $heroic = ($raid->heroic == '1' ? '&nbsp;<img src="include/images/icons/skull.png" alt="Heroischer Modus verf&uuml;gbar" title="Heroischer Modus verf&uuml;gbar" style="margin-bottom: -3px" />' : '');
    $preview = (!empty($raid->preview) ? '<a href="include/images/wowraids/raids/' . $raid->id . '/' . $raid->preview . '" target="_blank"><img src="include/images/wowraids/raids/' . $raid->id . '/' . $raid->preview . '" alt="Vorschau" width="100%" border="0" /></a>' : '<img src="include/images/icons/blank.png" alt="Keine Vorschau" />');
    $map = (!empty($raid->preview) ? '<a href="include/images/wowraids/maps/' . $raid->id . '/' . $raid->map . '" target="_blank"><img src="include/images/wowraids/maps/' . $raid->id . '/' . $raid->map . '" alt="Karte" width="100%" border="0" /></a>' : '<img src="include/images/icons/blank.png" alt="Keine Vorschau" />');


    $tpl->out(5);

    // Raid-Details
    $tpl->set_ar_out(array(
        'TITLE' => $raid->title,
        'heroic' => $heroic,
        'PREVIEW' => $preview,
        'MAP' => $map,
        'TYPE' => $type->name,
        'PLAYERS' => $raid->players,
        'LEVEL' => $raid->level,
        'PLACE' => $raid->place
            ), 6);

    $tpl->out(7);

    // Boss-Details
    $erg = db_query('SELECT * FROM `prefix_wow_raids_boss` WHERE raid = "' . $menu->get(2) . '" ORDER BY pos ASC');
    while ($row = db_fetch_object($erg)) {
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $normal = ($row->normal == '1' ? '<img src="include/images/icons/yes.png" alt="Status" title="erledigt" />' : '');
        $heroic = ($row->heroic == '1' ? '<img src="include/images/icons/yes.png" alt="Status" title="erledigt" />' : '');
        $mythical = ($row->mythical == '1' ? '<img src="include/images/icons/yes.png" alt="Status" title="erledigt" />' : '');
        $tpl->set_ar_out(array(
            'class' => $class,
            'ID' => $raid->id,
            'AVATAR' => $row->avatar,
            'NAME' => $row->name,
            'LVL' => $row->lvl,
            'NORMAL' => $normal,
            'HEROIC' => $heroic,
            'MYTHICAL' => $mythical
                ), 8);
    }

    $tpl->out(9);

    $nextRaids = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan WHERE raid = " . $raid->id . " AND TIME > " . time()));

    if ($nextRaids > 0) {
        $tpl->out(10);

        // Next Raids
        $erg = db_query('SELECT * FROM prefix_wow_raids_plan WHERE raid = ' . $raid->id . ' AND TIME > ' . time() . ' ORDER by time ASC');
        while ($row = db_fetch_object($erg)) {
            $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
            $time = date('d.m.Y - H:i', $row->time);
            $heroic = ($row->heroic == '1' ? '<img src="include/images/icons/skull.png" alt="Heroisch" title="Heroisch" style="margin-bottom: -3px" />' : '');
            $allPlayers = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_plan_players WHERE raid = " . $row->id . " AND status = 1"));
            $players = $allPlayers . '/' . $row->players;
            $tpl->set_ar_out(array(
                'class' => $class,
                'ID' => $row->id,
                'TIME' => $time,
                'NAME' => $raid->title,
                'heroic' => $heroic,
                'PLAYERS' => $players
                    ), 11);
        }

        $tpl->out(12);
    }
} else {


    #
    ###  R A I D S  -  U E B E R S I C H T  ###
    #
	
	$title = $allgAr['title'] . ' :: WoW Raids';
    $hmenu = 'WoW Raids';
    $design = new design($title, $hmenu);
    $design->header();

    $tpl = new tpl('raids');

    $tpl->out(0);

    $erg = db_query('SELECT * FROM `prefix_wow_addons` ORDER BY pos ASC');
    $class = '';
    while ($row = db_fetch_object($erg)) {
        // Type
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $addon = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids WHERE addon = " . $row->id));
        if ($addon > 0) {
            $tpl->set_ar_out(array('ADDON' => $row->title), 1);
        }
        $erg2 = db_query('SELECT * FROM `prefix_wow_raids_type` ORDER BY pos ASC');
        while ($row2 = db_fetch_object($erg2)) {
            $type = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids WHERE addon = " . $row->id . " AND type = " . $row2->id));
            if ($type > 0) {
                $tpl->set_ar_out(array('TYPE' => $row2->name), 2);
            }
            // Raids
            $erg3 = db_query('SELECT * FROM `prefix_wow_raids` WHERE addon = "' . $row->id . '" AND type = "' . $row2->id . '" ORDER BY pos ASC');
            while ($row3 = db_fetch_object($erg3)) {

                $bosses = @db_num_rows(db_query("SELECT * FROM prefix_wow_raids_boss WHERE raid = " . $row3->id));

                $heroic = ($row3->heroic == '1' ? '&nbsp;<img src="include/images/icons/skull.png" alt="Heroisch" title="Heroisch" style="margin-bottom: -3px" />' : '');

                $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
                $tpl->set_ar_out(array('ID' => $row3->id, 'class' => $class, 'NAME' => $row3->title, 'heroic' => $heroic, 'bosses' => $bosses, 'level' => $row3->level, 'players' => $row3->players), 3);
            }
        }
    }

    $tpl->out(4);
}

###  H T M L  -  A U S G A B E  ###
##
#

$design->footer();
?>