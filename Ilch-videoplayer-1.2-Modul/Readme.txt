# Videoplayer 1.2


Beschreibung:
-------------
Was macht das Modul?

Mit diesem Modul kannst du kinderleicht Videos von youtube & Co. auf deiner ilch-seite einbinden.

Mit diesem Modul kannst du Kategorien f�r deine Videos anlegen au�erdem bietet es eine gesamte Playliste und eine pers�nliche Favoriten-Playliste.

User die eingeloggt sind k�nnen Videos auch als Favorit abspeichern und diese auf der pers�nlichen Favoriten-Playliste abspielen.

Und es ist m�glich Kommentare zu den einzelnen Videos zu schreiben.


Es sind auch Live-Streams m�glich wie z.B. von tiwcht.tv (bereits vorinstalliert).


Im Adminbereich k�nnen s�mtliche Einstellungen wie Video-Breite und -H�he eingestellt werden.
Auch ob der Name und das Icon vom Anbieter ein-/ausgeblendet werden soll.
Du kannst einstellen ob nur User oder ob G�ste Kommentare schreiben d�rfen.


Bereits vorinstalliert sind:
- Dailymotion
- myvideo
- twitch.tv
- vimeo
- yahoo Video
- youtube



Entwickelt
----------
� von Cristian Gheorghiu - www.cristiang.de
� auf Basis von IlchClan 1.1 O



Installation:
-------------

� alle Dateien im Ordner UPLOAD, in ihrer Ordnerstruktur hochladen

� Die Installation aufrufen unter www.deineseite.de/index.php?install

� nach erfolgreicher Installation die Datei install.php aus dem Verzeichnis include/contents l�schen

Fertig!



Update 1.1 to 1.2:
------------------

� alle Dateien im Ordner update_1.1_to_1.2, in ihrer Ordnerstruktur hochladen

� Die Installation aufrufen unter www.deineseite.de/index.php?install

� nach erfolgreicher Installation die Datei install.php aus dem Verzeichnis include/contents l�schen

Fertig!



Sonstiges:
----------

� Die Icons f�r die Videocodecs befinden sich im Verzeichnis include/images/icons/player



Update-Log:
-----------

- Bugfix: Nun werden Videos und Favoriten richtig aus der Datenbank entfernt
- nun kann man Kategorien anlegen
- eine Box mit einem Zufalls-Video wurde hinzugef�gt
- die Videocodecs wurden um 2 Codecs erweitert




Einschr�nkungen:
----------------

- bisher keine bekannt




Fehler:
-------

- bisher keine bekannt



Haftungsausschluss:
-------------------
Ich �bernehme keine Haftung f�r Sch�den, die durch dieses Script entstehen.
Benutzung ausschlie�lich AUF EIGENE GEFAHR.



Support: www.cristiang.de
eMail: info@cristiang.de


Viel Spa�! :D