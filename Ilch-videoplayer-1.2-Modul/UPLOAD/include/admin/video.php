<?php
// Copyright by: Cristian Gheorghiu
// Support: www.cristiang.de.de
//
// Support & CMS: www.ilch.de
//
// Videoplayer 1.2

defined ('main') or die ('no direct access');
defined ('admin') or die ('only admin access');

$design = new design ('Admins Area', 'Admins Area', 2);
$design->header();
// -----------------------------------------------------------|
// #
// ##
// ###
// #### F u n k t i o n e n
function getCats ($Cid) {
    $erg = db_query("SELECT id,name FROM `prefix_vp_categories` ORDER BY name");
    if( $Cid == 0 ) {
	  $cats .= '<option value="0" selected>Keine</option>';
	} else {
	  $cats .= '<option value="0">Keine</option>';
	}
	while ($row = db_fetch_object($erg)) {
	  if( $row->id == $Cid ) {
	    $cats .= '<option value="'. $row->id .'" selected>' . $row->name . '</option>';
	  } else {
	    $cats .= '<option value="'. $row->id .'">' . $row->name . '</option>';
	  }
    }
    return ($cats);
}
function getPlayers ($Pid) {
    $erg = db_query("SELECT * FROM `prefix_vp_videocodec`");
    while ($row = db_fetch_object($erg)) {
	  if( $row->id == $Pid ) {
	    $players .= '<option value="'. $row->id .'" selected>' . $row->name . '</option>';
	  } else {
	    $players .= '<option value="'. $row->id .'">' . $row->name . '</option>';
	  }
    }
    return ($players);
}
// #### F u n k t i o n
// ###
// ##
// #

// #
// ##
// ###
// #### A k t i o n e n
if (!empty($_REQUEST['um'])) {
    $um = $_REQUEST['um'];
    $_POST['name'] = escape($_POST['name'], 'string');
	$_POST['vid'] = escape($_POST['vid'], 'string');
	$_POST['player'] = escape($_POST['player'], 'string');
	$_POST['cat'] = escape($_POST['cat'], 'integer');
	$_POST['text'] = escape($_POST['text'], 'string');
	$_POST['count'] = escape($_POST['count'], 'integer');
	$_POST['coms'] = escape($_POST['coms'], 'string');
	$_POST['box'] = escape($_POST['box'], 'string');
	$_POST['videoID'] = escape($_POST['videoID'], 'integer');
	
	$_POST['CID'] = escape($_POST['CID'], 'string');
	$_POST['cname'] = escape($_POST['cname'], 'string');
	$_POST['cicon'] = escape($_POST['cicon'], 'string');

	$_POST['C2ID'] = escape($_POST['C2ID'], 'string');
	$_POST['c2name'] = escape($_POST['c2name'], 'string');
	if ($um == 'insert') {
        // insert
        db_query("INSERT INTO `prefix_vp_video` (cdate,name,video,player,cat,text,count,coms,box)
		VALUES (". time() .",'" . $_POST['name'] . "','" . $_POST['vid'] . "','" . $_POST['player'] . "',". $_POST['cat'] .",'". $_POST['text'] ."',0,". $_POST['coms'] .",". $_POST['showBox'] .")");
        // insert
    } elseif ($um == 'change') {
        // edit
        if( $_POST['showBox'] == 1 ) {
          db_query('UPDATE `prefix_vp_video` SET box = "0" WHERE box = "1" LIMIT 1');
		}
		db_query('UPDATE `prefix_vp_video` SET
				name = "' . $_POST['name'] . '",
				video  = "' . $_POST['vid'] . '",
				player = "' . $_POST['player'] . '",
				cat = "'. $_POST['cat'] .'",
				text = "'. $_POST['text'] .'",
				coms = "'. $_POST['coms'] .'",
				box = "'. $_POST['showBox'] .'" WHERE id = "' . $_POST['videoID'] . '" LIMIT 1');
		
        $edit = $_POST['videoID'];
    } elseif ($um == 'cinsert') {
        // insert
        $text = escape($_POST['ctxt'], 'textarea');
        db_query("INSERT INTO `prefix_vp_videocodec` (name,code,icon)
		VALUES ('" . $_POST['cname'] . "','" . $text . "','" . $_POST['cicon'] . "')");
        // insert
    } elseif ($um == 'cchange') {
        // edit
        $text = escape($_POST['ctxt'], 'textarea');
		
        db_query('UPDATE `prefix_vp_videocodec` SET
				name = "' . $_POST['cname'] . '",
				icon  = "' . $_POST['cicon'] . '",
				code = "' . $text . '" WHERE id = "' . $_POST['cID'] . '" LIMIT 1');
        $edit = $_POST['cID'];
    } elseif ($um == 'c2insert') {
        // insert
        db_query("INSERT INTO `prefix_vp_categories` (name) VALUES ('" . $_POST['c2name'] . "')");
        // insert
    } elseif ($um == 'c2change') {
        // edit
        db_query('UPDATE `prefix_vp_categories` SET name = "' . $_POST['c2name'] . '" WHERE id = "' . $_POST['c2ID'] . '" LIMIT 1');
        $edit = $_POST['c2ID'];
    }
}
// edit
// del
if ($menu->get(1) == 'del') {
	// del video
    db_query('DELETE FROM `prefix_vp_video` WHERE id = "' . $menu->get(2) . '" LIMIT 1');
	// del fav of the video
    db_query('DELETE FROM `prefix_vp_favourites` WHERE video = "' . $menu->get(2) . '"');
	// del comments of the video
    db_query('DELETE FROM `prefix_vp_comments` WHERE video = "' . $menu->get(2) . '" LIMIT 1');
} elseif ($menu->get(1) == 'cdel') {
	// del videocodec
    db_query('DELETE FROM `prefix_vp_videocodec` WHERE id = "' . $menu->get(2) . '" LIMIT 1');
	// del video of the codec
	$erg = db_query('SELECT id FROM `prefix_vp_video` WHERE player = "'. $menu->get(2) .'"');
    while ($row = db_fetch_object($erg)) {
	  // del video
      db_query('DELETE FROM `prefix_vp_video` WHERE player = "' . $menu->get(2) . '" AND id ="'. $row->id .'"');
	  // del fav
      db_query('DELETE FROM `prefix_vp_favourites` WHERE video = "' . $row->id . '"');
	}
} elseif ($menu->get(1) == 'c2del') {
	// del cat
    db_query('DELETE FROM `prefix_vp_categories` WHERE id = "' . $menu->get(2) . '" LIMIT 1');
	// change cat of the video to none
	$erg = db_query('SELECT id FROM `prefix_vp_video` WHERE cat = "'. $menu->get(2) .'"');
    while ($row = db_fetch_object($erg)) {
	  // del video
      db_query('UPDATE `prefix_vp_video` set cat = "0" WHERE id = "' . $row->id . '"');
	}
}

// del
// #### A k t i o n e n
// ###
// ##
// #

// #
// ##
// ###
// #### h t m l   E i n g a b e n
if (empty ($doNoIn)) {
    $limit = 20; // Limit
    $page = ($menu->getA(1) == 'p' ? $menu->getE(1) : 1);
    $MPL = db_make_sites ($page , '' , $limit , "?news" , 'news');
    $anfang = ($page - 1) * $limit;
    if ($menu->get(1) != 'edit') {
        $FvideoID = '';
        $Faktion = 'insert';
        $Fname = '';
        $Fvid = '';
        $Fplayer = getPlayers($none);
        $Fcat = getCats($none);
        $Ftext = '';
        $Fcoms = '';
        $Fbox = '';
        $Fsub = 'Eintragen';
    } else {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_vp_video` WHERE id = " . $menu->get(2)));
        $FvideoID = $row->id;
        $Faktion = 'change';
        $Fname = $row->name;
        $Fvid = $row->video;
        $Fplayer = getPlayers($row->player);
        $Fcat = getCats($row->cat);
        $Ftext = $row->text;
        $Fcoms = ($row->coms == 1 ? 'checked="checked"' : '');
        $Fbox = ($row->box == 1 ? 'checked="checked"' : '');
        $Fsub = '&Auml;ndern';
    }
    $tpl = new tpl ('video', 1);

    $ar = array
    (
        'VIDEOID' => $FvideoID,
        'AKTION' => $Faktion,
        'MPL' => $MPL,
        'NAME' => $Fname,
        'VID' => $Fvid,
        'PLAYER' => $Fplayer,
        'CATS' => $Fcat,
        'TEXT' => $Ftext,
        'checkCOMS' => $Fcoms,
        'checkBOX' => $Fbox,
        'FSUB' => $Fsub

        );

    $tpl->set_ar_out($ar, 0);
	
	
	// v i d e o s
	$tpl->set_ar_out(array ('videocodec' => 'Keine Kategorie') , 1);
    $erg = db_query('SELECT id,name FROM `prefix_vp_video` WHERE cat = "0" ORDER BY name ASC');
	$class = '';
    while ($row = db_fetch_object($erg)) {
      $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
      $tpl->set_ar_out(array ('ID' => $row->id, 'class' => $class, 'name' => $row->name) , 2);
    }
	$erg = db_query('SELECT id,name FROM `prefix_vp_categories` ORDER BY name ASC');
    while ($row = db_fetch_object($erg)) {
	  $tpl->set_ar_out(array ('videocodec' => $row->name) , 1);
      $erg2 = db_query('SELECT id,name FROM `prefix_vp_video` WHERE cat = "'. $row->id .'" ORDER BY name ASC');
      $class = '';
      while ($row2 = db_fetch_object($erg2)) {
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $tpl->set_ar_out(array ('ID' => $row2->id, 'class' => $class, 'name' => $row2->name) , 2);
      }
    }
	#$tpl->out(1);
	#$tpl->out(2);
	// v i d e o s
	
	
	$tpl->out(3);
	
	
	// V I D E O C O D E C
    // e d i t , d e l e t e
    $abf = 'SELECT * FROM `prefix_vp_videocodec` ORDER BY name LIMIT ' . $anfang . ',' . $limit;

    $erg = db_query($abf);
    $class = '';
    while ($row = db_fetch_object($erg)) {
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $tpl->set_ar_out(array ('ID' => $row->id, 'class' => $class, 'ICON' => $row->icon, 'TITEL' => $row->name) , 4);
    }
    // e d i t , d e l e t e
	
	
	if ($menu->get(1) != 'cedit') {
        $FcID = '';
        $Faktion = 'cinsert';
        $Fcname = '';
        $Fcicon = '';
        $Fctxt = '';
        $Fsub = 'Eintragen';
    } else {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_vp_videocodec` WHERE id = " . $menu->get(2)));
        $FcID = $row->id;
        $Faktion = 'cchange';
        $Fcname = $row->name;
		$Fcicon = $row->icon;
        $Fctxt = $row->code;
        $Fsub = '&Auml;ndern';
    }

    $ar = array
    (
        'CID' => $FcID,
        'AKTION' => $Faktion,
        'MPL' => $MPL,
        'CNAME' => $Fcname,
        'CICON' => $Fcicon,
        'ctxt' => $Fctxt,
        'FSUB' => $Fsub

        );

    $tpl->set_ar_out($ar, 5);
	// V I D E O C O D E C
	
	
	// C A T E G O R I E S
	// e d i t , d e l e t e
    $abf = 'SELECT * FROM `prefix_vp_categories` ORDER BY name LIMIT ' . $anfang . ',' . $limit;

    $erg = db_query($abf);
    $class = '';
    while ($row = db_fetch_object($erg)) {
        $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
        $tpl->set_ar_out(array ('ID' => $row->id, 'class' => $class, 'TITEL' => $row->name) , 6);
    }
    // e d i t , d e l e t e
	
	if ($menu->get(1) != 'c2edit') {
        $Fc2ID = '';
        $Faktion = 'c2insert';
        $Fc2name = '';
        $Fsub = 'Anlegen';
    } else {
        $row = db_fetch_object(db_query("SELECT * FROM `prefix_vp_categories` WHERE id = " . $menu->get(2)));
        $Fc2ID = $row->id;
        $Faktion = 'c2change';
        $Fc2name = $row->name;
        $Fsub = '&Auml;ndern';
    }

    $ar = array
    (
        'C2ID' => $Fc2ID,
        'AKTION' => $Faktion,
        'MPL' => $MPL,
        'C2NAME' => $Fc2name,
        'FSUB' => $Fsub

        );

    $tpl->set_ar_out($ar, 7);
	// C A T E G O R I E S
}

$design->footer();

?>