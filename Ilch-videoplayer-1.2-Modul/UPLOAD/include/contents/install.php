<?php
// Viewer-Modul by: Cristian Gheorghiu
// Web: www.cristiang.de
//
// Support & CMS: www.ilch.de
//
// Videoplayer 1.2


defined ('main') or die ( 'no direct access' );



	// MySQL Tabellen anlegen
	if( isset($_POST['submit']) ) {
	
		
		// Admin > Module
		db_query("INSERT INTO `prefix_modules` (`url`, `name`, `gshow`, `ashow`, `fright`) VALUES
		('video', 'Videoplayer', 1, 1, 1);");
		
		
		// Admin > Settings
		db_query("INSERT INTO `prefix_config` (`schl`, `typ`, `kat`, `frage`, `wert`, `pos`) VALUES
		('vp_width', 'input', 'Videoplayer Optionen', 'Breite :', '440', 2),
		('vp_height', 'input', 'Videoplayer Optionen', 'H�he :', '250', 1),
		('vp_show_provider', 'r2', 'Videoplayer Optionen', 'Anbieter-Icon anzeigen?', '1', 4),
		('vp_show_name', 'r2', 'Videoplayer Optionen', 'Videoname anzeigen?', '1', 3),
		('vp_description', 'r2', 'Videoplayer Optionen', 'Beschreibung und Statistik anzeigen?', '1', 5),
		('vp_comments_rights', 'r2', 'Videoplayer Optionen', 'D�rfen G�ste Kommentare schreiben?', '1', 7),
		('vp_comments', 'r2', 'Videoplayer Optionen', 'Kommentare f�r Videos zulassen?', '1', 6)
		;");
		
				
		// Videocodecs
		db_query("CREATE TABLE IF NOT EXISTS `prefix_vp_videocodec` (
	   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	   `name` varchar(50) COLLATE latin1_german1_ci NOT NULL,
	   `code` text COLLATE latin1_german1_ci NOT NULL,
	   `icon` varchar(120) COLLATE latin1_german1_ci NOT NULL,
	    PRIMARY KEY (`id`) );");
		
		db_query("INSERT INTO `prefix_vp_videocodec` (`name`, `code`, `icon`) VALUES
		('Dailymotion', '<iframe frameborder=\"0\" width=\"{vwidth}\" height=\"{vheight}\" src=\"http://www.dailymotion.com/embed/video/{vid}\"></iframe>', 'dailymotion.png'),
		('myvideo', '<iframe src=\"https://www.myvideo.de/embed/{vid}\" style=\"width:{vwidth}px;height:{vheight}px;border:0px none;padding:0;margin:0;\" width=\"{vwidth}\" height=\"{vheight}\" frameborder=\"0\" scrolling=\"no\"></iframe>', 'myvideo.png'),
		('vimeo', '<iframe src=\"http://player.vimeo.com/video/{vid}?badge=0\" width=\"{vwidth}\" height=\"{vheight}\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>', 'vimeo.png'),
		('yahoo Videos', '<iframe width=\"{vwidth}\" height=\"{vheight}\" scrolling=\"no\" frameborder=\"0\" src=\"http://screen.yahoo.com/{vid}.html?format=embed&player_autoplay=false\"></iframe>', 'yahoo.png'),
		('youtube', '<object width=\"{vwidth}\" height=\"{vheight}\"><param name=\"movie\" value=\"http://www.youtube.com/v/{vid}?version=3&hl=de_DE&rel=0\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/{vid}?version=3&hl=de_DE&rel=0\" type=\"application/x-shockwave-flash\" width=\"{vwidth}\" height=\"{vheight}\" allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed></object>', 'youtube.png'),
		('twitch.tv', '<object type=\"application/x-shockwave-flash\" id=\"live_embed_player_flash\" data=\"http://de.twitch.tv/widgets/live_embed_player.swf?channel=esltv_studio1\" bgcolor=\"#000000\" height=\"{vheight}\" width=\"{vwidth}\"><param name=\"allowFullScreen\" value=\"true\"><param name=\"allowscriptaccess\" value=\"always\"><param name=\"allownetworking\" value=\"all\"><param name=\"movie\" value=\"http://de.twitch.tv/widgets/live_embed_player.swf\"><param name=\"flashvars\" value=\"channel={vid}&auto_play=false&start_volume=1\"></object>', 'twitchtv.png');");
		
		
		// Video categories
		db_query("CREATE TABLE IF NOT EXISTS `prefix_vp_categories` (
  		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  		`name` varchar(120) COLLATE latin1_german1_ci NOT NULL,
  		PRIMARY KEY (`id`) );");
		
		
		// Video
		db_query("CREATE TABLE IF NOT EXISTS `prefix_vp_video` (
		`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		`cdate` varchar(50) COLLATE latin1_german1_ci NOT NULL,
		`name` varchar(120) COLLATE latin1_german1_ci NOT NULL,
		`video` varchar(120) COLLATE latin1_german1_ci NOT NULL,
		`player` varchar(5) COLLATE latin1_german1_ci NOT NULL,
		`cat` varchar(5) COLLATE latin1_german1_ci NOT NULL,
		`text` text COLLATE latin1_german1_ci NOT NULL,
		`count` varchar(10) COLLATE latin1_german1_ci NOT NULL,
		`coms` varchar(1) COLLATE latin1_german1_ci NOT NULL,
		`box` varchar(1) COLLATE latin1_german1_ci NOT NULL,
		PRIMARY KEY (`id`) );");
		
		
		// Favourites
		db_query("CREATE TABLE IF NOT EXISTS `prefix_vp_favourites` (
		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
		`user` varchar(5) COLLATE latin1_german1_ci NOT NULL,
		`video` varchar(5) COLLATE latin1_german1_ci NOT NULL,
		PRIMARY KEY (`id`) );");
		
		
		// Comments
		db_query("CREATE TABLE IF NOT EXISTS `ic1_vp_comments` (
		`id` int(5) unsigned NOT NULL AUTO_INCREMENT,
		`cdate` varchar(50) COLLATE latin1_general_ci NOT NULL,
		`video` varchar(5) COLLATE latin1_general_ci NOT NULL,
		`user` varchar(5) COLLATE latin1_general_ci NOT NULL,
		`session` varchar(150) COLLATE latin1_general_ci NOT NULL,
		`name` varchar(50) COLLATE latin1_general_ci NOT NULL,
		`text` varchar(450) COLLATE latin1_general_ci NOT NULL,
		PRIMARY KEY (`id`) );");
		
		
	
	
		echo '<p>Videoplayer 1.2 wurde installiert.</p>';
		echo '<p><a href="admin.php">jetzt Videos eintragen</a></p>';

	} else { 
	
		echo '<p><h2>Videoplayer 1.2 Installation</h2>Wenn du das Viedeoplayer-Modul installiert hast, musst du die install.php aus dem Verzeichnis include/contents/ l&ouml;schen!</p>
<form action="" method="post"><input type="submit" name="submit" value="Jetzt installieren"  /></form>';

	}



?>
