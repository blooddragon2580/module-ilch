<?php 
#   Copyright by: Cristian Gheorghiu
#   Support: www.cristiang.de


defined ('main') or die ( 'no direct access' );

# menu
require_once('include/contents/forum/menu.php');

switch($menu->get(1)) {
  default :          	               $userDatei = 'player';         	 	 break;
	case 'details'					 : $userDatei = 'details';    	         break;
	case 'categories'				 : $userDatei = 'categories';            break;
	case 'codecs'					 : $userDatei = 'codecs';				 break;
	case 'favourites'				 : $userDatei = 'favourites';            break;
}
 
require_once('include/contents/video/'.$userDatei.'.php');


?>