<?php
#   Copyright by: Cristian Gheorghiu
#   Support: www.cristiang.de.de


defined ('main') or die ( 'no direct access' );

  
  // count all videos
  $vcount_all = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $menu->get(2) ."'"));
  // check if video exist
  $vcheck = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE id = '". $menu->get(4) ."' AND  cat = '". $menu->get(2) ."'"));
  // check if favourite exist
  $fcheck = db_num_rows(db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' AND video = '". $menu->get(4) ."'"));
  // count all categories
  $ccount_all = db_num_rows(db_query("SELECT id FROM prefix_vp_categories"));
  // check if categories exist
  $ccheck = db_num_rows(db_query("SELECT * FROM prefix_vp_categories WHERE id = '". $menu->get(2) ."'"));

#
##
###
####
#####   F U N C T I O N S   #####

  function prevBUTTON ($CAT,$pos) {
    $pos2 = $pos - 2;
    $ges = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $CAT ."' ORDER BY name"));
	$row2 = db_fetch_object(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $CAT ."' ORDER BY name DESC Limit ".$ges));
	if( $pos !== 1 ) { $row = db_fetch_object(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $CAT ."' ORDER BY name ASC Limit ". $pos2 .",1")); }
	if( $pos == 1 ) {
	  $prev = $row2->id;
	} elseif ( $pos > 1 ) {
	  $prev = $row->id;
	}
	return $prev;
  };
  
  
  function nextBUTTON ($CAT,$pos) {
    $pos2 = $pos - 1;
    $ges = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $CAT ."' ORDER BY name"));
	$row2 = db_fetch_object(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $CAT ."' ORDER BY name ASC Limit 1"));
	$row = db_fetch_object(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $CAT ."' ORDER BY name ASC Limit ". $pos .",1"));
	if( $pos >= $ges ) {
	  $next = $row2->id;
	} elseif ( $pos < $ges ) {
	  $next = $row->id;
	}
	return $next;
  };

#####   F U N C T I O N S   #####
####
###
##
#


#
##
###
####
#####   A C T I O N S   #####

  if( $menu->get(5) == 'fav' ) {
    if ( loggedin() ) {
	  if( $fcheck == 0 ) {
	    db_query("INSERT INTO `prefix_vp_favourites` (user,video) VALUES (" . $_SESSION['authid'] . ", " . $menu->get(4) . ")");
	  } else {
	    db_query('DELETE FROM `prefix_vp_favourites` WHERE user = "' . $_SESSION['authid'] . '" AND video = "' . $menu->get(4) . '" LIMIT 1');
	  }
	  header('Location: index.php?video-categories-'. $menu->get(2) .'-show-'. $menu->get(4));
	  $design->header();
	} else {
	  header('Location: index.php?user-login');
	  $design->header();
	}
  }

#####   A C T I O N S   #####
####
###
##
#


#
##
###
####
#####   H T M L  O U T   ##### 

  if( $ccount_all > 0 ) {
    if( $ccheck == 1 && $menu->get(3) == 'show' && $vcheck == 1 ) {
  
  	  
	  $cat = db_fetch_object(db_query("SELECT * FROM prefix_vp_categories WHERE id = ". $menu->get(2)));
	  
      $title = $allgAr['title'].' :: Video Kategorie - '. $cat->name;
      $hmenu  = '<a href="index.php?video-player">Videoplayer</a> &raquo; <a href="index.php?video-categories">Kategorie</a> &raquo; '. $cat->name;
      $design = new design ( $title , $hmenu, 1);
      $design->header();
	
      $tpl = new tpl ( 'video/categories.htm' );
	
	
	    $row['width'] = $allgAr ['vp_width'];
	    $tpl->set_ar_out( $row,4 );
		
	    // Video Out
		$erg = db_query("SELECT * FROM prefix_vp_video WHERE id ='". $menu->get(4) ."'");
        while ($row = db_fetch_assoc($erg) ) {
	      // Name Out
	      if( $allgAr['vp_show_name'] == 1 ) {
	        $tpl->set_ar_out( $row,5 );
	      }
	      $random_video = $row['id'];
	      $row2 = db_fetch_object(db_query("SELECT code FROM prefix_vp_videocodec WHERE id = '". $row['player'] ."'"));
	      $row['height'] = $allgAr['vp_height'];
	      $row['video'] = str_replace('{vid}', $row['video'], $row2->code);
	      $row['video'] = str_replace('{vwidth}', $allgAr['vp_width'], $row['video']);
	      $row['video'] = str_replace('{vheight}', $allgAr['vp_height'], $row['video']);
	      $tpl->set_ar_out( $row,6 );
		  // Hits + 1
    	  $count = $row['count'];
    	  $count++;
    	  db_query("UPDATE `prefix_vp_video` SET count = ". $count ." WHERE id = '" . $row['id'] ."'");
	    }
	
	
	    // Menu Build
	    $vcount_all = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE cat = '". $menu->get(2) ."'"));
	    // Select Video
	    $video_id = $menu->get(4);
	    $catID = $menu->get(2);
	    $erg = db_query("SELECT * FROM prefix_vp_video WHERE cat = '". $menu->get(2) ."' ORDER BY name");
	    $i = 0;
        while ($row = db_fetch_assoc($erg) ) {
	      $i++;
	      if( $row['id'] == $video_id ? $vcount = $i : '');
 	    }
	    $row['count'] = $vcount .'/'. $vcount_all;
	    $row['prev'] = prevBUTTON($menu->get(2),$vcount);	
	    $row['next'] = nextBUTTON($menu->get(2),$vcount);
	    // Favourite
	    $cvcheck = db_num_rows(db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' AND video = '". $menu->get(4) ."'"));
	    if ( $cvcheck == 1 ) {
	      $row['favico'] = 'heart.png';
	      $row['favtitle'] = 'als Favorit gespeichert';
	    } else {
	      $row['favico'] = 'heart_gray.png';
	      $row['favtitle'] = 'Favorit';
	    }
	    $row['vid'] = $video_id;
	    $tpl->set_ar_out( $row,7 );
	
	
	
	    // Videolist Out
	    $erg = db_query("SELECT * FROM prefix_vp_video WHERE cat = '". $menu->get(2) ."' ORDER BY name");
	    $i = 0;
        while ($row = db_fetch_assoc($erg) ) {
	      $row2 = db_fetch_object(db_query("SELECT * FROM prefix_vp_videocodec WHERE id = '". $row['player'] ."'"));
	      $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
	      $i++;
	      if( $random_video == $row['id'] ) {
	        $row['num'] = '<img src="include/images/icons/play.png" alt="playing" style="margin-bottom:-3px" width="16" height="16" />';
		    $class = 'Cdark';
	      } else {
	        $row['num'] = $i;
	      }
	      if( $allgAr['vp_show_provider'] == 1) { 
	        $row['icon'] = '<img src="include/images/icons/player/'. $row2->icon .'" alt="'. $row2->name .'" title="'. $row2->name .'" style="margin-bottom:-3px" />';
	      } else {
	        $row['icon'] = '';
	      }
	      $row['class'] = $class;
	      $tpl->set_ar_out( $row,8 );
	    }
	
	
	    $tpl->out(9);
	  
	  
	  
	  } else {
	  
	  
	  
	  $title = $allgAr['title'].' :: Video Kategorien';
      $hmenu  = '<a href="index.php?video-player">Videoplayer</a> &raquo; Kategorien';
      $design = new design ( $title , $hmenu, 1);
      $design->header();
	
      $tpl = new tpl ( 'video/categories.htm' );
	  
	    $tpl->out(0);
		
	    // Categorie Out
		$erg = db_query("SELECT * FROM prefix_vp_categories ORDER BY name");
        while ($row = db_fetch_assoc($erg) ) {
	      // Name Out
	      $tpl->set_ar_out( $row,1 );
		  // Videos Out
		  $erg2 = db_query("SELECT * FROM prefix_vp_video WHERE cat = '". $row['id'] ."' ORDER BY name");
		  $i = 0;
          while ($video = db_fetch_assoc($erg2) ) {
		    $row2 = db_fetch_object(db_query("SELECT * FROM prefix_vp_videocodec WHERE id = '". $video['player'] ."'"));
		    $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
		    $i++;
	        if( $allgAr['vp_show_provider'] == 1) { 
	          $video['icon'] = '<img src="include/images/icons/player/'. $row2->icon .'" alt="'. $row2->name .'" title="'. $row2->name .'" style="margin-bottom:-3px" />';
	        } else {
	          $video['icon'] = '';
	        }
			$video['class'] = $class;
			$video['num'] = $i;
		  $tpl->set_ar_out( $video,2 );
		  }
	    }
		
		$tpl->out(3);

	  
	  
	  }
	  $tpl->out(10);
	} else {
	  header('Location: index.php?video-player');
	  $design->header();
	}
	
#####   H T M L  O U T   #####  
####
###
##
#
	
	
	
$design->footer();
?>