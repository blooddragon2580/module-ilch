<?php
#   Copyright by: Cristian Gheorghiu
#   Support: www.cristiang.de.de


defined ('main') or die ( 'no direct access' );

  
  // count all videos
  $vcount_all = db_num_rows(db_query("SELECT id FROM prefix_vp_video"));
  // check if video exist
  $vcheck = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE id='". $menu->get(2) ."'"));
  // check if favourite exist
  $fcheck = db_num_rows(db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' AND video = '". $menu->get(2) ."'"));
  // video daten auslesen
  if( $vcheck == 1 ) { $vid = db_fetch_object(db_query("SELECT * FROM prefix_vp_video WHERE id='". $menu->get(2) ."'")); }
  // user daten
  if( loggedin() ) {
    $user = db_fetch_object(db_query("SELECT * FROM prefix_user WHERE id='". $_SESSION['authid'] ."'"));
  }

#
##
###
####
#####   F U N C T I O N S   #####

#####   F U N C T I O N S   #####
####
###
##
#


#
##
###
####
#####   A C T I O N S   #####

  if( $menu->get(3) == 'fav' ) {
    if ( loggedin() ) {
	  if( $fcheck == 0 ) {
	    db_query("INSERT INTO `prefix_vp_favourites` (user,video) VALUES (" . $_SESSION['authid'] . ", " . $menu->get(2) . ")");
	  } else {
	    db_query('DELETE FROM `prefix_vp_favourites` WHERE user = "' . $_SESSION['authid'] . '" AND video = "' . $menu->get(2) . '" LIMIT 1');
	  }
	  header('Location: index.php?video-details-'.$menu->get(2));
	  $design->header();
	} else {
	  header('Location: index.php?user-login');
	  $design->header();
	}
  }
  // Kommentar schreiben
    $_POST['name'] = escape($_POST['name'], 'string');
    $_POST['text'] = escape($_POST['text'], 'string');
  if( loggedin() AND !empty($_POST['text']) ) {
    db_query("INSERT INTO `prefix_vp_comments` (cdate,video,user,session,name,text)
	VALUES (" . time() . "," . $menu->get(2) . "," . $_SESSION['authid'] . ",'" . session_id() . "','" . $user->name . "','" . $_POST['text'] . "')");
	
	header('Location: index.php?video-details-'.$menu->get(2));
	$design->header();
  } elseif( !empty($_POST['name']) AND !empty($_POST['text']) AND chk_antispam('video') ) {
    db_query("INSERT INTO `prefix_vp_comments` (cdate,video,user,session,name,text)
	VALUES (" . time() . "," . $menu->get(2) . "," . $_SESSION['authid'] . ",'" . session_id() . "','" . $_POST['name'] . "','" . $_POST['text'] . "')");
	
	header('Location: index.php?video-details-'.$menu->get(2));
	$design->header();
  }
  // Kommentar entfernen
  if( is_admin() AND $menu->get(3) == 'del' ) {
    db_query('DELETE FROM `prefix_vp_comments` WHERE id = "' . $menu->get(4) . '" LIMIT 1');
	
	header('Location: index.php?video-details-'.$menu->get(2));
	$design->header();
  }
  // Hit +
  if( $vcheck == 1 ) {
    $count = $vid->count;
    $count++;
    db_query("UPDATE `prefix_vp_video` SET count = ". $count ." WHERE id = '" . $menu->get(2) ."'");
  }

#####   A C T I O N S   #####
####
###
##
#


#
##
###
####
#####   H T M L  O U T   ##### 

  if( $vcheck == 1 ) {
  	  
    $title = $allgAr['title'].' :: Video: '. $vid->name;
    $hmenu  = 'Video: '. $vid->name;
    $design = new design ( $title , $hmenu, 1);
    $design->header();
	
    $tpl = new tpl ( 'video/details.htm' );
	
	
	  $row['width'] = $allgAr ['vp_width'];
	  $tpl->set_ar_out( $row,0 );
	
	
	  // Video Out
	  $erg = db_query("SELECT * FROM prefix_vp_video WHERE id='". $menu->get(2) ."'");
      while ($row = db_fetch_assoc($erg) ) {
	    // Name Out
	    if( $allgAr['vp_show_name'] == 1 ) {
	      $tpl->set_ar_out( $row,1 );
	    }
	    $random_video = $row['id'];
	    $row2 = db_fetch_object(db_query("SELECT code FROM prefix_vp_videocodec WHERE id = '". $row['player'] ."'"));
	    $row['height'] = $allgAr['vp_height'];
	    $row['video'] = str_replace('{vid}', $row['video'], $row2->code);
	    $row['video'] = str_replace('{vwidth}', $allgAr['vp_width'], $row['video']);
	    $row['video'] = str_replace('{vheight}', $allgAr['vp_height'], $row['video']);
	    $row['text'] = bbcode($row['text']);
	    $row['hits'] = $row['count'];
	    $row['favs'] = db_result(db_query("SELECT COUNT(id) FROM prefix_vp_favourites WHERE video = ". $menu->get(2)));
	    $row['comments'] = db_result(db_query("SELECT COUNT(id) FROM prefix_vp_comments WHERE video = ". $menu->get(2)));
	    $tpl->set_ar_out( $row,2 );
	  }

	  // Favourite
	  if ( $fcheck == 1 ) {
	    $row['favico'] = 'heart.png';
	    $row['favtitle'] = 'als Favorit gespeichert';
	  } else {
	    $row['favico'] = 'heart_gray.png';
	    $row['favtitle'] = 'Favorit';
	  }
	  if ( $vid->cat == 0 ) {
	    $row['catlink'] = '';
	  } else {
	    $row['catlink'] = '-'.$vid->cat.'-show-'.$vid->id;
	  }

	  $row['vid'] = $vid->id;
	  $tpl->set_ar_out( $row,3 );
	
	
	  // Video Beschreibung & Statistik
	  if( $allgAr['vp_description'] == 1 ) {
	    $tpl->out(4);
	  }
	  $tpl->out(5);
	  
	  
	  if( $allgAr['vp_comments'] == 1 ) {
	    if( $vid->coms == 1 ) {
		  if ( loggedin() ) {
	        // User Formular
	        $tpl->out(6);
		      // name nicht eingeben
		      $form['name'] = $user->name;
		      $tpl->set_ar_out($form, 7);
	        $tpl->out(9);
			  // antispam nicht eingeben
		      $form['ANTISPAM'] = get_antispam('video',100);
		      $tpl->set_ar_out($form, 10);
		  } elseif( $allgAr['vp_comments_rights'] == 1 ) {
	        // Guest Formular
	        $tpl->out(6);
			  // name eingeben
		      $tpl->out(8);
	        $tpl->out(9);
			  // antispam
		      $form['ANTISPAM'] = get_antispam('video',100).'<br />';
		      $tpl->set_ar_out($form, 10);
		  }
		
			 $tpl->out(11);
		
	      // Kommentare ausgeben
	      $erg = db_query("SELECT * FROM prefix_vp_comments WHERE video='". $menu->get(2) ."' ORDER BY cdate DESC");
	      $i = 0;
          while ($message = db_fetch_assoc($erg) ) {
	  	    if( is_admin() ) {
		      // name nicht eingeben
		      $message['del'] = '<a href="index.php?video-details-'. $message['video'] .'-del-'. $message['id'] .'"><img src="include/images/icons/del.gif" alt="entfernen" title="entfernen" /></a>';
		    } else {
		      // antispam
		      $message['del'] = '';
		    }
	        $i++;
	        $message['no'] = $i;
	        $message['date'] = date('d.m.Y - H:i', $message['cdate']);
	        $tpl->set_ar_out( $message,12 );
	      }
	    } else {
	      echo '<br /><br />Kommentare sind f�r dieses Video deaktiviert.<br /><br />';
	    }
	  } else {
	    echo '<br /><br />Kommentare sind f�r Videos deaktiviert.<br /><br />';
	  }


	  $tpl->out(13);

	  
  } else {
  
  
    $title = $allgAr['title'].' :: Video';
    $hmenu  = 'Video';
    $design = new design ( $title , $hmenu, 1);
    $design->header();
	
	echo 'Das Video wurde nicht gefunden';
  
  
  }
	
#####   H T M L  O U T   #####  
####
###
##
#
	
	
	
$design->footer();
?>