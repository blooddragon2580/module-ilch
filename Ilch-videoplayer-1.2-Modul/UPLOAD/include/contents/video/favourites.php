<?php
#   Copyright by: Cristian Gheorghiu
#   Support: www.cristiang.de.de


defined ('main') or die ( 'no direct access' );

  
  // count all videos
  $vcount_all = db_num_rows(db_query("SELECT id FROM prefix_vp_video"));
  // check if video exist
  $vcheck = db_num_rows(db_query("SELECT id FROM prefix_vp_video WHERE id='". $menu->get(3) ."'"));
  // check if favourite exist
  $fcheck = db_num_rows(db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."'"));

#
##
###
####
#####   F U N C T I O N S   #####

  function prevBUTTON ($video,$pos) {
    $pos2 = $pos - 2;
    $ges = db_num_rows(db_query("SELECT id FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id"));
	$row2 = db_fetch_object(db_query("SELECT video FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id ASC Limit ".$ges));
	if( $pos !== 1 ) { $row = db_fetch_object(db_query("SELECT video FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id DESC Limit ". $pos2 .",1")); }
	if( $pos == 1 ) {
	  $prev = $row2->video;
	} elseif ( $pos > 1 ) {
	  $prev = $row->video;
	}
	return $prev;
  };
  
  
  function nextBUTTON ($video,$pos) {
    $pos2 = $pos - 1;
    $ges = db_num_rows(db_query("SELECT id FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id"));
	$row2 = db_fetch_object(db_query("SELECT video FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id DESC Limit 1"));
	$row = db_fetch_object(db_query("SELECT video FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id DESC Limit ". $pos .",1"));
	if( $pos >= $ges ) {
	  $next = $row2->video;
	} elseif ( $pos < $ges ) {
	  $next = $row->video;
	}
	return $next;
  };

#####   F U N C T I O N S   #####
####
###
##
#


#
##
###
####
#####   A C T I O N S   #####

  if( $menu->get(4) == 'fav' ) {
    if ( loggedin() ) {
	  if( $fcheck == 0 ) {
	    db_query("INSERT INTO `prefix_vp_favourites` (user,video) VALUES (" . $_SESSION['authid'] . ", " . $menu->get(3) . ")");
	  } else {
	    db_query('DELETE FROM `prefix_vp_favourites` WHERE user = "' . $_SESSION['authid'] . '" AND video = "' . $menu->get(3) . '" LIMIT 1');
	  }
	  header('Location: index.php?video-favourites');
	  $design->header();
	} else {
	  header('Location: index.php?user-login');
	  $design->header();
	}
  }

#####   A C T I O N S   #####
####
###
##
#


#
##
###
####
#####   H T M L  O U T   ##### 

  if( loggedin() && $fcheck >= 1 && $vcount_all > 0 ) {
  
    $title = $allgAr['title'].' :: Meine Favoriten';
    $hmenu  = '<a href="index.php?video-player">Videoplayer</a> &raquo; Meine Favoriten';
    $design = new design ( $title , $hmenu, 1);
    $design->header();
	
    $tpl = new tpl ( 'video/favourites.htm' );
	
	
	  $row['width'] = $allgAr ['vp_width'];
	  $tpl->set_ar_out( $row,0 );
	
	
	  if( $menu->get(2) == "show" && $vcheck == 1 ) {
	    // Select Video
	    $erg = db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' AND video ='". $menu->get(3) ."'");
	  } else {
	    // Random Video
	    $erg = db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id LIMIT 1");
	  }
	
	
	  // Video Out
      while ($row = db_fetch_assoc($erg) ) {
	    $video = db_fetch_object(db_query("SELECT * FROM prefix_vp_video WHERE id = '". $row['video'] ."'"));
	    // Name Out
	    if( $allgAr['vp_show_name'] == 1 ) {
		  $row['name'] = $video->name;
	      $tpl->set_ar_out( $row,1 );
	    }
	    $random_video = $video->id;
	    $row2 = db_fetch_object(db_query("SELECT code FROM prefix_vp_videocodec WHERE id = '". $video->player ."'"));
	    $row['height'] = $allgAr['vp_height'];
	    $row['video'] = str_replace('{vid}', $video->video, $row2->code);
	    $row['video'] = str_replace('{vwidth}', $allgAr['vp_width'], $row['video']);
	    $row['video'] = str_replace('{vheight}', $allgAr['vp_height'], $row['video']);
	    $tpl->set_ar_out( $row,2 );
		// Hits + 1
    	$count = $video->count;
    	$count++;
    	db_query("UPDATE `prefix_vp_video` SET count = ". $count ." WHERE id = '" . $video->id ."'");
	  }
	
	
	  // Menu Build
	  $vcount_all = db_num_rows(db_query("SELECT id FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."'"));
	  if( $menu->get(2) == "show" && $vcheck == 1 ) {
	    // Select Video
	    $video_id = $menu->get(3);
	  } else {
	    // Random Video
	    $video_id = $random_video;
	  }
	  $erg = db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id DESC");
	  $i = 0;
      while ($row = db_fetch_assoc($erg) ) {
	    $i++;
	    if( $row['video'] == $video_id ? $vcount = $i : '');
 	  }
	  $row['count'] = $vcount .'/'. $vcount_all;
	  $row['prev'] = prevBUTTON($video_id,$vcount);	
	  $row['next'] = nextBUTTON($video_id,$vcount);
	  // Favourite
	  $fvcheck = db_num_rows(db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' AND video = '". $video_id ."'"));
	  if ( $fvcheck == 1 ) {
	    $row['favico'] = 'heart.png';
	    $row['favtitle'] = 'als Favorit gespeichert';
	  } else {
	    $row['favico'] = 'heart_gray.png';
	    $row['favtitle'] = 'Favorit';
	  }
	  $row['vid'] = $video_id;
	  $tpl->set_ar_out( $row,3 );
	
	
	
	  // Videolist Out
	  $erg = db_query("SELECT * FROM prefix_vp_favourites WHERE user = '". $_SESSION['authid'] ."' ORDER BY id DESC");
	  $i = 0;
      while ($row = db_fetch_assoc($erg) ) {
	    $video = db_fetch_object(db_query("SELECT * FROM prefix_vp_video WHERE id = '". $row['video'] ."'"));
	    $row2 = db_fetch_object(db_query("SELECT * FROM prefix_vp_videocodec WHERE id = '". $video->player ."'"));
	    $class = ($class == 'Cmite' ? 'Cnorm' : 'Cmite');
	    $i++;
	    $row['id'] = $video->id;
	    $row['name'] = $video->name;
	    if( $random_video == $row['id'] ) {
	      $row['num'] = '<img src="include/images/icons/play.png" alt="playing" style="margin-bottom:-3px" width="16" height="16" />';
		  $class = 'Cdark';
	    } else {
	      $row['num'] = $i;
	    }
	    if( $allgAr['vp_show_provider'] == 1) { 
	      $row['icon'] = '<img src="include/images/icons/player/'. $row2->icon .'" alt="'. $row2->name .'" title="'. $row2->name .'" style="margin-bottom:-3px" />';
	    } else {
	      $row['icon'] = '';
	    }
	    $row['class'] = $class;
	    $tpl->set_ar_out( $row,4 );
	  }
	
	
	  $tpl->out(5);
	
	
	} else {
	  header('Location: index.php?video-player');
	  $design->header();
	}
	
#####   H T M L  O U T   #####  
####
###
##
#
	
	
	
$design->footer();
?>